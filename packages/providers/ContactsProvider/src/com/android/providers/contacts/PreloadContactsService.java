/*
| ===============================================================================|
|      Modifications on Features list / Changes Request / Problems Report        |
| *******************************************************************************|
| 07/13/2015|       feng.jing      |        PR-1042413   |Pre loaded phonebook  |
|           |                      |                     |contacts on Oi Brazil perso|
| ******************************************************************************|
================================================================================
*/
package com.android.providers.contacts;

import java.util.ArrayList;

import android.app.IntentService;
import android.database.sqlite.SQLiteDatabase;
import android.content.ContentResolver;
import android.content.Intent;
import android.content.ContentProviderOperation;
import android.provider.ContactsContract;
import android.os.RemoteException;
import android.content.OperationApplicationException;
import android.util.Log;

import com.google.common.annotations.VisibleForTesting;

/**
 * A service that pre-load phonebook contacts.
 */
public class PreloadContactsService extends IntentService {
    private static final String TAG = "PreloadContactsService";

    public PreloadContactsService(){
        super("PreloadContactsService");
    }

    @Override
    protected void onHandleIntent(Intent intent) {
      String[] mPreloadedContactsname = getResources().getStringArray(R.array.preloaded_contacts_name);
      String[] mPreloadedContactsnumber = getResources().getStringArray(R.array.preloaded_contacts_number);

      if(null == mPreloadedContactsname || null == mPreloadedContactsnumber){
          return;
      }
      if(mPreloadedContactsname.length >0 ){
          ContactsDatabaseHelper helper = ContactsDatabaseHelper.getInstance(this);
          SQLiteDatabase db=helper.getWritableDatabase();
          db.beginTransaction();
          try{
              for (int i=0;i<mPreloadedContactsname.length;i++){
                  actuallyAddOneContact(mPreloadedContactsname[i], mPreloadedContactsnumber[i]);
              }
              db.setTransactionSuccessful();
          }finally{
              db.endTransaction();
          }
        }else {
          return ;
      }
    }

    private void actuallyAddOneContact(String name, String number){
        final ArrayList<ContentProviderOperation> operationList=new ArrayList<ContentProviderOperation>();
        ContentProviderOperation.Builder builder=ContentProviderOperation.newInsert(ContactsContract.RawContacts.CONTENT_URI);

        // create a new contact account
        builder.withValue(ContactsContract.RawContacts.AGGREGATION_MODE, ContactsContract.RawContacts.AGGREGATION_MODE_DISABLED);
        operationList.add(builder.build());

        // add contact's name
        builder=ContentProviderOperation.newInsert(ContactsContract.Data.CONTENT_URI);
        builder.withValueBackReference(ContactsContract.Data.RAW_CONTACT_ID, 0);
        builder.withValue(ContactsContract.Data.MIMETYPE, ContactsContract.CommonDataKinds.StructuredName.CONTENT_ITEM_TYPE);
        builder.withValue(ContactsContract.CommonDataKinds.StructuredName.DISPLAY_NAME, name);
        operationList.add(builder.build());

        // add contacts's number
        if(!number.contains(",")){
            builder=ContentProviderOperation.newInsert(ContactsContract.Data.CONTENT_URI);
            builder.withValueBackReference(ContactsContract.Data.RAW_CONTACT_ID, 0);
            builder.withValue(ContactsContract.Data.MIMETYPE, ContactsContract.CommonDataKinds.Phone.CONTENT_ITEM_TYPE);
            builder.withValue(ContactsContract.CommonDataKinds.Phone.NUMBER, number);
            builder.withValue(ContactsContract.CommonDataKinds.Phone.TYPE,ContactsContract.CommonDataKinds.Phone.TYPE_MOBILE);
            operationList.add(builder.build());
        }else{
            String[] numbers=number.split(",");
            for(int i=0;i<numbers.length;i++){
                builder=ContentProviderOperation.newInsert(ContactsContract.Data.CONTENT_URI);
                builder.withValueBackReference(ContactsContract.Data.RAW_CONTACT_ID, 0);
                builder.withValue(ContactsContract.Data.MIMETYPE, ContactsContract.CommonDataKinds.Phone.CONTENT_ITEM_TYPE);
                builder.withValue(ContactsContract.CommonDataKinds.Phone.NUMBER, numbers[i]);
                builder.withValue(ContactsContract.CommonDataKinds.Phone.TYPE,ContactsContract.CommonDataKinds.Phone.TYPE_MOBILE);
                operationList.add(builder.build());
            }
        }

        try {
            getContentResolver().applyBatch(ContactsContract.AUTHORITY, operationList);
          }catch (RemoteException e) {
              Log.e(TAG, "Can't insert preload contacts to database",e);
          }catch (OperationApplicationException e) {
              Log.e(TAG, "Can't insert preload contacts to database",e);
         }
    }
}
