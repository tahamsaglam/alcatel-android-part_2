package com.android.providers.media;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.TimeZone;

import android.content.Context;
import android.net.Uri;
import android.provider.Settings;
import android.text.format.Time;
import android.util.TctLog;

import com.tct.drm.DrmUri;

public class DrmUtil {
    private static final boolean DEBUG = true;
    public static final String TAG = "DrmUtil";

    public static int getDrmObjType(String ringType) {
        if (ringType.equals(Settings.System.RINGTONE)) {
            return DrmTrackerAdapter.PATH_TYPE_RING_TONG;
        } else if (ringType.equals(Settings.System.NOTIFICATION_SOUND)) {
            return DrmTrackerAdapter.PATH_TYPE_NOTIFICATION;
        } else {
            return DrmTrackerAdapter.PATH_TYPE_ALERT_ALARM;
        }
    }

    public static String getPathFromUri(Context context, String uriString) {
        if (uriString != null && !uriString.isEmpty()) {
            Uri uri = Uri.parse(uriString);
            String path = DrmUri.convertUriToPath(uri, context);
            return path;
        }
        return null;
    }

    public static long timeStringToSeconds(String timeStr) {
        logd("Time string to seconds. time:" + timeStr);
        long timeSeconds = 0;
        try {
            SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            formatter.setTimeZone(TimeZone.getDefault());
            timeSeconds = formatter.parse(timeStr).getTime();
        } catch (ParseException e1) {
            TctLog.e(TAG, "Parse endTimeStr failed! endTimeStr:" + timeStr);
            e1.printStackTrace();
            return timeSeconds;
        }
        logd("Time string to seconds. time:" + timeSeconds);
        return timeSeconds;
    }

    public static boolean isMediaDbReady(Context context) {
        String[] names = {
                Settings.System.NOTIFICATION_SOUND, Settings.System.ALARM_ALERT,
                Settings.System.RINGTONE
        };
        String uriStr = null;
        Uri uri;
        String path;
        for (String name : names) {
            uriStr = Settings.System.getString(context.getContentResolver(), name);
            if (uriStr != null && !uriStr.isEmpty()) {
                uri = Uri.parse(uriStr);
                path = DrmUri.convertUriToPath(uri, context);
                DrmUtil.logd("Get settings-" + name + "| path=" + path);
                if (path == null) {
                    return false;
                }
            }
        }
        return true;
    }

    public static String[] parserPath(String path) {
        String[] result = new String[2];
        int index = path.lastIndexOf('/');
        if (index != -1) {
            result[0] = path.substring(0, index + 1);
            result[1] = path.substring(path.lastIndexOf('/') + 1);
        } else {
            result[0] = "";
            result[1] = "";
        }
        return result;
    }

    public static long getAvaliableTime(String endTimeStr) {
        DrmUtil.logd("Get avaliable time :" + endTimeStr);
        long endTime = 0;
        endTime = DrmUtil.timeStringToSeconds(endTimeStr);

        Time time = new Time();
        time.setToNow();
        long currMillis = time.toMillis(false);
        DrmUtil.logd("timeZone: " + Time.getCurrentTimezone() + "time: " + time.toString()
                + "currMillis: " + currMillis);
        long delay = endTime - currMillis;
        DrmUtil.logd("Get avaliable time exit  avaliableTime:" + delay);
        return delay;
    }
    public static String getRingTypeName(int drmPathType){
        if (drmPathType == DrmTrackerAdapter.PATH_TYPE_RING_TONG) {
            return Settings.System.RINGTONE;
        } else if(drmPathType == DrmTrackerAdapter.PATH_TYPE_NOTIFICATION) {
            return Settings.System.NOTIFICATION_SOUND;
        } else {
            return Settings.System.ALARM_ALERT;
        }
    }
    public static String formatPath(String src){
        if(src == null || src.isEmpty()){
            return src;
        }
        String result = null;
        int index = src.indexOf('/');
        if(index == 0 || index == -1){
            return src;
        }else {
            result = '/'+ src;
            logd("formatPath---src:" +src+"| result:" +result);
            return result;
        }
    }

    public static void logd(String value) {
        if (!DEBUG) {
            return;
        }
        TctLog.d(TAG, value);
    }

    public static void logv(String value) {
        if (!DEBUG) {
            return;
        }
        TctLog.d(TAG, value);
    }
}
