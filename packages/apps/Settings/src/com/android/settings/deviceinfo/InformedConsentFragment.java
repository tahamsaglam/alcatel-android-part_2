package com.android.settings.deviceinfo;

import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.content.res.Resources;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.os.SystemProperties;

import com.android.settings.R;

public class InformedConsentFragment extends Fragment implements View.OnClickListener{

    private Context mContext = null;
    private static final String INFORMED_PROPERTY_NAME = "def.diagnostic.on";

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.informed_consent_layout, container, false);
        mContext = inflater.getContext();
        TextView informedCancel = (TextView)v.findViewById(R.id.informed_cancel);
        informedCancel.setOnClickListener(this);
        TextView informedOk = (TextView)v.findViewById(R.id.informed_ok);
        informedOk.setOnClickListener(this);
        TextView informedDescription = (TextView)v.findViewById(R.id.informed_consent_description);
        Resources res = this.getResources();
        informedDescription.setText(res.getString(R.string.informed_consent_description)
                + res.getString(R.string.informed_consent_description_plus));
        return v;
    }

    private void finishActivity(){
        Activity activity = this.getActivity();
        if(null != activity){
            activity.finish();
        }
    }

    @Override
    public void onClick(View v) {
        switch(v.getId()){
            case R.id.informed_cancel:
                if(SystemProperties.getBoolean(INFORMED_PROPERTY_NAME, false)){
                    SystemProperties.set(INFORMED_PROPERTY_NAME, "false");
                }
                finishActivity();
                break;
            case R.id.informed_ok:
                if(!SystemProperties.getBoolean(INFORMED_PROPERTY_NAME, false)){
                    SystemProperties.set(INFORMED_PROPERTY_NAME, "true");
                }
                finishActivity();
                break;
            default:
                break;
        }

    }

}
