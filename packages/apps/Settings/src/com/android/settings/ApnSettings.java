/*
 * Copyright (C) 2008 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.android.settings;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.BroadcastReceiver;
import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.Resources.NotFoundException;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Looper;
import android.os.Message;
import android.os.PersistableBundle;
import android.os.SystemProperties;
import android.os.UserHandle;
import android.os.UserManager;
import android.provider.Telephony;
import android.support.v7.preference.Preference;
import android.support.v7.preference.PreferenceGroup;
import android.support.v7.preference.PreferenceScreen;
import android.telephony.CarrierConfigManager;
import android.telephony.ServiceState;
import android.telephony.SubscriptionInfo;
import android.telephony.SubscriptionManager;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.widget.TextView;
import android.widget.Toast;

import com.android.internal.logging.MetricsProto.MetricsEvent;
import com.android.internal.telephony.PhoneConstants;
import com.android.internal.telephony.TelephonyIntents;
import com.android.internal.telephony.dataconnection.ApnSetting;
import com.android.internal.telephony.uicc.IccRecords;
import com.android.internal.telephony.uicc.UiccController;
import com.android.settingslib.RestrictedLockUtils.EnforcedAdmin;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;

//[SOLUTION]-Add-BEGIN by TCTNB.(JiangLong Pan), 08/12/2016, SOLUTION-2719318
//Porting APN setting solution
import android.database.ContentObserver;
import android.provider.Settings;
import android.telephony.PhoneStateListener;
import android.telephony.ServiceState;

import android.util.TctLog;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.RelativeLayout;

import com.android.settingslib.WirelessUtils;
//[SOLUTION]-Add-END by TCTNB.(JiangLong Pan)

public class ApnSettings extends RestrictedSettingsFragment implements
        Preference.OnPreferenceChangeListener {
    static final String TAG = "ApnSettings";

    public static final String EXTRA_POSITION = "position";
    public static final String RESTORE_CARRIERS_URI =
        "content://telephony/carriers/restore";
    public static final String PREFERRED_APN_URI =
        "content://telephony/carriers/preferapn";

    public static final String APN_ID = "apn_id";
    public static final String SUB_ID = "sub_id";
    public static final String MVNO_TYPE = "mvno_type";
    public static final String MVNO_MATCH_DATA = "mvno_match_data";

    private static final String APN_NAME_DM = "CMCC DM";

    private static final int ID_INDEX = 0;
    private static final int NAME_INDEX = 1;
    private static final int APN_INDEX = 2;
    private static final int TYPES_INDEX = 3;
    private static final int MVNO_TYPE_INDEX = 4;
    private static final int MVNO_MATCH_DATA_INDEX = 5;
    private static final int RO_INDEX = 6;
    private static final int BEARER_INDEX = 7;
    private static final int BEARER_BITMASK_INDEX = 8;
//[SOLUTION]-Add-BEGIN by TCTNB.(JiangLong Pan), 08/12/2016, SOLUTION-2719318
//Porting APN setting solution
    private static final int APNNEW_INDEX = 9; // Defect-2197942
//[SOLUTION]-Add-END by TCTNB.(JiangLong Pan)

    private static final int MENU_NEW = Menu.FIRST;
    private static final int MENU_RESTORE = Menu.FIRST + 1;

    private static final int EVENT_RESTORE_DEFAULTAPN_START = 1;
    private static final int EVENT_RESTORE_DEFAULTAPN_COMPLETE = 2;

//[SOLUTION]-Add-BEGIN by TCTNB.(JiangLong Pan), 08/12/2016, SOLUTION-2719318
//Porting APN setting solution
    private static final int EVENT_ENABLE_RESTORE_DEFAULT_MENU = 3; //Defect-1911892
//[SOLUTION]-Add-END by TCTNB.(JiangLong Pan)

    private static final int DIALOG_RESTORE_DEFAULTAPN = 1001;

    private static final Uri DEFAULTAPN_URI = Uri.parse(RESTORE_CARRIERS_URI);
    private static final Uri PREFERAPN_URI = Uri.parse(PREFERRED_APN_URI);

    private static boolean mRestoreDefaultApnMode;

    private UserManager mUserManager;
    private RestoreApnUiHandler mRestoreApnUiHandler;
    private RestoreApnProcessHandler mRestoreApnProcessHandler;
    private HandlerThread mRestoreDefaultApnThread;
    private SubscriptionInfo mSubscriptionInfo;
    private UiccController mUiccController;
    private String mMvnoType;
    private String mMvnoMatchData;

    private String mSelectedKey;

//[SOLUTION]-Add-BEGIN by TCTNB.(JiangLong Pan), 08/12/2016, SOLUTION-2719318
//Porting APN setting solution
    private static final int START_SIM_SERVICE = 1;
    private static final String SIM_SERVICE = "sim_service";

    //porting m8976 task-528291
    public static final String APN_CHECK_IMSI = "imsi";
    public static final String START_BY_POWER_ON = "startbypoweron";
    protected TelephonyManager mTelephonyManager;
    //protected int mSubId;
    protected Uri mUri;
    private Button mButtonOk = null;
    private View mExtraView = null;
    private static final String TRANSACTION_START = "com.android.mms.transaction.START";
    private static final String TRANSACTION_STOP = "com.android.mms.transaction.STOP";
    private static final String MMS_TRANSACTION = "mms.transaction";
    private static boolean sNotInMmsTransation = true;
    protected boolean mAirplaneModeEnabled = false;
    protected int mSelectableApnCount = 0;
    public static final String TETHER_TYPE = "tethering";
    protected boolean mIsTetherApn = false;
    private Boolean mIsStartByPowerOn = false;
    private Intent mReceiveIntent = null;

    //Defect 1490925:disable restore apn function after apn db changed
    private MenuItem mMenuRestore = null;
    private ApnChangeObserver mApnObserver;


    //porting m8976 task
    private String APN_TABLE_23430[][] = {
            { "EE", "Internet", "everywhere" },
            { "EE", "MMS", "eezone" },
            /* MODIFIED-BEGIN by fliu2, 2016-05-30,BUG-2202129*/
            { "BT", "BT Internet", "btmobile.bt.com" },
            { "BT", "BT MMS", "mms.bt.com" },
            { "BT OnePhone", "BT OnePhone Internet", "internet.btonephone.com" },
            { "BT OnePhone", "BT OnePhone MMS", "mms.btonephone.com" },
            { "Virgin Mobile", "Virgin Media Mobile Internet", "goto.virginmobile.uk" },
            { "Virgin Mobile", "Virgin MMS", "goto.virginmobile.uk" },
            { "Asda Mobile", "Asda Internet", "everywhere" },
            { "Asda Mobile", "Asda MMS", "eezone" }, };

    private String APN_TABLE_23433[][] = {
            { "EE", "Internet", "everywhere" },
            { "EE", "MMS", "eezone" },};

    private String APN_TABLE_23410[][] = {
            { "O2 Pay Monthly", "O2 Mobile Web", "mobile.o2.co.uk" },
            { "O2 Pay Monthly", "O2 MMS", "wap.o2.co.uk" },
            //{ "O2 Pay Monthly", "O2 MMS", "payandgo.o2.co.uk" },
            { "O2 Pay & Go", "O2 Pay & Go", "payandgo.o2.co.uk" },
            { "TESCO", "Tesco WAP GPRS", "prepay.tesco-mobile.com" },
            { "TESCO", "Tesco MMS", "prepay.tesco-mobile.com" },
            { "giffgaff", "giffgaff", "giffgaff.com" },
            { "TalkTalk Mobile", "TalkTalk", "mobile.talktalk.co.uk" },};

    //Fix TalkTalk to TalKTalk WAP
    private String APN_TABLE_23415[][] = {
            { "Vodafone Contract", "Contract WAP", "wap.vodafone.co.uk" },
            { "Vodafone PAYG", "PAYG WAP", "pp.vodafone.co.uk" },
            { "Talkmobile Contract", "Talkmobile Internet", "talkmobile.co.uk" },
            { "Talkmobile Contract", "Talkmobile MMS", "talkmobile.co.uk" },
            { "Talkmobile PAYG", "Talkmobile PAYG Internet", "payg.talkmobile.co.uk" },
            { "Talkmobile PAYG", "Talkmobile PAYG MMS", "payg.talkmobile.co.uk" },
            { "TalkTalk Mobile", "TalkTalk", "mobile.talktalk.co.uk" },
            { "BT Mobile", "BT Mobile Internet", "btmobile.bt.com" },
            { "Lebara", "Lebara", "uk.lebara.mobi" },
            { "Sainsbury's PAYG", "Sainsbury's PAYG", "payg.mobilebysainsburys.co.uk" },
            { "Sainsbury's PAYG", "Sainsbury's PAYG MMS", "payg.mobilebysainsburys.co.uk" },
            { "Sainsbury's Contract", "Sainsbury's Contract", "mobilebysainsburys.co.uk" },
            { "Sainsbury's Contract", "Sainsbury's Contract MMS", "mobilebysainsburys.co.uk" },};

    // fix H3G UK to 3
    private String APN_TABLE_23420[][] = {
            { "3", "3", "three.co.uk"},
            { "iD", "iD", "iD"},};

    private String[][] getApnTableForUKOp(String operatorNum) {
        if ("23430".equals(operatorNum)) {
            return APN_TABLE_23430;
        } else if ("23433".equals(operatorNum)) {
            return APN_TABLE_23433;
        } else if ("23410".equals(operatorNum)) {
            return APN_TABLE_23410;
        } else if ("23415".equals(operatorNum)) {
            return APN_TABLE_23415;
        } else if ("23420".equals(operatorNum)) {
            return APN_TABLE_23420;
        }
        return null;
    }

    // O2 SIM auto-Detecting
    private boolean isSkipO2Apn(String apn, String simOp) {
        Log.d(TAG, "isSkipO2Apn, O2 card, Prepay or Postpay: " + simOp);
        if (!TextUtils.isEmpty(simOp)) {
            if (simOp.equals("prepay")) {
                Log.d(TAG, "isSkipO2Apn, Prepay");
                if (apn.equalsIgnoreCase("mobile.o2.co.uk")
                        || apn.equalsIgnoreCase("wap.o2.co.uk")
                        || apn.equalsIgnoreCase("giffgaff.com")
                        || apn.equalsIgnoreCase("m-bb.o2.co.uk")
                        || apn.equalsIgnoreCase("prepay.tesco-mobile.com")) {
                    Log.d(TAG, "isSkipO2Apn, Removed one non-prepay apn: " + apn);
                    return true;
                }
            } else if (simOp.equals("postpay")) {
                Log.d(TAG, "isSkipO2Apn, Postpay");
                if (apn.equalsIgnoreCase("payandgo.o2.co.uk")
                        || apn.equalsIgnoreCase("giffgaff.com")
                        || apn.equalsIgnoreCase("m-bb.o2.co.uk")
                        || apn.equalsIgnoreCase("prepay.tesco-mobile.com")) {
                    Log.d(TAG, "isSkipO2Apn, Removed one non-postpay apn: " + apn);
                    return true;
                }
            } else if (simOp.equals("tesco")) {
                Log.d(TAG, "isSkipO2Apn, tesco");
                if (apn.equalsIgnoreCase("mobile.o2.co.uk")
                        || apn.equalsIgnoreCase("wap.o2.co.uk")
                        || apn.equalsIgnoreCase("giffgaff.com")
                        || apn.equalsIgnoreCase("m-bb.o2.co.uk")
                        || apn.equalsIgnoreCase("payandgo.o2.co.uk")) {
                    Log.d(TAG, "isSkipO2Apn, Removed one non-tesco apn: " + apn);
                    return true;
                }
            } else if (simOp.equals("broadband")) {
                Log.d(TAG, "isSkipO2Apn, broadband");
                if (apn.equalsIgnoreCase("mobile.o2.co.uk")
                        || apn.equalsIgnoreCase("wap.o2.co.uk")
                        || apn.equalsIgnoreCase("giffgaff.com")
                        || apn.equalsIgnoreCase("prepay.tesco-mobile.com")
                        || apn.equalsIgnoreCase("payandgo.o2.co.uk")) {
                    Log.d(TAG, "isSkipO2Apn, Removed one non-broadband apn: " + apn);
                    return true;
                }
            } else if (simOp.equals("giffgaff")) {
                Log.d(TAG, "isSkipO2Apn, giffgaff");
                if (apn.equalsIgnoreCase("mobile.o2.co.uk")
                        || apn.equalsIgnoreCase("wap.o2.co.uk")
                        || apn.equalsIgnoreCase("m-bb.o2.co.uk")
                        || apn.equalsIgnoreCase("prepay.tesco-mobile.com")
                        || apn.equalsIgnoreCase("payandgo.o2.co.uk")) {
                    Log.d(TAG, "isSkipO2Apn, Removed one non-giffgaff apn: " + apn);
                    return true;
                }
            }
        }
        return false;
    }
//[SOLUTION]-Add-END by TCTNB.(JiangLong Pan)

    private IntentFilter mMobileStateFilter;

    private boolean mUnavailable;

    private boolean mHideImsApn;
    private boolean mAllowAddingApns;
    private boolean mApnSettingsHidden;

    protected int mSubId; // MODIFIED by sunyandong, 2016-08-08,BUG-2694521

    public ApnSettings() {
        super(UserManager.DISALLOW_CONFIG_MOBILE_NETWORKS);
    }

    private HashSet mIccidSet;

    private final BroadcastReceiver mMobileStateReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction().equals(
                    TelephonyIntents.ACTION_ANY_DATA_CONNECTION_STATE_CHANGED)) {
                PhoneConstants.DataState state = getMobileDataState(intent);
//[SOLUTION]-Add-BEGIN by TCTNB.(JiangLong Pan), 08/12/2016, SOLUTION-2719318
//Porting APN setting solution
                int subId = intent.getIntExtra(PhoneConstants.SUBSCRIPTION_KEY,
                        SubscriptionManager.INVALID_SUBSCRIPTION_ID);

                switch (state) {
                case CONNECTED:
                    //disable restore apn function after apn db changed,2016/03/21
                    if (mMenuRestore != null) {
                        Log.d(TAG, "set restore menu as true.");
                        mMenuRestore.setEnabled(true);
                    }

                    if (!mRestoreDefaultApnMode) {
                        Log.d(TAG, "fillList() from broadcastreceiver, and subid = " + subId);
                        fillList();
                    }
//[SOLUTION]-Add-END by TCTNB.(JiangLong Pan)
                    break;
                }
//[SOLUTION]-Add-BEGIN by TCTNB.(JiangLong Pan), 08/12/2016, SOLUTION-2719318
//Porting APN setting solution
            //porting m8976 task-528291
            } else if (intent.getAction().equals(TRANSACTION_START)) {
                sNotInMmsTransation = false;
                getPreferenceScreen().setEnabled(false);
            } else if (intent.getAction().equals(TRANSACTION_STOP)) {
                sNotInMmsTransation = true;
                getPreferenceScreen().setEnabled(getScreenEnableState());
            }else if (intent.getAction().equals(Intent.ACTION_AIRPLANE_MODE_CHANGED)) {
                mAirplaneModeEnabled = intent.getBooleanExtra("state", false);
                getPreferenceScreen().setEnabled(!mAirplaneModeEnabled);
            }
//[SOLUTION]-Add-END by TCTNB.(JiangLong Pan)
        }
    };

    private static PhoneConstants.DataState getMobileDataState(Intent intent) {
        String str = intent.getStringExtra(PhoneConstants.STATE_KEY);
        if (str != null) {
            return Enum.valueOf(PhoneConstants.DataState.class, str);
        } else {
            return PhoneConstants.DataState.DISCONNECTED;
        }
    }

    @Override
    protected int getMetricsCategory() {
        return MetricsEvent.APN;
    }

    @Override
    public void onCreate(Bundle icicle) {
        super.onCreate(icicle);
        final Activity activity = getActivity();
        final int subId = activity.getIntent().getIntExtra(SUB_ID,
                SubscriptionManager.INVALID_SUBSCRIPTION_ID);
        fillOperatorIccidset();
        Log.d(TAG, "onCreate: subId = " + subId);


        mMobileStateFilter = new IntentFilter(
                TelephonyIntents.ACTION_ANY_DATA_CONNECTION_STATE_CHANGED);

//[SOLUTION]-Add-BEGIN by TCTNB.(JiangLong Pan), 08/12/2016, SOLUTION-2719318
//Porting APN setting solution
        //porting m8976 task-528291
        mMobileStateFilter.addAction(Intent.ACTION_AIRPLANE_MODE_CHANGED);
        mMobileStateFilter.addAction(TelephonyIntents.ACTION_SIM_STATE_CHANGED);
        mMobileStateFilter.addAction(TRANSACTION_START);
        mMobileStateFilter.addAction(TRANSACTION_STOP);
//[SOLUTION]-Add-END by TCTNB.(JiangLong Pan)

        setIfOnlyAvailableForAdmins(true);

        mSubscriptionInfo = SubscriptionManager.from(activity).getActiveSubscriptionInfo(subId);

//[SOLUTION]-DEL-BEGIN by TCTNB.(JiangLong Pan), 08/12/2016, SOLUTION-2719318
//Porting APN setting solution
        //mUiccController = UiccController.getInstance();
//[SOLUTION]-DEL-END by TCTNB.(JiangLong Pan)

        CarrierConfigManager configManager = (CarrierConfigManager)
                getSystemService(Context.CARRIER_CONFIG_SERVICE);
        PersistableBundle b = configManager.getConfig();
        mHideImsApn = b.getBoolean(CarrierConfigManager.KEY_HIDE_IMS_APN_BOOL);
        mAllowAddingApns = b.getBoolean(CarrierConfigManager.KEY_ALLOW_ADDING_APNS_BOOL);
        mUserManager = UserManager.get(activity);

//[SOLUTION]-Add-BEGIN by TCTNB.(JiangLong Pan), 08/12/2016, SOLUTION-2719318
//Porting APN setting solution
        //Task1490925, disable restore apn function after apn db changed, 2016/04/08
        mApnObserver = new ApnChangeObserver();
        this.getContentResolver().registerContentObserver(
                Telephony.Carriers.CONTENT_URI, true, mApnObserver);
        mRestoreApnUiHandler = new RestoreApnUiHandler();
//[SOLUTION]-Add-END by TCTNB.(JiangLong Pan)
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        getEmptyTextView().setText(R.string.apn_settings_not_available);
        mUnavailable = isUiRestricted();
        setHasOptionsMenu(!mUnavailable);
        if (mUnavailable) {
            setPreferenceScreen(new PreferenceScreen(getPrefContext(), null));
            getPreferenceScreen().removeAll();
            return;
        }

        addPreferencesFromResource(R.xml.apn_settings);

        /* MODIFIED-BEGIN by sunyandong, 2016-08-08,BUG-2694521*/
        mSubId = getActivity().getIntent().getIntExtra(SUB_ID,
                SubscriptionManager.getDefaultDataSubscriptionId());
                /* MODIFIED-END by sunyandong,BUG-2694521*/

//[SOLUTION]-Add-BEGIN by TCTNB.(JiangLong Pan), 08/12/2016, SOLUTION-2719318
//Porting APN setting solution
        mUiccController = UiccController.getInstance();

        //porting m8976 task-528291
        initSimState();
        mIsStartByPowerOn = false;
        Bundle tempbundle = getActivity().getIntent().getExtras();
        if (tempbundle != null) {
            mIsStartByPowerOn = tempbundle.getBoolean(START_BY_POWER_ON, false);
        }
        if (mIsStartByPowerOn == true) {
            setView();// add one button "Ok" in the APN selection screen.
        }
        mTelephonyManager = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
        mTelephonyManager.listen(mPhoneStateListener, PhoneStateListener.LISTEN_SERVICE_STATE);
//[SOLUTION]-Add-END by TCTNB.(JiangLong Pan)
    }

    @Override
    public void onStop() {
        super.onStop();
        mApnSettingsHidden = true;
    }

    @Override
    public void onResume() {
        super.onResume();

        if (mUnavailable) {
            return;
        }

        getActivity().registerReceiver(mMobileStateReceiver, mMobileStateFilter);

//[SOLUTION]-Add-BEGIN by TCTNB.(JiangLong Pan), 08/12/2016, SOLUTION-2719318
//Porting APN setting solution
        //porting m8976 task-528291
        mAirplaneModeEnabled = WirelessUtils.isAirplaneModeOn(getActivity());
        sNotInMmsTransation = Settings.System.getInt(this.getContentResolver(),MMS_TRANSACTION, 0) == 0;

        Log.d(TAG, "onResume()>>> mRestoreDefaultApnMode = " + mRestoreDefaultApnMode);
//[SOLUTION]-Add-END by TCTNB.(JiangLong Pan)

        if (!mRestoreDefaultApnMode) {
            fillList();
        }
        mApnSettingsHidden = false;
    }

    @Override
    public void onPause() {
        super.onPause();

        if (mUnavailable) {
            return;
        }

        getActivity().unregisterReceiver(mMobileStateReceiver);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        if (mRestoreDefaultApnThread != null) {
            mRestoreDefaultApnThread.quit();
        }

//[SOLUTION]-Add-BEGIN by TCTNB.(JiangLong Pan), 08/12/2016, SOLUTION-2719318
//Porting APN setting solution
        //porting m8976 task-528291
        if ((mIsStartByPowerOn == true) && (mReceiveIntent != null)) {
            startActivity(mReceiveIntent);
            mReceiveIntent = null;
        }

        this.getContentResolver().unregisterContentObserver(mApnObserver);
//[SOLUTION]-Add-END by TCTNB.(JiangLong Pan)
    }

    @Override
    public EnforcedAdmin getRestrictionEnforcedAdmin() {
        final UserHandle user = UserHandle.of(mUserManager.getUserHandle());
        if (mUserManager.hasUserRestriction(UserManager.DISALLOW_CONFIG_MOBILE_NETWORKS, user)
                && !mUserManager.hasBaseUserRestriction(UserManager.DISALLOW_CONFIG_MOBILE_NETWORKS,
                        user)) {
            return EnforcedAdmin.MULTIPLE_ENFORCED_ADMIN;
        }
        return null;
    }

    private void fillOperatorIccidset(){
        mIccidSet = new HashSet<String>();
        mIccidSet.add("8991840");
        mIccidSet.add("8991854");
        mIccidSet.add("8991855");
        mIccidSet.add("8991856");
        mIccidSet.add("8991857");
        mIccidSet.add("8991858");
        mIccidSet.add("8991859");
        mIccidSet.add("899186");
        mIccidSet.add("8991870");
        mIccidSet.add("8991871");
        mIccidSet.add("8991872");
        mIccidSet.add("8991873");
        mIccidSet.add("8991874");
    }

    private void fillList() {
        boolean isSelectedKeyMatch = false;//ADD by dingyi 2016/10/26 for defect 3155543
        final TelephonyManager tm = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
        final String mccmnc = mSubscriptionInfo == null ? ""
                : tm.getSimOperator(mSubscriptionInfo.getSubscriptionId());
        Log.d(TAG, "mccmnc = " + mccmnc);

//[SOLUTION]-Add-BEGIN by TCTNB.(JiangLong Pan), 08/12/2016, SOLUTION-2719318
//Porting APN setting solution
        if (mccmnc == null || mccmnc.equals("")) {
            getEmptyTextView().setText(R.string.apn_settings_not_available);
        }
//[SOLUTION]-Add-END by TCTNB.(JiangLong Pan)

        StringBuilder where = new StringBuilder("numeric=\"" + mccmnc +
                "\" AND NOT (type='ia' AND (apn=\"\" OR apn IS NULL)) AND user_visible!=0");


//[SOLUTION]-Add-BEGIN by TCTNB.(JiangLong Pan), 08/12/2016, SOLUTION-2719318
//Porting APN setting solution
        boolean WifiCallingEnabled = "TRUE".equalsIgnoreCase(SystemProperties.get("persist.data.iwlan.enable"));
        boolean VoLTEenabled = "TRUE".equalsIgnoreCase(SystemProperties.get("persist_radio_VOLTE_ENABLE"));
        Log.d(TAG, "WifiCallingEnabled = " + WifiCallingEnabled + ", VoLTEenabled = " + VoLTEenabled);

        //if (mHideImsApn) {
        if (mHideImsApn || !(WifiCallingEnabled || VoLTEenabled)) {
            where.append(" AND NOT (type='ims')");
        }

        //defect-1132290
        int phoneNum = TelephonyManager.getDefault().getPhoneCount();
        //[bugfix]-add-begin by junyong.sun 02/11/2016
        if(phoneNum > 1 || phoneNum == 1) {
            StringBuilder selection = new StringBuilder("(" + "apnnew =\"" + "-1" + "\"");
            selection.append(" or sub_id =\"" + String.valueOf(mSubId) + "\"" + ")");
            where = selection.append(" and " ).append(where);
            Log.d(TAG, "Selection is: " + where);
        }
        //[bugfix]-add-end by junyong.sun 02/11/2016

        if (SystemProperties.getBoolean("persist.sys.hideapn", true)) {
            Log.d(TAG, "hiden apn feature enable.");
            // remove the filtered items, no need to show in UI

            if(getResources().getBoolean(R.bool.config_hide_ims_apns)){
                mHideImsApn = true;
            }

            // Filer fota and dm for specail carrier
            if (getResources().getBoolean(R.bool.config_hide_dm_enabled)) {
                for (String plmn : getResources().getStringArray(R.array.hidedm_plmn_list)) {
                    if (plmn.equals(mccmnc)) {
                        where.append(" and name <>\"" + APN_NAME_DM + "\"");
                        break;
                    }
                }
            }

            if (getResources().getBoolean(R.bool.config_hidesupl_enable)) {
                boolean needHideSupl = false;
                for (String plmn : getResources().getStringArray(R.array.hidesupl_plmn_list)) {
                    if (plmn.equals(mccmnc)) {
                        needHideSupl = true;
                        break;
                    }
                }

                if (needHideSupl) {
                    where.append(" and type <>\"" + PhoneConstants.APN_TYPE_SUPL + "\"");
                }
            }

            // Hide mms if config is true
            if (getResources().getBoolean(R.bool.config_hide_mms_enable)) {
                  where.append( " and type <>\"" + PhoneConstants.APN_TYPE_MMS + "\"");
            }
        }

        if(getResources().getBoolean(R.bool.config_regional_hide_ims_and_dun_apns)){
            where.append(" AND type <>\"" + PhoneConstants.APN_TYPE_DUN + "\"");
            where.append(" AND type <>\"" + PhoneConstants.APN_TYPE_IMS + "\"");
        }
        if (mHideImsApn) {
            where.append(" AND NOT (type='ims')");
        }

        if (isOperatorIccId()) {
            where.append(" AND type <>\"" + PhoneConstants.APN_TYPE_EMERGENCY + "\"");
            where.append(" AND type <>\"" + PhoneConstants.APN_TYPE_IMS + "\"");
        }
        Log.d(TAG, "where---" + where);

        Cursor cursor = getContentResolver().query(Telephony.Carriers.CONTENT_URI, new String[] {
                "_id", "name", "apn", "type", "mvno_type", "mvno_match_data", "read_only", "bearer",
                "bearer_bitmask", "apnnew"}, where.toString(), null, Telephony.Carriers.DEFAULT_SORT_ORDER);
//[SOLUTION]-Add-END by TCTNB.(JiangLong Pan)

        if (cursor != null) {
            IccRecords r = null;
            if (mUiccController != null && mSubscriptionInfo != null) {
                r = mUiccController.getIccRecords(SubscriptionManager.getPhoneId(
                        mSubscriptionInfo.getSubscriptionId()), UiccController.APP_FAM_3GPP);
            }
            PreferenceGroup apnList = (PreferenceGroup) findPreference("apn_list");
            apnList.removeAll();

//[SOLUTION]-Add-BEGIN by TCTNB.(JiangLong Pan), 08/12/2016, SOLUTION-2719318
//Porting APN setting solution
            //porting m8976 task-528291
            boolean keySetChecked = false;
//[SOLUTION]-Add-END by TCTNB.(JiangLong Pan)

            ArrayList<ApnPreference> mnoApnList = new ArrayList<ApnPreference>();
            ArrayList<ApnPreference> mvnoApnList = new ArrayList<ApnPreference>();
            ArrayList<ApnPreference> mnoMmsApnList = new ArrayList<ApnPreference>();
            ArrayList<ApnPreference> mvnoMmsApnList = new ArrayList<ApnPreference>();

//[SOLUTION]-Add-BEGIN by TCTNB.(JiangLong Pan), 08/12/2016, SOLUTION-2719318
//Porting APN setting solution:Task-667610
            // O2 SIM auto-Detecting
            boolean isUKHomologationenable = getResources().getBoolean(
                    com.android.internal.R.bool.feature_tctfw_preypaypostpay_on);
            String payKind = null;
            if (isUKHomologationenable) {
                payKind = SystemProperties.get("gsm.sim.operator.paykind");
            }
//[SOLUTION]-Add-END by TCTNB.(JiangLong Pan)

            mSelectedKey = getSelectedApnKey();
            Log.d(TAG, "mSelectedKey  = " + mSelectedKey); // MODIFIED by wei.zhang-nb, 2016-11-07,BUG-3312022
            cursor.moveToFirst();
            while (!cursor.isAfterLast()) {
                String name = cursor.getString(NAME_INDEX);
                String apn = cursor.getString(APN_INDEX);
                String key = cursor.getString(ID_INDEX);
                String type = cursor.getString(TYPES_INDEX);
                String mvnoType = cursor.getString(MVNO_TYPE_INDEX);
                String mvnoMatchData = cursor.getString(MVNO_MATCH_DATA_INDEX);
                boolean readOnly = (cursor.getInt(RO_INDEX) == 1);
                String localizedName = getLocalizedName(getActivity(), cursor, NAME_INDEX);
                if (!TextUtils.isEmpty(localizedName)) {
                    name = localizedName;
                }

//[SOLUTION]-Add-BEGIN by TCTNB.(JiangLong Pan), 08/12/2016, SOLUTION-2719318
//Porting APN setting solution
                int apn_new = cursor.getInt(APNNEW_INDEX);
                Log.d(TAG, "name  = " + name + ", apn = " + apn + ", key = " + key +" , type = " + type +
                    ", mvnoType = " + mvnoType + ", mvnoMatchData = " + mvnoMatchData + ", readOnly = " + readOnly +
                    ", apn_new = " + apn_new);

                //porting m8976 task-528291
                if (mIsTetherApn && !TETHER_TYPE.equals(type)) {
                    cursor.moveToNext();
                    continue;
                }

                //defect-2112285:Hide APN SUPL for PLMN 334090,33405 and 334050
                if (getResources().getBoolean(R.bool.def_cfg_hide_iusacell_supl_enabled)) {
                    if (("334090".equals(mccmnc) || "33405".equals(mccmnc) || "334050".equals(mccmnc)) &&
                        type.contains(PhoneConstants.APN_TYPE_SUPL)) {
                        cursor.moveToNext();
                        continue;
                    }
                }

                int bearer = cursor.getInt(BEARER_INDEX);
                int bearerBitMask = cursor.getInt(BEARER_BITMASK_INDEX);
                int fullBearer = bearer | bearerBitMask;
                int subId = mSubscriptionInfo != null ? mSubscriptionInfo.getSubscriptionId()
                        : SubscriptionManager.INVALID_SUBSCRIPTION_ID;
                int radioTech = networkTypeToRilRidioTechnology(TelephonyManager.getDefault()
                        .getDataNetworkType(subId));
                if (!ServiceState.bitmaskHasTech(fullBearer, radioTech)
                        && (bearer != 0 || bearerBitMask != 0)) {
                    // In OOS, show APN with bearer as default
                    if ((radioTech != ServiceState.RIL_RADIO_TECHNOLOGY_UNKNOWN) || (bearer == 0
                            && radioTech == ServiceState.RIL_RADIO_TECHNOLOGY_UNKNOWN)) {
                        cursor.moveToNext();
                        continue;
                    }
                }

                //[TASK]-Add-BEGIN by TCTNB.jie.qiu, 09/01/2016, Task-2828321, do not display vodafone apn when ctcc card inserted, solution-2701104
                boolean isOldApn = cursor.getInt(APNNEW_INDEX) == -1;
                if (r != null && "20404".equals(mccmnc) && isOldApn)
                {
                    Log.d(TAG, "mccmnc 20404, prebuilt apn");
                    boolean isCTCCIINMatched = false;
                    String iccId = r.getIccId();
                    if (iccId != null) {
                        String ctccIINs = getResources().getString(com.android.internal.R.string.def_tctfw_ctcc_iins);

                        if ("*".equalsIgnoreCase(ctccIINs)) {
                            Log.d(TAG, "ctcc iin match all set");
                            isCTCCIINMatched = true;
                        } else {
                            String[] ctccIINList = ctccIINs.split(",");
                            for (String iin : ctccIINList) {
                                Log.d(TAG, "iin: " + iin);
                                if (iccId.startsWith(iin)) {
                                    Log.d(TAG, "ctcc iin match found");
                                    isCTCCIINMatched = true;
                                    break;
                                }
                            }
                        }

                        //got ctcc roaming card, skip non-ctcc apns and ctcc mms apn
                        if (isCTCCIINMatched && !apn.equalsIgnoreCase("ctnet"))
                        {
                            cursor.moveToNext();
                            continue;
                        }
                        //got vodafone card, skip ctcc apns
                        else if (!isCTCCIINMatched && (apn.equalsIgnoreCase("ctnet") || apn.equalsIgnoreCase("ctwap")))
                        {
                            cursor.moveToNext();
                            continue;
                        }
                    }
                }
                //[TASK]-Add-END by TCTNB.jie.qiu

                //porting m8976 Task-667610
                if (getResources().getBoolean(
                        com.android.internal.R.bool.feature_tctfw_openmarketsimservice_on)
                        && "false".equalsIgnoreCase(SystemProperties.get("dualsim.ui.support",
                                "false")) && (apn_new == -1)) {
                    String[][] apnTable = getApnTableForUKOp(mccmnc);
                    String currentOperator = Settings.Global.getString(getContentResolver(),
                            SIM_SERVICE);
                    Log.d(TAG, "langyuntian apnsettings operatorNum " + mccmnc
                            + "SIM_SERVICE=" + currentOperator);
                    if (apnTable != null && currentOperator != null && !currentOperator.isEmpty()) {
                        Log.d(TAG, "apnTable currentOperator = " + currentOperator);
                        boolean flag = false;
                        for (String[] apn_record : apnTable) {
                            if (currentOperator.toLowerCase().equals(apn_record[0].toLowerCase())
                                    && apn.toLowerCase().equals(apn_record[2].toLowerCase())
                                    && name.toLowerCase().equals(apn_record[1].toLowerCase())) {
                                TctLog.d("csy", "apn_record = " + apn_record);
                                flag = true;
                                break;
                            }
                        }
                        if (!flag) {
                            cursor.moveToNext();
                            continue;
                        }
                    }
                }

                // O2 SIM auto-Detecting
                if (isUKHomologationenable) {
                    if ("23410".equals(mccmnc)) {
                        // Skip O2 APN according to card type
                        if (isSkipO2Apn(apn, payKind)) {
                            cursor.moveToNext();
                            continue;
                        }
                    }
                }

                ApnPreference pref = new ApnPreference(getPrefContext());

                pref.setApnReadOnly(readOnly);
                pref.setKey(key);
                pref.setTitle(name);
                pref.setSummary(apn);
                pref.setPersistent(false);
                pref.setOnPreferenceChangeListener(this);

                pref.setSubId(mSubId);

                //porting m8976 task-528291
                //boolean selectable = ((type == null) || !type.equals("mms"));
                //All tether apn will be selectable for others , mms will not be selectable.
                boolean selectable = true;
                if (TETHER_TYPE.equals(type)) {
                    selectable = mIsTetherApn;
                } else {
                    selectable = !"mms".equals(type);
                }

                if (name.equals("Orange Internet") && SystemProperties.get("persist.orangeapn.selectable").equalsIgnoreCase("false")) {
                   selectable=false;
                }

                if ("20810".equals(mccmnc) && type.equals(PhoneConstants.APN_TYPE_DUN)) {
                   selectable = false;
                }

                pref.setSelectable(selectable);
                if (selectable) {
                    if ((mSelectedKey != null) && mSelectedKey.equals(key)) {
                        pref.setChecked();
                        isSelectedKeyMatch = true;//ADD by dingyi 2016/10/26 for defect 3155543
//[SOLUTION]-Add-BEGIN by TCTNB.(JiangLong Pan), 08/12/2016, SOLUTION-2719318
//Porting APN setting solution
                        Log.d(TAG, "find select key = " + mSelectedKey);
                        keySetChecked = true;
//[SOLUTION]-Add-END by TCTNB.(JiangLong Pan)
                    }
                    addApnToList(pref, mnoApnList, mvnoApnList, r, mvnoType, mvnoMatchData, apn_new);
                } else {
                    addApnToList(pref, mnoMmsApnList, mvnoMmsApnList, r, mvnoType, mvnoMatchData, apn_new);
                }
                cursor.moveToNext();
            }
            cursor.close();

            if (!mvnoApnList.isEmpty()) {
                mnoApnList = mvnoApnList;
                mnoMmsApnList = mvnoMmsApnList;

                // Also save the mvno info
            }

            if (!mvnoMmsApnList.isEmpty()) {
                mnoMmsApnList = mvnoMmsApnList;
            }
//[SOLUTION]-Add-END by TCTNB.(JiangLong Pan)

            for (Preference preference : mnoApnList) {
                apnList.addPreference(preference);
            }
            //ADD begin by dingyi 2016/10/26 for defect 3155543
            if (!isSelectedKeyMatch && apnList.getPreferenceCount() > 0) {
                ApnPreference pref = (ApnPreference) apnList.getPreference(0);
                pref.setChecked();
                setSelectedApnKey(pref.getKey());
                Log.d(TAG, "set key to  " +pref.getKey());
            }
            //ADD end by dingyi 2016/10/26 for defect 3155543
            for (Preference preference : mnoMmsApnList) {
                apnList.addPreference(preference);
            }
//[SOLUTION]-Add-BEGIN by TCTNB.(JiangLong Pan), 08/12/2016, SOLUTION-2719318
//Porting APN setting solution
            getPreferenceScreen().setEnabled(getScreenEnableState());
//[SOLUTION]-Add-END by TCTNB.(JiangLong Pan)
        }
    }

    private boolean isOperatorIccId(){
        final String iccid = mSubscriptionInfo == null ? ""
                : mSubscriptionInfo.getIccId();
        Iterator<String> itr = mIccidSet.iterator();
        while (itr.hasNext()) {
            if (iccid.contains(itr.next())) {
                return true;
            }
        }
        return false;
    }

    private int networkTypeToRilRidioTechnology(int nt) {
        switch(nt) {
            case TelephonyManager.NETWORK_TYPE_GPRS:
                return ServiceState.RIL_RADIO_TECHNOLOGY_GPRS;
            case TelephonyManager.NETWORK_TYPE_EDGE:
                return ServiceState.RIL_RADIO_TECHNOLOGY_EDGE;
            case TelephonyManager.NETWORK_TYPE_UMTS:
                return ServiceState.RIL_RADIO_TECHNOLOGY_UMTS;
            case TelephonyManager.NETWORK_TYPE_HSDPA:
                return ServiceState.RIL_RADIO_TECHNOLOGY_HSDPA;
            case TelephonyManager.NETWORK_TYPE_HSUPA:
                return ServiceState.RIL_RADIO_TECHNOLOGY_HSUPA;
            case TelephonyManager.NETWORK_TYPE_HSPA:
                return ServiceState.RIL_RADIO_TECHNOLOGY_HSPA;
            case TelephonyManager.NETWORK_TYPE_CDMA:
                return ServiceState.RIL_RADIO_TECHNOLOGY_IS95B;
            case TelephonyManager.NETWORK_TYPE_1xRTT:
                return ServiceState.RIL_RADIO_TECHNOLOGY_1xRTT;
            case TelephonyManager.NETWORK_TYPE_EVDO_0:
                return ServiceState.RIL_RADIO_TECHNOLOGY_EVDO_0;
            case TelephonyManager.NETWORK_TYPE_EVDO_A:
                return ServiceState.RIL_RADIO_TECHNOLOGY_EVDO_A;
            case TelephonyManager.NETWORK_TYPE_EVDO_B:
                return ServiceState.RIL_RADIO_TECHNOLOGY_EVDO_B;
            case TelephonyManager.NETWORK_TYPE_EHRPD:
                return ServiceState.RIL_RADIO_TECHNOLOGY_EHRPD;
            case TelephonyManager.NETWORK_TYPE_LTE:
                return ServiceState.RIL_RADIO_TECHNOLOGY_LTE;
            case TelephonyManager.NETWORK_TYPE_HSPAP:
                return ServiceState.RIL_RADIO_TECHNOLOGY_HSPAP;
            case TelephonyManager.NETWORK_TYPE_GSM:
                return ServiceState.RIL_RADIO_TECHNOLOGY_GSM;
            case TelephonyManager.NETWORK_TYPE_TD_SCDMA:
                return ServiceState.RIL_RADIO_TECHNOLOGY_TD_SCDMA;
            case TelephonyManager.NETWORK_TYPE_IWLAN:
                return ServiceState.RIL_RADIO_TECHNOLOGY_IWLAN;
            case TelephonyManager.NETWORK_TYPE_LTE_CA:
                return ServiceState.RIL_RADIO_TECHNOLOGY_LTE_CA;
            default:
                return ServiceState.RIL_RADIO_TECHNOLOGY_UNKNOWN;
        }
    }

//[SOLUTION]-Add-BEGIN by TCTNB.(JiangLong Pan), 08/12/2016, SOLUTION-2719318
//Porting APN setting solution
    public static String getLocalizedName(Context context, Cursor cursor, int index) {
        // If can find a localized name, replace the APN name with it
        String resName = cursor.getString(index);
        String localizedName = null;
        if (resName != null && !resName.isEmpty()) {
            int resId = context.getResources().getIdentifier(resName, "string",
                    context.getPackageName());
            try {
                localizedName = context.getResources().getString(resId);
                Log.d(TAG, "Replaced apn name with localized name");
            } catch (NotFoundException e) {
                Log.e(TAG, "Got execption while getting the localized apn name.", e);
            }
        }
        return localizedName;
    }

    private void addApnToList(ApnPreference pref, ArrayList<ApnPreference> mnoList,
                              ArrayList<ApnPreference> mvnoList, IccRecords r, String mvnoType,
                              String mvnoMatchData, int apnNew) { //Defect-2197942
//[SOLUTION]-Add-END by TCTNB.(JiangLong Pan)
        /* MODIFIED-BEGIN by bo.chen, 2016-09-06,BUG-2851269*/
        if (getResources().getBoolean(R.bool.def_settings_mvno_filter_enable)
                && r != null && !TextUtils.isEmpty(mvnoType) && !TextUtils.isEmpty(mvnoMatchData)) {
                /* MODIFIED-END by bo.chen,BUG-2851269*/
            if (ApnSetting.mvnoMatches(r, mvnoType, mvnoMatchData)) {
                mvnoList.add(pref);
                // Since adding to mvno list, save mvno info
                mMvnoType = mvnoType;
                mMvnoMatchData = mvnoMatchData;
            }
//[SOLUTION]-Add-BEGIN by TCTNB.(JiangLong Pan), 08/12/2016, SOLUTION-2719318
//Porting APN setting solution
        } else if ((apnNew != -1) && !TextUtils.isEmpty(mMvnoType) && !TextUtils.isEmpty(mMvnoMatchData)) {
            mvnoList.add(pref);
            Log.d(TAG, "New APN or OTA provisioning APN, add key to mvnoList: " + pref.getKey());
//[SOLUTION]-Add-END by TCTNB.(JiangLong Pan)
        } else {
            mnoList.add(pref);
        }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        if (!mUnavailable) {
//[SOLUTION]-Add-BEGIN by TCTNB.(JiangLong Pan), 08/12/2016, SOLUTION-2719318
//Porting APN setting solution
            //if (mAllowAddingApns) {
            if (getResources().getBoolean(R.bool.def_settings_allowNewApn_enable)) {
                menu.add(0, MENU_NEW, 0,
                        getResources().getString(R.string.menu_new))
                        .setIcon(android.R.drawable.ic_menu_add)
                        .setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM);
            }

            mMenuRestore = menu.add(0, MENU_RESTORE, 0,
                    getResources().getString(R.string.menu_restore))
                    .setIcon(android.R.drawable.ic_menu_upload);
//[SOLUTION]-Add-END by TCTNB.(JiangLong Pan)
        }

        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
        case MENU_NEW:
            addNewApn();
            return true;

        case MENU_RESTORE:
            restoreDefaultApn();
            return true;

        case android.R.id.home:
            getActivity().onBackPressed();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void addNewApn() {
        Intent intent = new Intent(Intent.ACTION_INSERT, Telephony.Carriers.CONTENT_URI);
        int subId = mSubscriptionInfo != null ? mSubscriptionInfo.getSubscriptionId()
                : SubscriptionManager.INVALID_SUBSCRIPTION_ID;
        intent.putExtra(SUB_ID, subId);

//[SOLUTION]-Add-BEGIN by TCTNB.(JiangLong Pan), 08/12/2016, SOLUTION-2719318
//Porting APN setting solution
        if (!getResources().getBoolean(R.bool.def_settings_NewApnMvnoValue_empty)) {
            if (!TextUtils.isEmpty(mMvnoType) && !TextUtils.isEmpty(mMvnoMatchData)) {
                intent.putExtra(MVNO_TYPE, mMvnoType);
                intent.putExtra(MVNO_MATCH_DATA, mMvnoMatchData);
            }
        }
//[SOLUTION]-Add-END by TCTNB.(JiangLong Pan)

        startActivity(intent);
    }

    @Override
    public boolean onPreferenceTreeClick(Preference preference) {
        int pos = Integer.parseInt(preference.getKey());

        /* MODIFIED-BEGIN by sunyandong, 2016-08-08,BUG-2694521*/
        try { // MODIFIED by ming.zeng, 2016-10-27,BUG-3137613
            Uri url = ContentUris.withAppendedId(getUri(Telephony.Carriers.CONTENT_URI), pos);
            Intent intent = new Intent(Intent.ACTION_EDIT, url);
            intent.putExtra(SUB_ID, mSubId);
            if (preference instanceof ApnPreference) {
                intent.putExtra("DISABLE_EDITOR", ((ApnPreference) preference).getApnReadOnly());
            }
            startActivity(intent);
        /* MODIFIED-BEGIN by ming.zeng, 2016-10-27,BUG-3137613*/
        } catch (ActivityNotFoundException e){
            Log.e(TAG, "not find edit activity");
        }
        /* MODIFIED-END by ming.zeng,BUG-3137613*/

        return true;
    }

    //Qcom upgrade add same method
    //private Uri getUri(Uri uri) {
    //    return Uri.withAppendedPath(uri, "subId/" + mSubId);
    //}
    /* MODIFIED-END by sunyandong,BUG-2694521*/

    public boolean onPreferenceChange(Preference preference, Object newValue) {
        Log.d(TAG, "onPreferenceChange(): Preference - " + preference
                + ", newValue - " + newValue + ", newValue type - "
                + newValue.getClass());
        if (newValue instanceof String) {
            setSelectedApnKey((String) newValue);
        }

        return true;
    }

    private void setSelectedApnKey(String key) {
        mSelectedKey = key;
        Log.d(TAG, "mSelectedKey setSelectedApnKey = " + mSelectedKey); // MODIFIED by wei.zhang-nb, 2016-11-07,BUG-3312022
        ContentResolver resolver = getContentResolver();

        ContentValues values = new ContentValues();
        values.put(APN_ID, mSelectedKey);

//[SOLUTION]-Add-BEGIN by TCTNB.(JiangLong Pan), 08/12/2016, SOLUTION-2719318
//Porting APN setting solution
        resolver.update(getUri(PREFERAPN_URI), values, null, null);
        Log.d(TAG, "setSelectedApnKey(): URI = " + getUri(PREFERAPN_URI).toString());
//[SOLUTION]-Add-END by TCTNB.(JiangLong Pan)
    }

    private String getSelectedApnKey() {
        String key = null;

        /* MODIFIED-BEGIN by wei.zhang-nb, 2016-11-07,BUG-3312022*/
        Log.d(TAG, "getSelectedApnKey ");
//[SOLUTION]-Add-BEGIN by TCTNB.(JiangLong Pan), 08/12/2016, SOLUTION-2719318
//Porting APN setting solution
        Cursor cursor = getContentResolver().query(getUri(PREFERAPN_URI), new String[] {"_id"},
                null, null, Telephony.Carriers.DEFAULT_SORT_ORDER);
//[SOLUTION]-Add-END by TCTNB.(JiangLong Pan)
        if (cursor.getCount() > 0) {
            Log.d(TAG, "getSelectedApnKey count: " + cursor.getCount());
            /* MODIFIED-END by wei.zhang-nb,BUG-3312022*/
            cursor.moveToFirst();
            key = cursor.getString(ID_INDEX);
        }
        cursor.close();
        return key;
    }

    private boolean restoreDefaultApn() {
        showDialog(DIALOG_RESTORE_DEFAULTAPN);
        mRestoreDefaultApnMode = true;

        if (mRestoreApnUiHandler == null) {
            mRestoreApnUiHandler = new RestoreApnUiHandler();
        }

        if (mRestoreApnProcessHandler == null ||
            mRestoreDefaultApnThread == null) {
            mRestoreDefaultApnThread = new HandlerThread(
                    "Restore default APN Handler: Process Thread");
            mRestoreDefaultApnThread.start();
            mRestoreApnProcessHandler = new RestoreApnProcessHandler(
                    mRestoreDefaultApnThread.getLooper(), mRestoreApnUiHandler);
        }

        mRestoreApnProcessHandler
                .sendEmptyMessage(EVENT_RESTORE_DEFAULTAPN_START);
        return true;
    }

    private class RestoreApnUiHandler extends Handler {
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case EVENT_RESTORE_DEFAULTAPN_COMPLETE:
                    Activity activity = getActivity();
                    if (activity == null) {
                        mRestoreDefaultApnMode = false;
                        return;
                    }
                    fillList();
                    getPreferenceScreen().setEnabled(true);
                    mRestoreDefaultApnMode = false;
//[SOLUTION]-Add-BEGIN by TCTNB.(JiangLong Pan), 08/12/2016, SOLUTION-2719318
//Porting APN setting solution
                    removeDialogAllowingStateLoss(DIALOG_RESTORE_DEFAULTAPN); // MODIFIED by siyan.chen-nb, 2016-10-17,BUG-3109376

                    boolean isUKHomoSimserviceEnable = getResources().getBoolean(com.android.internal.R.bool.feature_tctfw_openmarketsimservice_on);
                    Log.d(TAG, "onOptionsItemSelected(): isUKHomoSimserviceEnable - " + isUKHomoSimserviceEnable);
                    if (isUKHomoSimserviceEnable) {
                        restoreDefaultApnPersal();
                    } else {
                        Toast.makeText(
                                activity,
                                getResources().getString(R.string.restore_default_apn_completed),
                                Toast.LENGTH_LONG).show();
                    }
                    break;

                //Task1490925, disable restore apn function after apn db changed, 2016/04/08, BEGIN
                case EVENT_ENABLE_RESTORE_DEFAULT_MENU:
                    if (!mMenuRestore.isEnabled() && mTelephonyManager.getDataEnabled(mSubId)) {
                        Log.d(TAG, "set restore menu as true, after 2s");
                        mMenuRestore.setEnabled(true);
                    }
                    break;
//[SOLUTION]-Add-END by TCTNB.(JiangLong Pan)
            }
        }
    }

    private class RestoreApnProcessHandler extends Handler {
        private Handler mRestoreApnUiHandler;

        public RestoreApnProcessHandler(Looper looper, Handler restoreApnUiHandler) {
            super(looper);
            this.mRestoreApnUiHandler = restoreApnUiHandler;
        }

        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case EVENT_RESTORE_DEFAULTAPN_START:
                    ContentResolver resolver = getContentResolver();
                    //[FEATURE]-Mod-BEGIN by TCTNB.Xijun.Zhang,08/30/2016,Task-2821885,
                    //Restore APN list according subscription ID
                    //resolver.delete(DEFAULTAPN_URI, null, null);
                    resolver.delete(getUri(DEFAULTAPN_URI), null, null);
                    //[FEATURE]-Mod-END by TCTNB.Xijun.Zhang,08/30/2016,Task-2821885

                    mRestoreApnUiHandler
                        .sendEmptyMessage(EVENT_RESTORE_DEFAULTAPN_COMPLETE);
                    break;
            }
        }
    }

    @Override
    public Dialog onCreateDialog(int id) {
        if (id == DIALOG_RESTORE_DEFAULTAPN) {
            ProgressDialog dialog = new ProgressDialog(getActivity()) {
                public boolean onTouchEvent(MotionEvent event) {
                    return true;
                }
            };
            dialog.setMessage(getResources().getString(R.string.restore_default_apn));
            dialog.setCancelable(false);
            return dialog;
        }
        return null;
    }

    private Uri getUri(Uri uri) {
        int subId = SubscriptionManager.getDefaultDataSubscriptionId();
        if (mSubscriptionInfo != null && SubscriptionManager.isValidSubscriptionId(
                mSubscriptionInfo.getSubscriptionId())) {
            subId = mSubscriptionInfo.getSubscriptionId();
        }

        //Add by jianglong.pan for Qcom code conflict--BEGIN
        Log.d(TAG, "subId: " + subId + "; mSubId: " + mSubId);
        if (subId != mSubId) {
            subId = mSubId;
        }
        //Add by jianglong.pan for Qcom code conflict--END

        return Uri.withAppendedPath(uri, "/subId/" + subId);
    }

//[SOLUTION]-Add-BEGIN by TCTNB.(JiangLong Pan), 08/12/2016, SOLUTION-2719318
//Porting APN setting solution
    private void initSimState() {
        mUri = Telephony.Carriers.CONTENT_URI;
    }
    protected boolean getScreenEnableState() {
        int slotId = SubscriptionManager.getSlotId(mSubId);
        boolean simReady = TelephonyManager.SIM_STATE_READY == TelephonyManager.getDefault().getSimState(slotId);
        Log.d(TAG, "slotId: " + slotId + ", simReady: " + simReady);

        //[Aircom][Pre-10776] LTE-BTR-1-7472-AMR-MM: Fail: Device failed to activate the second PDP Context
        //Simultaneous voice and data are supported in AMSS 8xxx baseline and android, so it's normal to add/edit/activate APN during ongoing CS call.
        //here the judgement condition (mIsCallStateIdle) should be removed.
        //return sNotInMmsTransation && mIsCallStateIdle && !mAirplaneModeEnabled && simReady;//<Original expression>
        Log.d(TAG, "Transacting mms: " + sNotInMmsTransation + ", Airplanemode: " + mAirplaneModeEnabled);
        return sNotInMmsTransation && !mAirplaneModeEnabled && simReady;
     }

    private PhoneStateListener mPhoneStateListener = new PhoneStateListener() {

        @Override
        public void onServiceStateChanged(ServiceState serviceState) {
            super.onServiceStateChanged(serviceState);
        }
    };

    public void setView() {
        FrameLayout.LayoutParams lp = new FrameLayout.LayoutParams(
                LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);
        lp.gravity = Gravity.BOTTOM | Gravity.CENTER_HORIZONTAL;
        mExtraView = LayoutInflater.from(getActivity()).inflate(R.layout.apn_setting_next_button,
                null);
        mButtonOk = (Button) mExtraView.findViewById(R.id.next);
        mButtonOk.setText(getResources().getText(R.string.dlg_ok));
        mButtonOk.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Log.d(TAG, "mSelectedKey onClick = " + mSelectedKey); // MODIFIED by wei.zhang-nb, 2016-11-07,BUG-3312022
                if (mSelectedKey != null) {
                    setSelectedApnKey(mSelectedKey);
                    finish();
                } else {
                    return;
                }
            }
        });
        RelativeLayout buttonBar = (RelativeLayout) getActivity().findViewById(R.id.button_bar);
        buttonBar.removeAllViews();
        buttonBar.addView(mExtraView, lp);
        buttonBar.setVisibility(View.VISIBLE);
    }

    private void restoreDefaultApnPersal() {
        String simNumeric = mSubscriptionInfo == null ? "" : mTelephonyManager.getSimOperator(mSubscriptionInfo.getSubscriptionId());
        if (simNumeric == null) {
            Log.e(TAG, "get simNumeric failed");
            return;
        }
        Log.d(TAG, "restoreDefaultApnPersal simNumeric - " + simNumeric);

        boolean isUKMccmnc = "23430".equals(simNumeric) || "23433".equals(simNumeric) ||
            "23420".equals(simNumeric) ||
            "23410".equals(simNumeric) || "23415".equals(simNumeric);
        if (isUKMccmnc) {
            Settings.Global.putString(getContentResolver(), SIM_SERVICE, "");
            Intent simServiceIntent = new Intent(this.getActivity(), SimServiceActivity.class);
            simServiceIntent.putExtra("mccmnc", simNumeric);
            this.startActivityForResult(simServiceIntent, START_SIM_SERVICE);
        }
        return;
    }

    //CR-1005404(porting from CR951799)
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case START_SIM_SERVICE:
                if (resultCode == Activity.RESULT_OK) {
                    // restoreDefaultApn();  // CR-1011603
                } else if (resultCode == Activity.RESULT_CANCELED) {
                    fillList();
                }
                break;
        }
    }

    /**
     * Add by JRD_Zhanglei for Task1490925, 2016/04/08
     * Handles changes to the APN db, first set mMenuRestore disabled, then enable it.
     */
    private class ApnChangeObserver extends ContentObserver {
        public ApnChangeObserver() {
            super(new Handler());
        }

        @Override
        public void onChange(boolean selfChange) {
            if (mMenuRestore != null) {
                if (mTelephonyManager.getDataEnabled(mSubId)) {
                    Log.d(TAG, "set restore menu as false.");
                    mMenuRestore.setEnabled(false);
                    //after 1s mMenuRestore is disable and data enabled, then enable mMenuRestore.
                    mRestoreApnUiHandler.sendEmptyMessageDelayed(EVENT_ENABLE_RESTORE_DEFAULT_MENU, 1000);
                }
            }
        }
    }
//[SOLUTION]-Add-END by TCTNB.(JiangLong Pan)
}

