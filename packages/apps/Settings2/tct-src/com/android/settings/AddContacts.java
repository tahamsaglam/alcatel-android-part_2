/* Copyright (C) 2016 Tcl Corporation Limited */
package com.android.settings;

import mst.app.MstActivity; // MODIFIED by wanshun.ni, 2016-10-20,BUG-2827925
/* MODIFIED-BEGIN by wanshun.ni, 2016-10-10,BUG-2827925*/
import mst.app.dialog.AlertDialog;
import android.content.DialogInterface;
/* MODIFIED-END by wanshun.ni,BUG-2827925*/
import android.content.Intent;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
/* MODIFIED-BEGIN by wanshun.ni, 2016-10-10,BUG-2827925*/
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
/* MODIFIED-BEGIN by wanshun.ni, 2016-10-20,BUG-2827925*/
import mst.widget.toolbar.Toolbar;
import android.database.Cursor;
import android.provider.ContactsContract.Data;
/* MODIFIED-END by wanshun.ni,BUG-2827925*/

/*
 * This Activity is to save emergency message of One Finger Alarm
 */
public class AddContacts extends MstActivity { // MODIFIED by wanshun.ni, 2016-10-20,BUG-2827925
    private EditText nameEditText,numEditText;
    private String nameString, numString;
    private Button addBtn,deleteBtn;
    AlarmDateBaseHelper dbHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        /* MODIFIED-BEGIN by wanshun.ni, 2016-10-20,BUG-2827925*/
        setMstContentView(R.layout.alarm_add_contacts);
        showBackIcon(true);
        setTitle(R.string.add_alarm_contact);
        inflateToolbarMenu(R.menu.alarm_message_menu);
        /* MODIFIED-END by wanshun.ni,BUG-2827925*/
        Intent intent = getIntent();
        boolean isclickListView = (boolean) intent.getExtra("isclickListView");

        addBtn = (Button)findViewById(R.id.add_contacts);
        addBtn.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                Intent intent = new Intent("android.intent.action.contacts.list.PICKMULTIPHONES");
                intent.setType("vnd.android.cursor.dir/phone");
                intent.putExtra("mstFilter", "oneKeyAlarm");
                startActivityForResult(intent, 3);
            }
        });
        /* MODIFIED-END by wanshun.ni,BUG-2827925*/
        deleteBtn = (Button)findViewById(R.id.delete_contact);
        nameEditText = (EditText)findViewById(R.id.input_name);
        numEditText = (EditText)findViewById(R.id.input_num);
        if(isclickListView){
            addBtn.setVisibility(View.GONE);
            deleteBtn.setVisibility(View.VISIBLE);
            nameString = intent.getExtra("name").toString();
            numString = intent.getExtra("number").toString();
            nameEditText.setText(nameString);
            numEditText.setText(numString);
        }
        /* MODIFIED-END by wanshun.ni,BUG-2827925*/
        nameEditText.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                // TODO Auto-generated method stub
                nameString = nameEditText.getText().toString();

            }
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                    int after) {
                // TODO Auto-generated method stub

            }

            @Override
            public void afterTextChanged(Editable s) {
                // TODO Auto-generated method stub
            }
        });
        numEditText.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                // TODO Auto-generated method stub
                numString = numEditText.getText().toString();

            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                    int after) {
                // TODO Auto-generated method stub
            }
            @Override
            public void afterTextChanged(Editable s) {
                // TODO Auto-generated method stub
            }
        });
        /* MODIFIED-BEGIN by wanshun.ni, 2016-10-10,BUG-2827925*/
        deleteBtn.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                new AlertDialog.Builder(AddContacts.this).setTitle("Delete emergency contacts")
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Intent intent = getIntent();
                        setResult(1, intent);
                        finish();
                    }
                })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                })
                .show();
            }
        });
        /* MODIFIED-END by wanshun.ni,BUG-2827925*/

    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        // TODO Auto-generated method stub
        super.onActivityResult(requestCode, resultCode, data);
        /* MODIFIED-BEGIN by wanshun.ni, 2016-10-20,BUG-2827925*/
        if(data == null)
            return;
        final long[] dataIds = data.getLongArrayExtra("com.mediatek.contacts.list.pickdataresult");
        if (dataIds == null || dataIds.length <= 0) {
        return;
        }
        StringBuilder selection = new StringBuilder();
        selection.append(Data._ID);
        selection.append(" IN (");
        selection.append(dataIds[0]);
        selection.append(",");
        selection.append(dataIds[0]+1);
        selection.append(")");
        Cursor cursor=getContentResolver().query(Data.CONTENT_URI,null,selection.toString(),null,null);
        int i = 0;
        while (cursor.moveToNext()){
            if(i == 0){
                numEditText.setText(cursor.getString(cursor.getColumnIndex("data1")));
            } else {
                nameEditText.setText(cursor.getString(cursor.getColumnIndex("data1")));
            }
            i++;
        }
        /* MODIFIED-END by wanshun.ni,BUG-2827925*/
    }
    @Override
    public boolean onMenuItemClick(MenuItem item) { // MODIFIED by wanshun.ni, 2016-10-20,BUG-2827925
        // TODO Auto-generated method stub
        switch (item.getItemId()) {
        case R.id.one_finger_save:
            if(nameString == null || nameString.length() == 0){
                /* MODIFIED-BEGIN by wanshun.ni, 2016-10-10,BUG-2827925*/
                Toast.makeText(AddContacts.this, R.string.contact_null, Toast.LENGTH_SHORT).show();
                return false;
            }
            if(numString == null || numString.length() == 0){
                Toast.makeText(AddContacts.this, R.string.contact_number_null, Toast.LENGTH_SHORT).show();
                return false;
            }
            if(numString.length() != 11 || Integer.parseInt(numString.substring(0, 1)) != 1){
                Toast.makeText(AddContacts.this, R.string.contact_number_error, Toast.LENGTH_SHORT).show();
                return false;
            }

            Intent dataIntent = getIntent();
            dataIntent.putExtra("name", nameString);
            dataIntent.putExtra("number", numString);
            setResult(0, dataIntent);
            /* MODIFIED-END by wanshun.ni,BUG-2827925*/
            finish();

            return true;
        }
        /* MODIFIED-BEGIN by wanshun.ni, 2016-10-20,BUG-2827925*/
        return false;
    }
    @Override
    public void onNavigationClicked(View view) {
        super.onNavigationClicked(view);
        finish();
        /* MODIFIED-END by wanshun.ni,BUG-2827925*/
    }
}
