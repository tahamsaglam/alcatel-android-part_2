/******************************************************************************/
/* ========================================================================== */
/*     Modifications on Features list / Changes Request / Problems Report     */
/* -------------------------------------------------------------------------- */
/*    date   |        author        |         Key          |     comment      */
/* ----------|----------------------|----------------------|----------------- */
/* 13/10/2016|     zhengyu.hu       |   Task-2894950       |[LMN]Low memory   */
/*           |                      |                      |notification      */
/*           |                      |                      |policy            */
/* ----------|----------------------|----------------------|----------------- */
/******************************************************************************/

package com.android.settings;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnKeyListener;
import android.content.Intent;
import android.os.Bundle;
import android.os.StatFs;
import android.preference.Preference;
import android.telephony.PhoneStateListener;
import android.telephony.TelephonyManager;
import android.view.KeyEvent;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.content.BroadcastReceiver;
import android.content.IntentFilter;
import android.content.Context;
//import android.util.TctLog;
import android.content.ComponentName;
import android.provider.Settings;
import android.app.StatusBarManager;

public class LowMemoryStorage extends Activity implements
        Preference.OnPreferenceChangeListener {
    private String TAG = "LowMemoryStorage";
    private AlertDialog mDialog = null;
    private long mFreeMem;
    private String[] mStrings = null;
    private StatFs mDataFileStats;
    private static final String DATA_PATH = "/data";
    private BroadcastReceiver receiver = null;
    private long mOldFreeMem;
    private boolean reCreateDialog = false;
    private TelephonyManager mTelephonyManager;
    // Force mode 50M a, force user just can do 4 things (Application Manger
    // ,file manager ,Dial ,SMS);
    public static final int FORCE_MODE_THRESHOLD = 100 * 1024 * 1024; //Mod by fuyin.liu for defect 1073492
    public static final int EXTREMELY_MODE_THRESHOLD = 50 * 1024 * 1024 ; //add by fuyin.liu for defect 1073492

    // [Low memory]Can't pop up the call interface directly
    private PhoneStateListener mPhoneStateListener = new PhoneStateListener() {
        @Override
        public void onCallStateChanged(int state, String ignored) {
            // The user might already be in a call when the alarm fires. When
            // we register onCallStateChanged, we get the initial in-call state
            // which kills the alarm. Check against the initial call state so
            // we don't kill the alarm during a call.
            if (state == TelephonyManager.CALL_STATE_RINGING
                    || state == TelephonyManager.CALL_STATE_OFFHOOK) {
                if (mDialog != null && mDialog.isShowing()) {
                    mDialog.dismiss();
                }
            } else if (state == TelephonyManager.CALL_STATE_IDLE) {
                // add by zhangfu for pr991887
                mDataFileStats.restat(DATA_PATH);
                long m_FreeMem = (long) mDataFileStats.getAvailableBlocks()
                        * mDataFileStats.getBlockSize();
                boolean force_mode_flag = (m_FreeMem < FORCE_MODE_THRESHOLD);
                if (mDialog != null && !mDialog.isShowing() && force_mode_flag) {
                    mDialog.show();
                }
            }
        }
    };

    @Override
    public void onCreate(Bundle icicle) {
        super.onCreate(icicle);
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        mDataFileStats = new StatFs(DATA_PATH);
        receiver = new BroadcastReceiver() {
            public void onReceive(Context context, Intent intent) {
                refreshDialog();
            }
        };
        IntentFilter filter = new IntentFilter();
        filter.addAction("intent.action.EXTREMELY_MODE_THRESHOLD");
        this.registerReceiver(receiver, filter);
        // [Low memory]Can't pop up the call interface directly
        mTelephonyManager = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
        mTelephonyManager.listen(mPhoneStateListener,
                PhoneStateListener.LISTEN_CALL_STATE);
    }

    private DialogInterface.OnClickListener mListener = new DialogInterface.OnClickListener() {

        public void onClick(DialogInterface dialog, int which) {
            if (which == DialogInterface.BUTTON_POSITIVE) {
                Intent mIntent = new Intent();
                mIntent.setAction("android.intent.action.MANAGE_PACKAGE_STORAGE");
                mIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(mIntent);
            } else if (which == DialogInterface.BUTTON_NEGATIVE) {
                finish();
            }
        }
    };

    @Override
    public void onResume() {
        super.onResume();
        refreshDialog();
        if (mDialog == null) {
            finish();
        }
    }

    @Override
    public boolean onSearchRequested() {
        return true;
    }

    @Override
    public boolean onPreferenceChange(Preference preference, Object newValue) {
        return false;
    }

    public void refreshDialog() {
        mDataFileStats.restat(DATA_PATH);
        mOldFreeMem = mFreeMem;
        mFreeMem = (long) mDataFileStats.getAvailableBlocks()
                * mDataFileStats.getBlockSize();
        if (mFreeMem >= (FORCE_MODE_THRESHOLD)) {
            finish();
        }
        //Mod by fuyin.liu for defect 1073492
        if ((mOldFreeMem < EXTREMELY_MODE_THRESHOLD && mFreeMem >= EXTREMELY_MODE_THRESHOLD)
                || (mOldFreeMem >= EXTREMELY_MODE_THRESHOLD && mFreeMem < EXTREMELY_MODE_THRESHOLD)) {
            reCreateDialog = true;
        } else {
            reCreateDialog = false;
        }
        if (mDialog != null && reCreateDialog) {
            mDialog.dismiss();
            mDialog = null;
        }
        // TctLog.i(TAG,"mFreeMem = " + mFreeMem/(1024*1024)+"M mDialog == "+
        // mDialog+ " reCreateDialog = "+ reCreateDialog);
        if (mDialog == null) {
            String string1 = this.getResources().getString(
                    R.string.summary_popup_dialog);
            String string3 = this.getResources().getString(
                    R.string.phone_dialog_button);
            String string4 = this.getResources().getString(
                    R.string.message_dialog_button);
            String string2 = this.getResources().getString(
                    R.string.filemanager_delete_file);
            if (mFreeMem < EXTREMELY_MODE_THRESHOLD ) {
                mStrings = new String[] { string1, string2, string3 };
            } else {
                mStrings = new String[] { string1, string2, string3, string4 };
            }
            mDialog = new AlertDialog.Builder(this,
                    R.style.Theme_LowMemoryDialog)
                    // mDialog = new AlertDialog.Builder(this)
                    .setIcon(com.android.internal.R.drawable.ic_dialog_alert)
                    .setTitle(getText(R.string.criticallowtitle))
                    .setPositiveButton(
                            getText(com.android.internal.R.string.ok),
                            mListener)
                    .setNegativeButton(
                            getText(com.android.internal.R.string.cancel),
                            mListener)
                    .setItems(mStrings, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                          Intent mIntent = new Intent();
                          switch(which){
                            case 0:
                                mIntent.setAction("android.intent.action.MANAGE_PACKAGE_STORAGE");
                                mIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                startActivity(mIntent);
                                break;
                            case 1:
                                mIntent.setComponent(new ComponentName(
                                        "com.jrdcom.filemanager",
                                        "com.jrdcom.filemanager.activity.FileBrowserActivity"));
                                mIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                startActivity(mIntent);
                                break;
                            case 2:
                                mIntent.setAction("android.intent.action.DIAL"/* "android.intent.action.PHONE_BUTTON" */);
                                mIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                startActivity(mIntent);
                                break;
                            case 3:
                                mIntent.setComponent(new ComponentName(
                                        "com.android.mms",
                                        "com.android.mms.ui.ComposeMessageActivity"));
                                mIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                startActivity(mIntent);
                                break;
                            default:
                                break;
                            }
                        }
                    }).create();
            if (mDialog != null) {
                Window window = mDialog.getWindow();
                window.setType(WindowManager.LayoutParams.TYPE_SYSTEM_ALERT);
                WindowManager.LayoutParams lp = window.getAttributes();
                // lp.flags |=
                // WindowManager.LayoutParams.FLAG_HOMEKEY_DISPATCHED;
                window.setAttributes(lp);
            }
        }
        if (mFreeMem < (FORCE_MODE_THRESHOLD)) {
            mDialog.setOnKeyListener(new OnKeyListener() {
                public boolean onKey(DialogInterface dialog, int keyCode,
                        KeyEvent event) {
                    if (keyCode == KeyEvent.KEYCODE_SEARCH
                            || keyCode == KeyEvent.KEYCODE_BACK) {
                        return true;
                    } else {
                        return false;
                    }
                }
            });
            if (!mDialog.isShowing()) {
                mDialog.show();
            }
            Button mNegativeButton = mDialog
                    .getButton(AlertDialog.BUTTON_NEGATIVE);
            if (mNegativeButton != null) {
                mDialog.getButton(AlertDialog.BUTTON_NEGATIVE)
                        .setEnabled(false);
            }
            mDialog.setCancelable(false);
        } else {
            finish();
        }
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        switch (keyCode) {
        case KeyEvent.KEYCODE_HOME:
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }

    public void onDestroy() {
        super.onDestroy();
        // [Low memory]Can't pop up the call interface directly
        mTelephonyManager.listen(mPhoneStateListener, 0);
        this.unregisterReceiver(receiver);
    }
}

