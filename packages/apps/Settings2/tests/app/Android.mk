LOCAL_PATH:= $(call my-dir)
include $(CLEAR_VARS)

# We only want this apk build for tests.
LOCAL_MODULE_TAGS := tests
LOCAL_CERTIFICATE := platform

LOCAL_JAVA_LIBRARIES := android.test.runner bouncycastle

# Include all test java files.
LOCAL_SRC_FILES := $(call all-java-files-under, src)

LOCAL_PACKAGE_NAME := Settings2Tests
LOCAL_OVERRIDES_PACKAGES := Settings2Tests

LOCAL_INSTRUMENTATION_FOR := Settings2

include $(BUILD_PACKAGE)
