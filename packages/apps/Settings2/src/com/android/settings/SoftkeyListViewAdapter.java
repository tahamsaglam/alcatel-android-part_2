/* Copyright (C) 2016 Tcl Corporation Limited */
package com.android.settings;

import java.util.List;
import java.util.Map;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RadioButton;
import com.android.settings.R;
import android.widget.TextView;

public class SoftkeyListViewAdapter extends BaseAdapter {
    private String TAG = "SoftkeyListViewAdapter";
    private Context context;
    private List<Map<String, Object>> listItems;
    private int[] softkeyImageRes = null;
    private String[] typeName;

    public SoftkeyListViewAdapter(Context context, List<Map<String, Object>> items, int[] keyImageRes) {
        this.context = context;
        this.listItems = items;
        this.softkeyImageRes = keyImageRes;
    }

    public SoftkeyListViewAdapter(Context context, List<Map<String, Object>> items, int[] keyImageRes,String[] typeName) {
        this.context = context;
        this.listItems = items;
        this.softkeyImageRes = keyImageRes;
        this.typeName = typeName;
    }

    @Override
    public int getCount() {
        return listItems.size();
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.softkey_list_view_item, null);
            holder = new ViewHolder();
            holder.tv = (TextView) convertView.findViewById(R.id.keytype_name);
            holder.rb = (RadioButton) convertView.findViewById(R.id.button);
            holder.keyImage = (ImageView) convertView.findViewById(R.id.keyImage);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        holder.tv.setText(typeName[position]);
        holder.rb.setChecked((Boolean) getListItems().get(position).get("checked"));
        holder.keyImage.setImageResource(softkeyImageRes[position]);
        return convertView;
    }

    public Context getContext(){
        return context;
    }

    public List<Map<String, Object>> getListItems(){
        return listItems;
    }

    public final class ViewHolder {
        public TextView tv;
        public ImageView keyImage;
        public RadioButton rb;
    }
}
