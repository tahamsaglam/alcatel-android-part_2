/* Copyright (C) 2016 Tcl Corporation Limited */
package com.android.settings;

import android.content.Context;
//import android.preference.ListPreference;
import mst.preference.ListPreference;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.View;
import android.widget.TextView;

public class WidgetListPreference extends ListPreference {

    private TextView mTextView;
    private String mDetail;

    public WidgetListPreference(Context context, AttributeSet attrs) {
        super(context, attrs);
        setWidgetLayoutResource(R.layout.pref_widget);
    }
    public WidgetListPreference(Context context) {
        this(context, null);
    }
    //@Override
    //protected void onBindView(View view) {
    //    super.onBindView(view);
    //    View widgetFrame = view.findViewById(com.android.internal.R.id.widget_frame);
    //
    //    if (widgetFrame != null) {
    //        mTextView = (TextView) widgetFrame.findViewById(R.id.pref_tv_detail);
    //        if (!TextUtils.isEmpty(mDetail)) {
    //            mTextView.setText(mDetail);
    //        } else {
    //            mTextView.setText("");
    //        }
    //    }
    //}
    public void onBindView(View holder) {
        super.onBindView(holder);
        View widgetFrame = (View) holder.findViewById(com.android.internal.R.id.widget_frame);

        if (widgetFrame != null) {
            mTextView = (TextView) widgetFrame.findViewById(R.id.pref_tv_detail);
            if (!TextUtils.isEmpty(mDetail)) {
                mTextView.setText(mDetail);
            } else {
                mTextView.setText("");
            }
        }
    }

    public void setDetail(String detail) {
        this.mDetail = detail;
        notifyChanged();
    }

    public void setSummary(CharSequence summary) {
        this.mDetail = summary.toString();
        notifyChanged();
    }
}
