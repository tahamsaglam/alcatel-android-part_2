/* Copyright (C) 2016 Tcl Corporation Limited */
package com.android.settings;

import android.content.Context;
//import android.preference.Preference;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import mst.preference.Preference;

import com.android.settings.R;

/**
 * @author heng.zhang1
 */
public class WidgetPreferenceSec extends Preference {
    private TextView mTextView;
    private String mDetail;
    private ImageView mImageView;
    private boolean mShow = false;
    // [BUGFIX]-ADD-BEGIN BY TCTNB.DINGYI,2016/05/10,defect-2106034
    private boolean needsetcolor = false;
    private Context mContext = null;
    // [BUGFIX]-ADD-END BY TCTNB.DINGYI,2016/05/10,defect-2106034

    public WidgetPreferenceSec(Context context) {
        this(context, null);
        // [BUGFIX]-ADD-BEGIN BY TCTNB.DINGYI,2016/05/10,defect-2106034
        mContext = context;
        // [BUGFIX]-ADD-END BY TCTNB.DINGYI,2016/05/10,defect-2106034
        setWidgetLayoutResource(R.layout.pref_widget);
    }

    public WidgetPreferenceSec(Context context, AttributeSet attrs) {
        this(context, attrs, com.android.internal.R.attr.preferenceStyle);
    }

    public WidgetPreferenceSec(Context context, AttributeSet attrs, int defStyleAttr) {
        this(context, attrs, defStyleAttr, 0);
    }

    public WidgetPreferenceSec(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        // [BUGFIX]-ADD-BEGIN BY TCTNB.DINGYI,2016/05/10,defect-2106034
        mContext = context;
        // [BUGFIX]-ADD-END BY TCTNB.DINGYI,2016/05/10,defect-2106034
        setWidgetLayoutResource(R.layout.pref_widget);
    }

    public void onBindView(View view) {
        super.onBindView(view);
        android.util.Log.d("yshsw", " onBindViewHolder ");
        View widgetFrame = (View) view.findViewById(com.android.internal.R.id.widget_frame);

        if (widgetFrame != null) {
            mTextView = (TextView) widgetFrame.findViewById(R.id.pref_tv_detail);
            if (!TextUtils.isEmpty(mDetail)) {
                android.util.Log.d("yshsw", " not null mDetail: " + mDetail);
                mTextView.setText(mDetail);
            } else {
                android.util.Log.d("yshsw", " null");
                mTextView.setText("");
            }
            mImageView = (ImageView) widgetFrame.findViewById(R.id.pref_image_detail);
            if (mShow) {
                mImageView.setVisibility(View.GONE);
            }
            // [BUGFIX]-ADD-BEGIN BY TCTNB.DINGYI,2016/05/10,defect-2106034
            if (needsetcolor) {
                mImageView.setColorFilter(mContext.getResources().getColor(R.color.text_disable));
            }
            // [BUGFIX]-ADD-END BY TCTNB.DINGYI,2016/05/10,defect-2106034
        }
    }

    public void setDetail(String detail) {
        this.mDetail = detail;
        notifyChanged();
    }

    public void setGone(boolean visible) {
        this.mShow = visible;
        notifyChanged();
    }

    public String getDetail() {
        return mDetail;
    }

    public void setSummary(CharSequence summary) {
        setSummary(summary, true);
    }

    public void setSummary(CharSequence summary, boolean location) {
        if (location) {
            this.mDetail = summary.toString();
            notifyChanged();
        } else {
            super.setSummary(summary);
        }
    }

    // [BUGFIX]-ADD-BEGIN BY TCTNB.DINGYI,2016/05/10,defect-2106034
    public void setDsiabledImageColorFilter(boolean needset) {
        this.needsetcolor = needset;
        notifyChanged();
    }
    // [BUGFIX]-ADD-END BY TCTNB.DINGYI,2016/05/10,defect-2106034
}
