/*
 * Copyright (C) 2006 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.android.settings;

import com.android.ims.ImsManager;
import com.android.internal.telephony.Phone;
import com.android.internal.telephony.PhoneConstants;
import com.android.internal.telephony.PhoneFactory;
import com.android.internal.telephony.IccCardConstants;
import com.android.internal.telephony.TelephonyIntents;
import com.android.internal.telephony.TelephonyProperties;
import com.android.settingslib.RestrictedLockUtils;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import android.app.ActionBar;
import mst.app.dialog.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.net.Uri;
import android.os.AsyncResult;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.PersistableBundle;
import android.os.RemoteException;
import android.os.ServiceManager;
import android.os.SystemProperties;
import android.os.UserHandle;
import android.os.UserManager;
import mst.preference.Preference;
import mst.preference.PreferenceActivity;
import mst.preference.PreferenceScreen;
import mst.preference.PreferenceCategory;
import mst.preference.SwitchPreference;
import android.telecom.PhoneAccount;
import android.telecom.PhoneAccountHandle;
import android.telephony.CarrierConfigManager;
import android.telephony.PhoneStateListener;
import android.telephony.SubscriptionInfo;
import android.telephony.SubscriptionManager;
import android.telephony.TelephonyManager;
import android.telecom.TelecomManager;

import android.text.TextUtils;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import org.codeaurora.internal.IExtTelephony;

//For Russian Beeline,notify end user when set preferred network mode to Lte On-
//ly.The Notification should display when reboot or hotplug if Lte only is the
//preferred networkmode.
import java.lang.reflect.Field;
import android.widget.TextView;
import android.provider.Settings;
import android.provider.Settings.SettingNotFoundException;

/**
 * "Mobile network settings" screen.  This preference screen lets you
 * enable/disable mobile data, and control data roaming and other
 * network-specific mobile data features.  It's used on non-voice-capable
 * tablets as well as regular phone devices.
 *
 * Note that this PreferenceActivity is part of the phone app, even though
 * you reach it from the "Wireless & Networks" section of the main
 * Settings app.  It's not part of the "Call settings" hierarchy that's
 * available from the Phone app (see CallFeaturesSetting for that.)
 */
public class TctSimCardSettings extends PreferenceActivity
        implements Preference.OnPreferenceChangeListener{

    private static final String TAG = "TctSimCardSettings";
    private static final String KEY_SIM_ENABLE = "tct_sim_enable";
    private static final String KEY_SIM_DATA_ROAMING = "tct_sim_data_roaming";
    private static final String KEY_TCT_SIM_NAME = "tct_sim_name";
    private static final String KEY_TCT_SIM_NUMBER = "tct_sim_number";
    private static final String KEY_SIM_OTHER_CATEGORY = "sim_other_category";
    private static final String KEY_TCT_OPERATOR_SETTING = "tct_operator_setting";
    private static final String KEY_TCT_APN_SETTING = "tct_apn_setting";
    private static final String KEY_TCT_SIM_APP = "tct_sim_app";

    private static final String ACTION_UICC_MANUAL_PROVISION_STATUS_CHANGED =
            "org.codeaurora.intent.action.ACTION_UICC_MANUAL_PROVISION_STATUS_CHANGED";
    private static final String ACTION_STK_SET_APP_STATE_COMPLETED = "com.android.stk.ACTION_STK_SET_APP_STATE_COMPLETED";
    private static final String DUAL_ICON = "dual_icon";
    private static final String APP_INSTALL = "app_install";
    private static final String STK_SLOTID = "stk_slotid";
    private static final String EXTRA_SUB_ID = "sub_id";
    private static final int PROVISIONED = 1;
    private static final int NOT_PROVISIONED = 0;
    private static final int INVALID_STATE = -1;
    private static final int CARD_NOT_PRESENT = -2;
    private static final String EXTRA_NEW_PROVISION_STATE = "newProvisionState";

    private static final int EVT_UPDATE = 1;
    private static final int EVT_SHOW_RESULT_DLG = 2;
    private static final int EVT_SHOW_PROGRESS_DLG = 3;
    private static final int EVT_PROGRESS_DLG_TIME_OUT = 4;

    private static final int CONFIRM_ALERT_DLG_ID = 10;
    private static final int ERROR_ALERT_DLG_ID = 11;
    private static final int RESULT_ALERT_DLG_ID = 12;

    private static final int REQUEST_SUCCESS = 0;
    private static final int GENERIC_FAILURE = -1;
    private static final int INVALID_INPUT = -2;
    private static final int REQUEST_IN_PROGRESS = -3;

    private static final int PROGRESS_DLG_TIME_OUT = 30000;
    private static final int MSG_DELAY_TIME = 3000;

    private SwitchPreference mSimEnablePreference;
    private TctRestrictedSwitchPreference mDataRoamingPreference;
    private TctWidgetPreference mSimNamePreference;
    private TctWidgetPreference mSimNumberPreference;
    private TctWidgetPreference mOperatorPreference;
    private TctWidgetPreference mAPNPreference;
    private TctWidgetPreference mSimAppPreference;
    private PreferenceCategory mSimOtherCategory;

    private int mPhoneCount = TelephonyManager.getDefault().getPhoneCount();
    private int[] mUiccProvisionStatus = new int[mPhoneCount];
    private int stk_slotId = -1;
    private boolean stk_dualicon = true;
    private boolean stk_install = false;
    private int mSlotId;
    private int mNumSlots;
    private boolean mSimEnableIsChecked;
    private boolean mSimEnableCmdInProgress = false;
    private boolean mOkClicked;
    private AlertDialog mSimEnableAlertDialog = null;
    private ProgressDialog mSimEnableProgressDialog = null;
    private Dialog alertDataRoamingDialog = null;
    private boolean needupdateDataRoaming = true;
    private SubscriptionManager mSubscriptionManager;
    private TelephonyManager mTelephonyManager;
    private Context mContext;

    @Override
    public boolean onPreferenceTreeClick(PreferenceScreen preferenceScreen, Preference preference) {
        if (preference == mSimEnablePreference){
            return true;
        } else if (preference == mDataRoamingPreference){
            return true;
        } else if(preference == mSimNamePreference){
            SubscriptionInfo sir = mSubscriptionManager
                    .getActiveSubscriptionInfoForSimSlotIndex(mSlotId);
            if (sir != null) {
                TctSimNamePreferenceDialog mTctSimName = new TctSimNamePreferenceDialog();
                mTctSimName.showdialog(mContext, mSlotId);
            }
        }else if(preference == mSimNumberPreference){
            SubscriptionInfo sir = mSubscriptionManager
                    .getActiveSubscriptionInfoForSimSlotIndex(mSlotId);
            if (sir != null) {
                TctSimNumberPreferenceDialog mTctSimNumber = new TctSimNumberPreferenceDialog();
                mTctSimNumber.showdialog(mContext, mSlotId);
            }
        } else if (preference == mOperatorPreference){
            SubscriptionInfo sir = mSubscriptionManager
                    .getActiveSubscriptionInfoForSimSlotIndex(mSlotId);
            if (sir != null) {
                if (SubscriptionManager.isValidSubscriptionId(sir.getSubscriptionId())) {
                    /* MODIFIED-BEGIN by bo.chen, 2016-09-27,BUG-3000255*/
                    //final Intent intent = new Intent(
                    //       "org.codeaurora.settings.NETWORK_OPERATOR_SETTINGS_ASYNC");
                    Intent intent = new Intent(Intent.ACTION_MAIN);
                    intent.setComponent(new ComponentName("com.android.phone",
                    "com.android.phone.NetworkSetting"));
                    /* MODIFIED-END by bo.chen,BUG-3000255*/
                    intent.putExtra(EXTRA_SUB_ID, sir.getSubscriptionId());
                    startActivity(intent);
                }
            }
        } else if (preference == mAPNPreference){
            SubscriptionInfo sir = mSubscriptionManager
                    .getActiveSubscriptionInfoForSimSlotIndex(mSlotId);
            if (sir != null) {
                if (SubscriptionManager.isValidSubscriptionId(sir.getSubscriptionId())){
                    final Intent intent = new Intent(Settings.ACTION_APN_SETTINGS);
                    intent.putExtra(":settings:show_fragment_as_subsetting", true);
                    intent.putExtra(EXTRA_SUB_ID, sir.getSubscriptionId());
                    startActivity(intent);
                }
            }
        } else if (preference == mSimAppPreference){
            boolean subValid = isCurrentSubValid();
            boolean currentSimActivated = (getProvisionStatus(mSlotId)== PROVISIONED);
            if(subValid && currentSimActivated){
                ComponentName cName;
                Intent intent = new Intent(Intent.ACTION_MAIN);
                if(stk_dualicon==true){
                    String[] launcherActivity = {
                            "com.android.stk.StkLauncherActivity0",
                            "com.android.stk.StkLauncherActivity1"
                    };
                    cName = new ComponentName("com.android.stk", launcherActivity[mSlotId]);
                }else{
                    cName = new ComponentName("com.android.stk", "com.android.stk.StkMain");
                }
                intent.setComponent(cName);
                startActivity(intent);
            }
        }
        return true;
    }

    @Override
    protected void onCreate(Bundle icicle) {
        super.onCreate(icicle);
        addPreferencesFromResource(R.xml.tct_simcard_settings);
        mContext = this;
        final Bundle extras = getIntent().getExtras();
        mSlotId = extras.getInt(TctDualSimNetworkSettings.EXTRA_SLOT_ID, -1);
        setTitle(String.format(mContext.getResources().getString(R.string.tct_dualsim_simcard_title),(mSlotId + 1)));
        mSubscriptionManager = SubscriptionManager.from(mContext);
        mTelephonyManager = (TelephonyManager) mContext.getSystemService(Context.TELEPHONY_SERVICE);
        mNumSlots = mTelephonyManager.getSimCount();
        stk_dualicon = "true".equalsIgnoreCase(SystemProperties.get("def.stk.show.double.icon", "false"));
         /* MODIFIED-BEGIN by qili.zhang, 2016-09-26,BUG-2994458*/
         if (getActionBar() != null) {
           getActionBar().setDisplayHomeAsUpEnabled(true);
            getActionBar().setHomeButtonEnabled(true);
         }
         /* MODIFIED-END by qili.zhang,BUG-2994458*/
        PreferenceScreen prefSet = getPreferenceScreen();
        mSimEnablePreference = (SwitchPreference)prefSet.findPreference(KEY_SIM_ENABLE);
        mSimEnablePreference.setOnPreferenceChangeListener(this);
        mDataRoamingPreference = (TctRestrictedSwitchPreference)prefSet.findPreference(KEY_SIM_DATA_ROAMING);
        mDataRoamingPreference.setOnPreferenceChangeListener(this);
        mSimNamePreference = (TctWidgetPreference)prefSet.findPreference(KEY_TCT_SIM_NAME);
        mSimNumberPreference = (TctWidgetPreference)prefSet.findPreference(KEY_TCT_SIM_NUMBER);
        mOperatorPreference = (TctWidgetPreference)prefSet.findPreference(KEY_TCT_OPERATOR_SETTING);
        mAPNPreference = (TctWidgetPreference)prefSet.findPreference(KEY_TCT_APN_SETTING);
        mSimAppPreference = (TctWidgetPreference)prefSet.findPreference(KEY_TCT_SIM_APP);

        mSimOtherCategory = (PreferenceCategory)prefSet.findPreference(KEY_SIM_OTHER_CATEGORY);
        IntentFilter intentFilter = new IntentFilter(ACTION_UICC_MANUAL_PROVISION_STATUS_CHANGED);
        intentFilter.addAction(ACTION_STK_SET_APP_STATE_COMPLETED);
        mContext.registerReceiver(mReceiver, intentFilter);
    }

    private int getProvisionStatus(int slotId) {
        return mUiccProvisionStatus[slotId];
    }

    private boolean hasCard(int slodId) {
        return TelephonyManager.getDefault().hasIccCard(slodId);
    }

    private boolean isAirplaneModeOn() {
        return Settings.Global.getInt(mContext.getContentResolver(),
                Settings.Global.AIRPLANE_MODE_ON, 0) != 0;
    }

    private boolean isCurrentSubValid() {
        boolean isSubValid = false;
        if (!isAirplaneModeOn() && hasCard(mSlotId)) {
            SubscriptionInfo sir = mSubscriptionManager.
                    getActiveSubscriptionInfoForSimSlotIndex(mSlotId);
            if (sir != null ) {
                if (SubscriptionManager.isValidSubscriptionId(sir.getSubscriptionId()) &&
                        sir.getSimSlotIndex() >= 0 &&
                        getProvisionStatus(sir.getSimSlotIndex()) >= 0) {
                    isSubValid = true;
                }
            }
        }
        return isSubValid;
    }

    private void setUserSelectPhoneAccount(int sub_id) {
        TelecomManager telecomManager = TelecomManager.from(mContext);
        PhoneAccountHandle phoneAccountHandle = telecomManager.getUserSelectedOutgoingPhoneAccount();
        List<PhoneAccountHandle> allPhoneAccounts = telecomManager.getCallCapablePhoneAccounts();

        if (allPhoneAccounts != null) {
            PhoneAccount phoneAccount = null;
            for (int i = 0; i < allPhoneAccounts.size(); i++) {
                phoneAccount = telecomManager.getPhoneAccount(allPhoneAccounts.get(i));
                if (phoneAccount != null) {
                    int get_subId = mTelephonyManager.getSubIdForPhoneAccount(phoneAccount);
                    Log.d(TAG,"getSubIdForPhoneAccount(phoneAccount) " + get_subId + ", sub_id =" + sub_id
                            + ", phoneAccount = " + phoneAccount.getLabel());

                    if (get_subId != -1 && get_subId == sub_id) {
                        if (phoneAccountHandle != allPhoneAccounts.get(i)) {
                            //not need set same account every time
                            telecomManager.setUserSelectedOutgoingPhoneAccount(allPhoneAccounts.get(i));
                        }
                        break;
                    }
                } else {
                    Log.d(TAG,"phoneAccount is null, allPhoneAccounts.get(" + i +") = " + allPhoneAccounts.get(i));
                }
            }
        }
    }

    private int getSlotProvisionStatus(int slot) {
        int provisionStatus = -1;
        try {
            //get current provision state of the SIM.
            IExtTelephony extTelephony =
                    IExtTelephony.Stub.asInterface(ServiceManager.getService("extphone"));
            provisionStatus =  extTelephony.getCurrentUiccCardProvisioningStatus(slot);
        } catch (RemoteException ex) {
            provisionStatus = INVALID_STATE;
            Log.e(TAG,"Failed to get slotId: "+ slot +" Exception: " + ex);
        } catch (NullPointerException ex) {
            provisionStatus = INVALID_STATE;
            Log.e(TAG,"Failed to get slotId: "+ slot +" Exception: " + ex);
        }
        return provisionStatus;
    }

    public void updateDefaultValues() {

        boolean slot1_hasIcc = mTelephonyManager.hasIccCard(PhoneConstants.SUB1);
        boolean slot2_hasIcc = mTelephonyManager.hasIccCard(PhoneConstants.SUB2);
        SubscriptionInfo sub1_Info = mSubscriptionManager.getActiveSubscriptionInfoForSimSlotIndex(PhoneConstants.SUB1);
        SubscriptionInfo sub2_Info = mSubscriptionManager.getActiveSubscriptionInfoForSimSlotIndex(PhoneConstants.SUB2);

        int sub1_id = -1;
        int sub2_id = -1;
        if (sub1_Info != null) {
            sub1_id = sub1_Info.getSubscriptionId();
            Log.d(TAG,"sub1_id = " + sub1_id);
        }
        if (sub2_Info != null) {
            sub2_id = sub2_Info.getSubscriptionId();
            Log.d(TAG,"sub2_id = " + sub2_id);
        }

        int sub1_state = getSlotProvisionStatus(PhoneConstants.SUB1);
        int sub2_state = getSlotProvisionStatus(PhoneConstants.SUB2);
        Log.d(TAG,"sub1 status = " + sub1_state + ", sub2_status = " + sub2_state);

        if (sub1_state != PROVISIONED && sub2_state != PROVISIONED) {
            Log.d(TAG,"sub1 and sub2 all not provisioned, return out.");
            return;
        }

        //if hotswap sim card sim1 and sim2 load speed is different, so changed default sms/call/data sub
        //depend on at least one sim card loaded, need wait sim loaded.
//        if (!IccCardConstants.INTENT_VALUE_ICC_LOADED.equals(sim1_status) &&
//                !IccCardConstants.INTENT_VALUE_ICC_LOADED.equals(sim2_status)) {
//            Log.d(TAG,"sim1 and sim2 are all not loaded, need wait the sim status change.");
//            return;
//        }

        if (mSubscriptionManager.getSimStateForSlotIdx(PhoneConstants.SUB1) != TelephonyManager.SIM_STATE_READY &&
                mSubscriptionManager.getSimStateForSlotIdx(PhoneConstants.SUB2) != TelephonyManager.SIM_STATE_READY) {
            Log.d(TAG,"sim1 and sim2 are all not SIM_STATE_READY.");
            return;
        }

        TelecomManager telecomManager = TelecomManager.from(mContext);
        PhoneAccountHandle phoneAccount = telecomManager.getUserSelectedOutgoingPhoneAccount();
        List<PhoneAccountHandle> allPhoneAccounts = telecomManager.getCallCapablePhoneAccounts();
        Log.d(TAG,"allPhoneAccounts.size() = " + allPhoneAccounts.size());

        if (allPhoneAccounts == null) {
            Log.d(TAG,"There is no phone account, return out.");
            return;
        }

        if (slot1_hasIcc && slot2_hasIcc) {
            Log.d(TAG,"CASE1:  Slot1 & Slot2 all have IccCard");

            if (sub1_state == PROVISIONED) {
               if (sub2_state == PROVISIONED) {
                   Log.d(TAG,"SUB1 is active, SUB2 is active, Do Nothing.");
               } else {
                   Log.d(TAG,"SUB1 is active, SUB2 is de-active, so set SUB1 as default SMS/CALL/DATA sub");
                   if (sub1_id == -1) {
                       Log.d(TAG,"sub1_id is -1, not need set default sms/call/data. ");
                       return;
                   }

                   //Set user select phone count, depend on sub1
                   setUserSelectPhoneAccount(sub1_id);
                   try {
                       //only sim1 is active, not need set sms prompt
                       IExtTelephony extTelephony =
                               IExtTelephony.Stub.asInterface(ServiceManager.getService("extphone"));
                       extTelephony.setSMSPromptEnabled(false);
                   } catch (RemoteException ex) {
                       Log.e(TAG,"RemoteException @isSMSPromptEnabled" + ex);
                   } catch (NullPointerException ex) {
                       Log.e(TAG,"NullPointerException @isSMSPromptEnabled" + ex);
                   }

                   //set sub1 as default call/sms/data subscription
                   if (mSubscriptionManager.getDefaultVoiceSubscriptionId() != sub1_id) {
                       mSubscriptionManager.setDefaultVoiceSubId(sub1_id);
                   }
                   if (mSubscriptionManager.getDefaultSmsSubscriptionId() != sub1_id) {
                       mSubscriptionManager.setDefaultSmsSubId(sub1_id);
                   }
                   if (mSubscriptionManager.getDefaultDataSubscriptionId() != sub1_id) {
                       mSubscriptionManager.setDefaultDataSubId(sub1_id);
                   }
               }
            } else {
                if (sub2_state == PROVISIONED) {
                    Log.d(TAG,"SUB1 is de-active, SUB2 is active, so set SUB2 as default SMS/CALL/DATA sub");
                    if (sub2_id == -1) {
                        Log.d(TAG,"sub2_id is -1, not need set default sms/call/data. ");
                        return;
                    }

                    //Set user select phone count, depend on sub2
                    setUserSelectPhoneAccount(sub2_id);
                    try {
                        //only sim1 is active, not need set sms prompt
                        IExtTelephony extTelephony =
                                IExtTelephony.Stub.asInterface(ServiceManager.getService("extphone"));
                        extTelephony.setSMSPromptEnabled(false);
                    } catch (RemoteException ex) {
                        Log.e(TAG,"RemoteException @isSMSPromptEnabled" + ex);
                    } catch (NullPointerException ex) {
                        Log.e(TAG,"NullPointerException @isSMSPromptEnabled" + ex);
                    }

                    //set sub2 as default call/sms/data subscription
                    if (mSubscriptionManager.getDefaultVoiceSubscriptionId() != sub2_id) {
                        mSubscriptionManager.setDefaultVoiceSubId(sub2_id);
                    }
                    if (mSubscriptionManager.getDefaultSmsSubscriptionId() != sub2_id) {
                        mSubscriptionManager.setDefaultSmsSubId(sub2_id);
                    }
                    if (mSubscriptionManager.getDefaultDataSubscriptionId() != sub2_id) {
                        mSubscriptionManager.setDefaultDataSubId(sub2_id);
                    }
                } else {
                    Log.d(TAG,"SUB1 and SUB1 all de-active, it's a exception case");
                }
            }
        }
        /*
         * Removed by JRD_Zhanglei, Single SIM case in this method, it's not need do the fellow
         * action while single SIM. 2016/01/13, BEGIN
         *
        else if (slot1_hasIcc && !slot2_hasIcc) {
            log("CASE2:  Slot1 has IccCard, slot2 not has IccCard");
            if (sub1_state == PROVISIONED) {
                log("Slot1 has IccCard, slot2 is empty , set SUB1 as default SMS/CALL/DATA sub");
                if (sub1_id == -1) {
                    log("sub1_id is -1, not need set default sms/call/data. ");
                    return;
                }

                try {
                    mExtTelephony.setSMSPromptEnabled(false);
                } catch (RemoteException ex) {
                    loge("RemoteException @isSMSPromptEnabled" + ex);
                } catch (NullPointerException ex) {
                    loge("NullPointerException @isSMSPromptEnabled" + ex);
                }

                //set sub1 as default sms/call/data sub
                if (mSubscriptionManager.getDefaultVoiceSubId() != sub1_id) {
                    mSubscriptionManager.setDefaultVoiceSubId(sub1_id);
                }
                if (mSubscriptionManager.getDefaultSmsSubId() != sub1_id) {
                    mSubscriptionManager.setDefaultSmsSubId(sub1_id);
                }
                if (mSubscriptionManager.getDefaultDataSubId() != sub1_id) {
                    mSubscriptionManager.setDefaultDataSubId(sub1_id);
                }
            } else {
                log("Only slot1 has IccCard, slot1 can't be de-active, it's exception case");
            }
        } else if (!slot1_hasIcc && slot2_hasIcc) {
            log("CASE3:  Slot2 has IccCard, slot1 not has IccCard");
            if (sub2_state == PROVISIONED) {
                log("Slot2 has IccCard, slot1 is empty , set SUB2 as default SMS/CALL/DATA sub");
                if (sub2_id == -1) {
                    log("sub2_id is -1, not need set default sms/call/data. ");
                    return;
                }

                try {
                    mExtTelephony.setSMSPromptEnabled(false);
                } catch (RemoteException ex) {
                    loge("RemoteException @isSMSPromptEnabled" + ex);
                } catch (NullPointerException ex) {
                    loge("NullPointerException @isSMSPromptEnabled" + ex);
                }

                //set sub2 as default sms/call/data sub
                if (mSubscriptionManager.getDefaultVoiceSubId() != sub2_id) {
                    mSubscriptionManager.setDefaultVoiceSubId(sub2_id);
                }
                if (mSubscriptionManager.getDefaultSmsSubId() != sub2_id) {
                    mSubscriptionManager.setDefaultSmsSubId(sub2_id);
                }
                if (mSubscriptionManager.getDefaultDataSubId() != sub2_id) {
                    mSubscriptionManager.setDefaultDataSubId(sub2_id);
                }
            } else {
                log("Only slot2 has IccCard, slot2 can't be de-active, it's a exception case");
            }
        }
        *
        * Removed single SIM case, END
        */
    }

    private void updatesim() {
        for (int i = 0; i < mNumSlots; ++i) {
            mUiccProvisionStatus[i] = getSlotProvisionStatus(i);
        }
        updateSimEnable();
        updateDataRoaming();
        updateSimName();
        updateSimNumber();
        updateOperator();
        updateAPN();
        updateSimApp();
    }

    private void updateSimEnable(){
        boolean subValid = isCurrentSubValid();
        boolean currentSimActivated = (getProvisionStatus(mSlotId)== PROVISIONED);
        mSimEnablePreference.setEnabled(subValid);
        mSimEnablePreference.setChecked(subValid && currentSimActivated);
    }

    private void updateDataRoaming(){
        boolean subValid = isCurrentSubValid();
        boolean currentSimActivated = (getProvisionStatus(mSlotId)== PROVISIONED);
        mDataRoamingPreference.setDisabledByAdmin(false);
        mDataRoamingPreference.setEnabled(subValid && currentSimActivated);
        if (mDataRoamingPreference.isEnabled()) {
            if (RestrictedLockUtils.hasBaseUserRestriction(mContext,
                    UserManager.DISALLOW_DATA_ROAMING, UserHandle.myUserId())) {
                mDataRoamingPreference.setEnabled(false);
            } else {
                mDataRoamingPreference.checkRestrictionAndSetDisabled(UserManager.DISALLOW_DATA_ROAMING);
            }
        }
        if (!mDataRoamingPreference.isEnabled()){
            if(alertDataRoamingDialog !=null &&alertDataRoamingDialog.isShowing()){
                alertDataRoamingDialog.dismiss();
            }
        }
        if (needupdateDataRoaming == true) {
            mDataRoamingPreference.setChecked(mTelephonyManager.getDataRoamingEnabled(mSlotId));
        }
    }

    private void updateSimName(){
        boolean subValid = isCurrentSubValid();
        boolean currentSimActivated = (getProvisionStatus(mSlotId)== PROVISIONED);
        mSimNamePreference.setEnabled(subValid && currentSimActivated);
        SubscriptionInfo sir = mSubscriptionManager.getActiveSubscriptionInfoForSimSlotIndex(mSlotId);
        if (sir != null) {
            if(sir.getDisplayName()!=null){
                mSimNamePreference.setDetail(sir.getDisplayName().toString());
            }
        }
    }

    private void updateSimNumber(){
        boolean subValid = isCurrentSubValid();
        boolean currentSimActivated = (getProvisionStatus(mSlotId)== PROVISIONED);
        mSimNumberPreference.setEnabled(subValid && currentSimActivated);
        SubscriptionInfo sir = mSubscriptionManager.getActiveSubscriptionInfoForSimSlotIndex(mSlotId);
        if (sir != null) {
            if(sir.getNumber()!=null){
                mSimNumberPreference.setDetail(sir.getNumber().toString());
            }
        }
    }

    private void updateOperator(){
//        PreferenceScreen prefSet = getPreferenceScreen();
        boolean subValid = isCurrentSubValid();
        boolean currentSimActivated = (getProvisionStatus(mSlotId)== PROVISIONED);
        int phoneType = TelephonyManager.getDefault().getCurrentPhoneTypeForSlot(mSlotId);
        boolean iscdmaphone = (phoneType == PhoneConstants.PHONE_TYPE_CDMA) ? true : false;
        mOperatorPreference.setEnabled(subValid && currentSimActivated && !iscdmaphone);
        if(subValid && currentSimActivated && !iscdmaphone ){
            mSimOtherCategory.addPreference(mOperatorPreference);
        } else {
            mSimOtherCategory.removePreference(mOperatorPreference);
        }
    }

    private void updateAPN(){
        boolean subValid = isCurrentSubValid();
        boolean currentSimActivated = (getProvisionStatus(mSlotId)== PROVISIONED);
        mAPNPreference.setEnabled(subValid && currentSimActivated);
    }

    private void updateSimApp(){
//        PreferenceScreen prefSet = getPreferenceScreen();
        PackageManager pm = mContext.getPackageManager();
        if(pm==null){
            mSimOtherCategory.removePreference(mSimAppPreference);
            return;
        }
        boolean subValid = isCurrentSubValid();
        boolean currentSimActivated = (getProvisionStatus(mSlotId)== PROVISIONED);
        mSimAppPreference.setEnabled(subValid && currentSimActivated);
        ComponentName cName;
        if(stk_dualicon==true){
            String[] launcherActivity = {
                    "com.android.stk.StkLauncherActivity0",
                    "com.android.stk.StkLauncherActivity1"
            };
            cName = new ComponentName("com.android.stk", launcherActivity[mSlotId]);
        }else{
            cName = new ComponentName("com.android.stk", "com.android.stk.StkMain");
        }
        boolean stkenable = (PackageManager.COMPONENT_ENABLED_STATE_ENABLED ==
                pm.getComponentEnabledSetting(cName));
        if ((stk_install || stkenable) && subValid && currentSimActivated) {
            mSimOtherCategory.addPreference(mSimAppPreference);
        } else {
            mSimOtherCategory.removePreference(mSimAppPreference);
        }
    }

    @Override
    public void onDestroy() {
        mContext.unregisterReceiver(mReceiver);
        Log.d(TAG,"on onDestroy");
        super.onDestroy();
    }

    @Override
    protected void onResume() {
        Log.d(TAG,"onResume");
        super.onResume();
        mSubscriptionManager.addOnSubscriptionsChangedListener(mOnSubscriptionsChangeListener);
        updatesim();
    }

    @Override
    protected void onPause() {
        super.onPause();
        mSubscriptionManager.removeOnSubscriptionsChangedListener(mOnSubscriptionsChangeListener);
        cleanUpPendingDialogs();
    }

     /* MODIFIED-BEGIN by qili.zhang, 2016-09-26,BUG-2994458*/
     @Override
    public boolean onOptionsItemSelected(MenuItem item) {
       if (item.getItemId() == android.R.id.home) {
            finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
    /* MODIFIED-END by qili.zhang,BUG-2994458*/
     /* MODIFIED-BEGIN by chengbiao.hu, 2016-11-14,BUG-3458921*/
     public boolean onPreferenceChange(Preference preference, Object objValue) {
         if (preference == mSimEnablePreference){
             mSimEnableIsChecked = mSimEnablePreference.isChecked();
             Log.d(TAG,"onPreferenceChange: mSimEnableIsChecked:"+mSimEnableIsChecked);
             handleUserRequest();
         } else if (preference == mDataRoamingPreference) {
             Log.d(TAG,"onPreferenceTreeClick: preference == mDataRoamingPreference.");

             //normally called on the toggle click
             if (!mDataRoamingPreference.isChecked()) {
                 if (!getResources().getBoolean(R.bool.feature_show_roaming_warning)) {
                     boolean success = mTelephonyManager.setDataRoamingEnabled(mSlotId, true);
                     if (success == true) {
                         mOkClicked = true;
                     }
                 } else {
                     // First confirm with a warning dialog about charges
                     mOkClicked = false;
                     needupdateDataRoaming = false;
                     int wariningId = R.string.tct_roaming_warning;

                     if (alertDataRoamingDialog == null) {
                         AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(mContext);
                         alertDialogBuilder.setTitle(R.string.roaming_alert_title);
                         alertDialogBuilder.setMessage(mContext.getResources().getString(wariningId));

                         alertDialogBuilder.setPositiveButton(R.string.yes,
                                 new DialogInterface.OnClickListener() {
                                     @Override
                                     public void onClick(DialogInterface dialog, int id) {
                                         boolean success = mTelephonyManager.setDataRoamingEnabled(
                                                  mSlotId, true);
                                         if (success == true) {
                                             mOkClicked = true;
                                         }
                                         dialog.dismiss();
                                     }
                         });
                         alertDialogBuilder.setNegativeButton(R.string.no,
                                 new DialogInterface.OnClickListener() {
                                     @Override
                                     public void onClick(DialogInterface dialog, int id) {
                                         mDataRoamingPreference.setChecked(false);
                                         dialog.dismiss();
                                     }
                         });

                         alertDataRoamingDialog = alertDialogBuilder.create();
                         alertDataRoamingDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
                                     @Override
                                     public void onDismiss(DialogInterface dialog) {
                                         needupdateDataRoaming = true;
                                         mDataRoamingPreference.setChecked(mOkClicked);
                                         alertDataRoamingDialog = null;
                                     }
                         });
                     }
                     alertDataRoamingDialog.show();
                 }

             } else {
                 mTelephonyManager.setDataRoamingEnabled(mSlotId,false);
             }
             return true;
         }
         return true;
     }
     /* MODIFIED-END by chengbiao.hu,BUG-3458921*/

    public int getNumOfSubsProvisioned() {
        int activeSubInfoCount = 0;
        List<SubscriptionInfo> subInfoLists = mSubscriptionManager.getActiveSubscriptionInfoList();
        if (subInfoLists != null) {
            for (SubscriptionInfo subInfo : subInfoLists) {
                if (getProvisionStatus(subInfo.getSimSlotIndex()) == PROVISIONED) {
                    activeSubInfoCount++;
                }
            }
        }
        return activeSubInfoCount;
    }

    private void sendMessage(int event, Handler handler, int delay) {
        Message message = handler.obtainMessage(event);
        handler.sendMessageDelayed(message, delay);
    }

    private void sendMessage(int event, Handler handler, int delay, int arg1, int arg2) {
        Message message = handler.obtainMessage(event, arg1, arg2);
        handler.sendMessageDelayed(message, delay);
    }

    // This internal method called when user changes preference from UI
    // 1. For activation/deactivation request from User, if device in APM mode
    //    OR if voice call active on any SIM it dispay error dialog and returns.
    // 2. For deactivation request it returns error dialog if only one SUB in
    //    active state.
    // 3. In other cases it sends user request to framework.
    synchronized private void handleUserRequest() {
        if (isAirplaneModeOn()) {
            // do nothing but warning
            Log.d(TAG,"APM is on, EXIT!");
            showAlertDialog(ERROR_ALERT_DLG_ID, R.string.sim_enabler_airplane_on);
            return;
        }
        for (int i = 0; i < mPhoneCount; i++) {
            int[] subId = SubscriptionManager.getSubId(i);
            //when voice call in progress, subscription can't be activate/deactivate.
            if (TelephonyManager.getDefault().getCallState(subId[0])
                != TelephonyManager.CALL_STATE_IDLE) {
                Log.d(TAG,"Call state for phoneId: " + i + " is not idle, EXIT!");
                showAlertDialog(ERROR_ALERT_DLG_ID, R.string.sim_enabler_in_call);
                return;
            }
        }

        if (mSimEnableIsChecked) {
            if (getNumOfSubsProvisioned() > 1) {
                Log.d(TAG,"More than one sub is active, Deactivation possible.");
                sendUiccProvisioningRequest();
            } else {
                Log.d(TAG,"Only one sub is active. Deactivation not possible.");
                showAlertDialog(ERROR_ALERT_DLG_ID, R.string.sim_enabler_both_inactive);
                return;
            }
        } else {
            Log.d(TAG,"Activate the sub");
            sendUiccProvisioningRequest();
        }
    }
    private void sendUiccProvisioningRequest() {
        new SimEnablerDisabler().execute();
    }

    private class SimEnablerDisabler extends AsyncTask<Void, Void, Integer> {

        int newProvisionedState = NOT_PROVISIONED;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            mSimEnableCmdInProgress = true;
            showProgressDialog();
        }

        @Override
        protected Integer doInBackground(Void... params) {
            int result = -1;
            newProvisionedState = NOT_PROVISIONED;
            try {
                IExtTelephony extTelephony =
                        IExtTelephony.Stub.asInterface(ServiceManager.getService("extphone"));
                if (!mSimEnableIsChecked) {
                    result = extTelephony.activateUiccCard(mSlotId);
                    newProvisionedState = PROVISIONED;
                } else {
                    result = extTelephony.deactivateUiccCard(mSlotId);
                }
            } catch (RemoteException ex) {
                Log.e(TAG,"Activate  sub failed " + result + " phoneId " + mSlotId);
            } catch (NullPointerException ex) {
                Log.e(TAG,"Failed to activate sub Exception: " + ex);
            }
            return result;
        }

        @Override
        protected void onPostExecute(Integer result) {
            processSetUiccDone(result.intValue(), newProvisionedState);
        }
    }

    private void processSetUiccDone(int result, int newProvisionedState) {
        sendMessage(EVT_UPDATE, mHandler, MSG_DELAY_TIME);
        sendMessage(EVT_SHOW_RESULT_DLG, mHandler, MSG_DELAY_TIME, result, newProvisionedState);
        mSimEnableCmdInProgress = false;
    }

    private void showAlertDialog(int dialogId, int msgId) {
        SubscriptionInfo sir = mSubscriptionManager
                .getActiveSubscriptionInfoForSimSlotIndex(mSlotId);
        String title;
        if(sir != null && sir.getDisplayName()!=null && !TextUtils.isEmpty(sir.getDisplayName().toString())){
            title = sir.getDisplayName().toString();
        } else {
            title = String.format(mContext.getResources().getString(R.string.tct_dualsim_simcard_title),(mSlotId + 1));
        }
        // Confirm only one AlertDialog instance to show.
        dismissDialog(mSimEnableAlertDialog);
        dismissDialog(mSimEnableProgressDialog);
        // Create dialog only if Fragment is visible

        AlertDialog.Builder builder = new AlertDialog.Builder(mContext).setIcon(
                android.R.drawable.ic_dialog_alert).setTitle(title);

        switch (dialogId) {
            case CONFIRM_ALERT_DLG_ID:
                String message;
                message = mContext.getString(R.string.sim_enabler_need_disable_sim);
                builder.setMessage(message);
                builder.setPositiveButton(android.R.string.ok, mDialogClickListener);
                builder.setNegativeButton(android.R.string.no, mDialogClickListener);
                builder.setOnCancelListener(mDialogCanceListener);
                break;

            case ERROR_ALERT_DLG_ID:
                builder.setMessage(mContext.getString(msgId));
                builder.setNeutralButton(android.R.string.ok, mDialogClickListener);
                builder.setCancelable(false);
                break;

            case RESULT_ALERT_DLG_ID:
                String msg = !mSimEnableIsChecked ? mContext
                        .getString(R.string.sub_activate_success) : mContext
                        .getString(R.string.sub_deactivate_success);
                builder.setMessage(msg);
                builder.setNeutralButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                        @Override
                         public void onClick(DialogInterface dialog, int which) {
                            // TODO Auto-generated method stub
                             dialog.dismiss() ;
                         }
                      });
                updateDefaultValues();
                mSimEnableAlertDialog = builder.create();
                mSimEnableAlertDialog.setCanceledOnTouchOutside(false);
//                mSimEnableAlertDialog.show();
                return;
            default:
                break;
        }

        mSimEnableAlertDialog = builder.create();
        mSimEnableAlertDialog.setCanceledOnTouchOutside(false);
        mSimEnableAlertDialog.show();

    }

    private void showProgressDialog() {
        SubscriptionInfo sir = mSubscriptionManager
                .getActiveSubscriptionInfoForSimSlotIndex(mSlotId);
        String title;
        if(sir != null && sir.getDisplayName()!=null && !TextUtils.isEmpty(sir.getDisplayName().toString())){
            title = sir.getDisplayName().toString();
        } else {
            title = String.format(mContext.getResources().getString(R.string.tct_dualsim_simcard_title),(mSlotId + 1));
        }
        String msg = mContext.getString(!mSimEnableIsChecked ? R.string.sim_enabler_enabling
                : R.string.sim_enabler_disabling);
        dismissDialog(mSimEnableProgressDialog);
        // Create dialog only if Fragment is visible

        mSimEnableProgressDialog = new ProgressDialog(mContext);
        mSimEnableProgressDialog.setIndeterminate(true);
        mSimEnableProgressDialog.setTitle(title);
        mSimEnableProgressDialog.setMessage(msg);
        mSimEnableProgressDialog.setCancelable(false);
        mSimEnableProgressDialog.setCanceledOnTouchOutside(false);
        mSimEnableProgressDialog.show();

        sendMessage(EVT_PROGRESS_DLG_TIME_OUT, mHandler, PROGRESS_DLG_TIME_OUT);

    }
    private void dismissDialog(Dialog dialog) {
        if((dialog != null) && (dialog.isShowing())) {
            dialog.dismiss();
            dialog = null;
        }
    }

    public void cleanUpPendingDialogs() {
        dismissDialog(mSimEnableProgressDialog);
        dismissDialog(mSimEnableAlertDialog);
    }

    private DialogInterface.OnClickListener mDialogClickListener = new DialogInterface.OnClickListener() {
        public void onClick(DialogInterface dialog, int which) {
            if (which == DialogInterface.BUTTON_POSITIVE) {
                dismissDialog(mSimEnableAlertDialog);
                sendUiccProvisioningRequest();
            } else if (which == DialogInterface.BUTTON_NEGATIVE) {
                updateSimEnable();
            } else if (which == DialogInterface.BUTTON_NEUTRAL) {
                updateSimEnable();
            }
            if(mSimEnableAlertDialog!=null && mSimEnableAlertDialog.isShowing()){
                mSimEnableAlertDialog.dismiss();
            }
        }
    };

    private DialogInterface.OnCancelListener mDialogCanceListener = new DialogInterface.OnCancelListener() {
        public void onCancel(DialogInterface dialog) {
            updateSimEnable();
        }
    };

    private Handler mHandler = new Handler() {
            @Override
            public void handleMessage(Message msg) {

                switch(msg.what) {
                    case EVT_UPDATE:
                        updateSimEnable();

                    case EVT_SHOW_RESULT_DLG:
                        int result = msg.arg1;
                        int newProvisionedState = msg.arg2;
                        Log.d(TAG,"EVT_SHOW_RESULT_DLG result: " + result +
                                " new provisioned state " + newProvisionedState);
                        updateSimEnable();
                        if (result != REQUEST_SUCCESS) {
                            int msgId = (newProvisionedState == PROVISIONED) ?
                                    R.string.sub_activate_failed :
                                    R.string.sub_deactivate_failed;
                            showAlertDialog(ERROR_ALERT_DLG_ID, msgId);
                        } else {
                            showAlertDialog(RESULT_ALERT_DLG_ID, 0);
                        }
                        mHandler.removeMessages(EVT_PROGRESS_DLG_TIME_OUT);
                        break;

                    case EVT_SHOW_PROGRESS_DLG:
                        Log.d(TAG,"EVT_SHOW_PROGRESS_DLG");
                        showProgressDialog();
                        break;

                    case EVT_PROGRESS_DLG_TIME_OUT:
                        Log.d(TAG,"EVT_PROGRESS_DLG_TIME_OUT");
                        dismissDialog(mSimEnableProgressDialog);

                        updateSimEnable();
                        break;

                    default:
                    break;
                }
            }
        };

    private final SubscriptionManager.OnSubscriptionsChangedListener mOnSubscriptionsChangeListener = new SubscriptionManager.OnSubscriptionsChangedListener() {
        @Override
        public void onSubscriptionsChanged() {
            Log.d(TAG,"onSubscriptionsChanged:");
            updatesim();
        }
    };

    private final BroadcastReceiver mReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            Log.d(TAG, "Intent received: " + action);
            if (ACTION_UICC_MANUAL_PROVISION_STATUS_CHANGED.equals(action)) {
                int phoneId = intent.getIntExtra(PhoneConstants.PHONE_KEY,
                        SubscriptionManager.INVALID_SUBSCRIPTION_ID);
                int newProvisionedState = intent.getIntExtra(EXTRA_NEW_PROVISION_STATE,
                        NOT_PROVISIONED);
                updatesim();
                Log.d(TAG, "Received ACTION_UICC_MANUAL_PROVISION_STATUS_CHANGED on phoneId: "
                         + phoneId + " new sub state " + newProvisionedState);
            } else if (ACTION_STK_SET_APP_STATE_COMPLETED.equals(action)){
                stk_slotId = intent.getIntExtra(STK_SLOTID,-1);
                boolean install = intent.getBooleanExtra(APP_INSTALL, false);
                if(stk_slotId==mSlotId){
                    stk_install = install;
                }
                Log.d(TAG, "Received ACTION_STK_SET_APP_STATE_COMPLETED  stk_slotId:" + stk_slotId
                        + " ,stk_dualicon:" + stk_dualicon + " ,install:" + install + " ,mSlotId:"
                        + mSlotId + " ,stk_install:" + stk_install);
                updatesim();
            }
        }
    };
}
