/* Copyright (C) 2016 Tcl Corporation Limited */

/******************************************************************************/
/*     Modifications on Features list / Changes Request / Problems Report     */
/* -------------------------------------------------------------------------- */
/*    date   |        author        |         Key          |     comment      */
/* ----------|----------------------|----------------------|----------------- */
/* 2016/11/17|     jianhong.yang    |     task 3476360     |  Ergo development*/
/*           |                      |                      |  create new file */
/* ----------|----------------------|----------------------|----------------- */

package com.android.settings;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.net.wifi.WifiManager;
import android.os.Handler;
import android.os.IBinder;
import android.os.Binder;
import android.os.Message;
import android.os.RemoteException;
import android.os.ServiceManager;
import android.os.UserHandle;
import android.provider.Settings;
import android.util.Log;
import android.widget.Toast;

import java.util.Timer;
import java.util.TimerTask;

import android.net.INetworkStatsService;

public class HotSpotTrafficService extends Service {
    public static final boolean DEBUG = true;
    public static final String MAX_DATA_USAGE_LIMIT_NUM = "max_data_usage_limit_num";
    public static final String HOTSPOT_START_POINT_DATA_USAGE_NUM = "hotspot_start_point_data_usage_num";

    private static final int TIMER_SCHEDULE = 0;
    private static final String TAG = "HotspotTrafficService";
    private Timer mTimer;
    private HotSpotTrafficListener mHotSpotTrafficListener;
    private static long mHotspotStartPointDataUsageNum;
    private static long mWifiHotspotTotalTraffic;
    private static long mPersistWifiHotspotTotalTraffic = 0;
    private static long mWifiHotspotMaxTraffic = Long.MAX_VALUE;
    private static boolean mIsRunning = false;
    private WifiManager mWifiManager;
    private INetworkStatsService mStatsService;

    private BroadcastReceiver mReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if (WifiManager.WIFI_AP_STATE_CHANGED_ACTION.equals(action)) {
                int wifiApState = intent.getIntExtra(WifiManager.EXTRA_WIFI_AP_STATE, WifiManager.WIFI_AP_STATE_FAILED);
                if (wifiApState == WifiManager.WIFI_AP_STATE_DISABLED) {
                    if (DEBUG) Log.d(TAG,"Wifi ap is disabled, stop this service");
                    stopTimer();
                    mPersistWifiHotspotTotalTraffic = 0;
                    mWifiHotspotTotalTraffic = 0;
                    HotSpotTrafficService.this.stopSelf();
                }
            }
        }
    };

    private Handler mHandler = new Handler() {
        @Override
        public void handleMessage(Message message) {
            switch (message.what) {
                case TIMER_SCHEDULE:
                  /*  try {
                        long currentData = mStatsService.getTetherStatsBytes();
                        if (currentData == 0) { //if currentData = 0 means that moblie data has been disabled
                            mPersistWifiHotspotTotalTraffic = mWifiHotspotTotalTraffic;
                            mHotspotStartPointDataUsageNum = currentData;
                        }
                        mWifiHotspotTotalTraffic = currentData - mHotspotStartPointDataUsageNum + mPersistWifiHotspotTotalTraffic;
                    } catch (RemoteException e) {
                        throw new RuntimeException(e);
                    }*/
                    if (mWifiHotspotTotalTraffic >= mWifiHotspotMaxTraffic ) {
                        mWifiHotspotTotalTraffic = mWifiHotspotMaxTraffic;

                        if (DEBUG) Log.d(TAG,"Wifi Hotspot reach the MAX traffic, Close wifi hotspot");
                        if (mWifiManager != null) {
                            //mWifiManager.setWifiApEnabled(null, false);
                            //setNotificationVisible(true,0,true);
                            //Toast.makeText(getApplicationContext(), R.string.reah_max_traffic_notice, Toast.LENGTH_LONG).show();
                        }
                    }

                    if (mWifiHotspotTotalTraffic < 0) mWifiHotspotTotalTraffic = 0;

                    if (mHotSpotTrafficListener != null) {
                        mHotSpotTrafficListener.onTrafficChange(mWifiHotspotTotalTraffic);
                    }

                    break;
                default:
                    break;
            }
        }
    };

    public void setHotspotTrafficListener(HotSpotTrafficListener listener) {
        if (DEBUG) Log.d(TAG,"setHotspotTrafficListener");
        mHotSpotTrafficListener = listener;
    }

    public void setHotspotMaxTraffic(int maxMB) {
        if (DEBUG) Log.d(TAG,"setHotspotMaxTraffic = " + maxMB + " MB");
        mWifiHotspotMaxTraffic = maxMB * 1024 * 1024;
    }

    private void stopTimer() {
        if (mTimer != null) {
            mTimer.cancel();
            mTimer = null;
            mIsRunning = false;
        }
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        if (DEBUG) Log.d(TAG,"onStartCommand Timer is running " + mIsRunning);
        long defValue = 0;
        /*try {
            defValue = mStatsService.getTetherStatsBytes();
        } catch (RemoteException e) {
            throw new RuntimeException(e);
        }*/

        Long maxDataUsageLimitNumMB = Settings.Global.getLong(getContentResolver(),
                MAX_DATA_USAGE_LIMIT_NUM, Long.MAX_VALUE);
        mHotspotStartPointDataUsageNum = Settings.Global.getLong(getContentResolver(),
                HOTSPOT_START_POINT_DATA_USAGE_NUM, defValue);

        mWifiHotspotMaxTraffic = maxDataUsageLimitNumMB * 1024 * 1024;
        if(mWifiHotspotMaxTraffic == 0) {
            mWifiHotspotMaxTraffic = Long.MAX_VALUE;
        }
        if (!mIsRunning) {
            TimerTask task = new TimerTask() {
                public void run() {
                    Message message = mHandler.obtainMessage();
                    message.what = TIMER_SCHEDULE;
                    mHandler.sendMessage(message);
                }
            };
            mTimer = new Timer(true);
            mTimer.schedule(task, 500, 500);
            mIsRunning = true;
        }
        return super.onStartCommand(intent, flags, startId);
    }

    @Override
    public IBinder onBind(Intent intent) {
        return new MyBinder();
    }

    public class MyBinder extends Binder {
        public HotSpotTrafficService getService() {
            return HotSpotTrafficService.this;
        }
    }

    @Override
    public void onCreate() {
        super.onCreate();
        IntentFilter filter = new IntentFilter();
        filter.addAction(WifiManager.WIFI_AP_STATE_CHANGED_ACTION);
        this.registerReceiver(mReceiver, filter);
        mWifiManager = (WifiManager) getSystemService(Context.WIFI_SERVICE);
        mStatsService = INetworkStatsService.Stub.asInterface(
                ServiceManager.getService(Context.NETWORK_STATS_SERVICE));
    }

    private boolean mNotificationShown = false;
    private void setNotificationVisible(boolean visible, int numNetworks, boolean force) {

        // If it should be hidden and it is already hidden, then noop
        if (!visible && !mNotificationShown && !force) {
            return;
        }

        Context mContext = getBaseContext();
        NotificationManager notificationManager = (NotificationManager) mContext
                .getSystemService(Context.NOTIFICATION_SERVICE);
        Notification.Builder mNotificationBuilder = null;
        int icon = com.android.internal.R.drawable.stat_sys_tether_wifi;

        Intent intent = new Intent();
        intent.setClassName("com.android.settings", "com.android.settings.TetherSettings");
        intent.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
        PendingIntent pi = PendingIntent.getActivityAsUser(mContext, 0, intent, 0,
                null, UserHandle.CURRENT);

        if (visible) {

            if (mNotificationBuilder == null) {
                // Cache the Notification builder object.
                mNotificationBuilder = new Notification.Builder(mContext)
                        .setWhen(0)
                        .setSmallIcon(icon)
                        .setAutoCancel(true)
                        .setContentIntent(pi);
            }

            Resources r = mContext.getResources();
            CharSequence title = r.getText(R.string.restrict_data_traffic);
            CharSequence details = r.getText(R.string.reah_max_traffic_notice);

            mNotificationBuilder.setContentTitle(title);
            mNotificationBuilder.setContentText(details);

            notificationManager.notifyAsUser(null,icon,
                    mNotificationBuilder.build(), UserHandle.ALL);
        } else {
            notificationManager.cancelAsUser(null, icon, UserHandle.ALL);
        }

        mNotificationShown = visible;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        stopTimer();
        this.unregisterReceiver(mReceiver);
    }
}
