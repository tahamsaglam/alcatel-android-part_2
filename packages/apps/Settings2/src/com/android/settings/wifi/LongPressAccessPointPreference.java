/*
 * Copyright (C) 2015 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.android.settings.wifi;

import android.app.Fragment;
import android.content.Context;
import android.content.pm.PackageManager;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.StateListDrawable;
import android.net.wifi.WifiConfiguration;
import android.os.Looper;
import android.os.UserHandle;
import mst.preference.Preference;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.util.SparseArray;
import android.view.View;
import android.widget.TextView;

import com.android.settings.R;
import com.android.settingslib.wifi.AccessPoint;
import com.android.settingslib.wifi.AccessPointPreference;

public class LongPressAccessPointPreference extends AccessPointPreference {

    private final Fragment mFragment;

    // Used for dummy pref.
    public LongPressAccessPointPreference(Context context, AttributeSet attrs) {
        super(context, attrs);
        mFragment = null;
    }

    public LongPressAccessPointPreference(AccessPoint accessPoint, Context context,
            UserBadgeCache cache, boolean forSavedNetworks, Fragment fragment) {
        super(accessPoint, context, cache, forSavedNetworks);
        mFragment = fragment;
    }

    @Override
    public void onBindView(final View view) {
        super.onBindView(view);
        if (mFragment != null) {
            //[FEATURE]Mod-BEGIN TCTNB.jianhong.yang,2016/10/20,task 3046476
            //view.setOnCreateContextMenuListener(mFragment);
            view.setTag(this);
            //view.setLongClickable(true);
            //[FEATURE]Mod-END TCTNB.jianhong.yang
        }
    }
}
