/* Copyright (C) 2016 Tcl Corporation Limited */

/******************************************************************************/
/*     Modifications on Features list / Changes Request / Problems Report     */
/* -------------------------------------------------------------------------- */
/*    date   |        author        |         Key          |     comment      */
/* ----------|----------------------|----------------------|----------------- */
/* 2016/10/11|     jianhong.yang    |     task 3046476     |  Ergo development*/
/*           |                      |                      |  create new file */
/* ----------|----------------------|----------------------|----------------- */

package com.android.settings.wifi;

import java.util.Map;

import android.content.Context;
import android.content.SharedPreferences;
import android.net.wifi.WifiConfiguration;
import android.util.Log;

import com.android.settingslib.wifi.AccessPoint;

public class MarkPayApManager {
     private Context mContext;
     private SharedPreferences mSharedPreference;
     public static final String MARKED_PAY_AP_LIST = "markedPayApList";

     public MarkPayApManager(Context context) {
         mContext = context;
         mSharedPreference = mContext.getSharedPreferences(MARKED_PAY_AP_LIST, Context.MODE_PRIVATE);
     }

    public boolean add(AccessPoint accessPoint){
        if (accessPoint == null || mSharedPreference == null) {
            return false;
        }

        if(!accessPoint.isSaved()) {
            return false;
        }

        return mSharedPreference.edit().putString("" + accessPoint.getNetworkId(),
                AccessPoint.removeDoubleQuotes(accessPoint.getSsidStr())).commit();
    }

    public boolean add(int networkId,String ssid){
        if (mSharedPreference == null ||
                networkId == WifiConfiguration.INVALID_NETWORK_ID ||
                ssid == null) {
            return false;
        }
        ssid = AccessPoint.removeDoubleQuotes(ssid);

        return mSharedPreference.edit().putString(networkId + "",
                AccessPoint.removeDoubleQuotes(ssid)).commit();
    }

    public boolean remove(AccessPoint accessPoint){
        if (accessPoint == null || mSharedPreference == null) {
            return false;
        }

        if(!accessPoint.isSaved()) {
            mSharedPreference.edit().remove(accessPoint.getNetworkId() + "").commit();
            return true;
        }

        return false;
    }

    public boolean remove(int networkId){
        if (mSharedPreference == null || networkId == WifiConfiguration.INVALID_NETWORK_ID) {
            return false;
        }

        return mSharedPreference.edit().remove(networkId + "").commit();
    }

    public boolean removeAll() {
        if (mSharedPreference == null) {
            return false;
        }

        mSharedPreference.edit().clear().commit();
        return true;
    }

    public boolean isMarkedPayAp(AccessPoint accessPoint) {
        if (accessPoint == null || mSharedPreference == null) {
            return false;
        }

        if(!accessPoint.isSaved()) {
            return false;
        }

        if(accessPoint.getSsidStr().equals(mSharedPreference.getString(accessPoint.getNetworkId() + "",null))) {
            return true;
        }
        return false;
    }

    public boolean isMarkedPayAp(int networkId,String ssid) {
        if (mSharedPreference == null ||
                networkId == WifiConfiguration.INVALID_NETWORK_ID ||
                ssid == null) {
            return false;
        }
        ssid = AccessPoint.removeDoubleQuotes(ssid);

        if(ssid.equals(AccessPoint.removeDoubleQuotes(mSharedPreference.getString(networkId + "",null)))) {
            return true;
        }

        return false;
    }

    public Map<String,?> getMarkedPayApList() {
        if (mSharedPreference == null) {
            return null;
        }

        Map<String,?> map = mSharedPreference.getAll();

        return map;
    }
}