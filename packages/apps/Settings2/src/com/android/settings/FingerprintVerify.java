package com.android.settings;

import android.app.Activity;
import android.app.admin.DevicePolicyManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.hardware.fingerprint.FingerprintManager;
import android.os.Bundle;
import android.os.CancellationSignal;
import android.os.Handler;
import android.os.UserHandle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;
import com.android.internal.widget.LockPatternUtils;

/**
 * Created by liang.zhang1 on 9/26/16.
 */
public class FingerprintVerify extends Activity implements View.OnClickListener{
    private final static String TAG = "FingerprintVerify";

    private static final int MSG_FINGER_AUTH_SUCCESS = 1001;
    private static final int MSG_FINGER_AUTH_FAIL = 1002;
    private static final int MSG_FINGER_AUTH_ERROR = 1003;
    private static final int MSG_FINGER_AUTH_HELP = 1004;

    private static final int CONFIRM_REQUEST = 101;

    private FingerprintManager mFingerprintManager;
    private CancellationSignal mFingerprintCancel;
    private boolean mInFingerprintLockout;
    private int mUserId;

    private Button mVerifyCancel;
    private Button mVerifyOther;

    private LockPatternUtils mLockPatternUtils;
    private static final String KEY_UNLOCK_SET_OFF = "unlock_set_off";
    private static final String KEY_UNLOCK_SET_NONE = "unlock_set_none";
    private static final String KEY_UNLOCK_SET_PIN = "unlock_set_pin";
    private static final String KEY_UNLOCK_SET_PASSWORD = "unlock_set_password";
    private static final String KEY_UNLOCK_SET_PATTERN = "unlock_set_pattern";
    private static final String KEY_UNLOCK_SET_MANAGED = "unlock_set_managed";

    private FingerprintManager.AuthenticationCallback mAuthCallback = new FingerprintManager.AuthenticationCallback() {
        @Override
        public void onAuthenticationSucceeded(FingerprintManager.AuthenticationResult result) {
            int fingerId = result.getFingerprint().getFingerId();
            mHandler.obtainMessage(MSG_FINGER_AUTH_SUCCESS, fingerId, 0).sendToTarget();
        }

        @Override
        public void onAuthenticationFailed() {
            mHandler.obtainMessage(MSG_FINGER_AUTH_FAIL).sendToTarget();
        };

        @Override
        public void onAuthenticationError(int errMsgId, CharSequence errString) {
            mHandler.obtainMessage(MSG_FINGER_AUTH_ERROR, errMsgId, 0, errString)
                    .sendToTarget();
        }

        @Override
        public void onAuthenticationHelp(int helpMsgId, CharSequence helpString) {
            mHandler.obtainMessage(MSG_FINGER_AUTH_HELP, helpMsgId, 0, helpString)
                    .sendToTarget();
        }
    };

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.finger_print_verify);

        mUserId = getIntent().getIntExtra(
                Intent.EXTRA_USER_ID, UserHandle.myUserId());

        mFingerprintManager = (FingerprintManager) getSystemService(
                Context.FINGERPRINT_SERVICE);
        mInFingerprintLockout = false;

        mVerifyCancel = (Button) findViewById(R.id.verify_cancel);
        mVerifyOther = (Button) findViewById(R.id.verify_other);
        mVerifyCancel.setOnClickListener(this);
        mVerifyOther.setOnClickListener(this);
        setFinishOnTouchOutside(false);
        mLockPatternUtils = new LockPatternUtils(this);

    }

    @Override
    public void onClick(View v) {
        if(v.getId() == R.id.verify_cancel) {
            finish();
        } else if(v.getId() == R.id.verify_other) {
            ChooseLockSettingsHelper helper = new ChooseLockSettingsHelper(this);
            helper.launchConfirmationActivity(CONFIRM_REQUEST,getString(R.string.security_settings_fingerprint_preference_title),false, mUserId);
        }
    }

    private final Handler mHandler = new Handler() {
        @Override
        public void handleMessage(android.os.Message msg) {
            switch (msg.what) {
                case MSG_FINGER_AUTH_SUCCESS:
                    mFingerprintCancel = null;
                    verifySuccess(msg.arg1);
                    retryFingerprint();
                    break;
                case MSG_FINGER_AUTH_FAIL:
                    // No action required... fingerprint will allow up to 5 of these
                    break;
                case MSG_FINGER_AUTH_ERROR:
                    handleError(msg.arg1 /* errMsgId */, (CharSequence) msg.obj /* errStr */ );
                    break;
                case MSG_FINGER_AUTH_HELP: {
                    // Not used
                }
                break;
            }
        }
    };

    /**
     * @param errMsgId
     */
    protected void handleError(int errMsgId, CharSequence msg) {
        mFingerprintCancel = null;
        switch (errMsgId) {
            case FingerprintManager.FINGERPRINT_ERROR_CANCELED:
                return; // Only happens if we get preempted by another activity. Ignored.
            case FingerprintManager.FINGERPRINT_ERROR_LOCKOUT:
                mInFingerprintLockout = true;
                // We've been locked out.  Reset after 30s.
//                if (!mHandler.hasCallbacks(mFingerprintLockoutReset)) {
//                    mHandler.postDelayed(mFingerprintLockoutReset,
//                            LOCKOUT_DURATION);
//                }
                // Fall through to show message
            default:
                // Activity can be null on a screen rotation.
//                final Activity activity = getActivity();
//                if (activity != null) {
//                    Toast.makeText(activity, msg, Toast.LENGTH_SHORT);
//                }
                break;
        }
        retryFingerprint(); // start again
    }

    private void retryFingerprint() {
        if (!mInFingerprintLockout) {
            mFingerprintCancel = new CancellationSignal();
            mFingerprintManager.authenticate(null, mFingerprintCancel, 0 /* flags */,
                    mAuthCallback, null, mUserId);
        }
    }

    private void verifySuccess(int fingerId) {
        Intent intent = new Intent();
        intent.putExtra("finger_id",fingerId);
        setResult(RESULT_OK, intent);
        finish();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == CONFIRM_REQUEST && resultCode == RESULT_OK) {
            Intent intent = new Intent();
            intent.putExtra("finger_id",0);
            setResult(RESULT_OK, intent);
            finish();
        } else {
            finish();
        }
    }

    private String getKeyForCurrent() {

        if (mLockPatternUtils.isLockScreenDisabled(mUserId)) {
            return KEY_UNLOCK_SET_OFF;
        }
        switch (mLockPatternUtils.getKeyguardStoredPasswordQuality(mUserId)) {
            case DevicePolicyManager.PASSWORD_QUALITY_SOMETHING:
                return KEY_UNLOCK_SET_PATTERN;
            case DevicePolicyManager.PASSWORD_QUALITY_NUMERIC:
            case DevicePolicyManager.PASSWORD_QUALITY_NUMERIC_COMPLEX:
                return KEY_UNLOCK_SET_PIN;
            case DevicePolicyManager.PASSWORD_QUALITY_ALPHABETIC:
            case DevicePolicyManager.PASSWORD_QUALITY_ALPHANUMERIC:
            case DevicePolicyManager.PASSWORD_QUALITY_COMPLEX:
                return KEY_UNLOCK_SET_PASSWORD;
            case DevicePolicyManager.PASSWORD_QUALITY_MANAGED:
                return KEY_UNLOCK_SET_MANAGED;
            case DevicePolicyManager.PASSWORD_QUALITY_UNSPECIFIED:
                return KEY_UNLOCK_SET_NONE;
        }
        return null;
    }

    @Override
    public void onResume() {
        super.onResume();
        String type = getKeyForCurrent();
        if(!mFingerprintManager.hasEnrolledFingerprints()) {
            if(KEY_UNLOCK_SET_NONE.equals(type) || KEY_UNLOCK_SET_OFF.equals(type)) {
                Intent intent = new Intent();
                intent.putExtra("finger_id",0);
                setResult(RESULT_OK, intent);
                finish();
            } else {
                ChooseLockSettingsHelper helper = new ChooseLockSettingsHelper(this);
                helper.launchConfirmationActivity(CONFIRM_REQUEST, getString(R.string.security_settings_fingerprint_preference_title), false, mUserId);
            }
        } else {
            retryFingerprint();
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        if (mFingerprintCancel != null) {//stop listening
            mFingerprintCancel.cancel();
            mFingerprintCancel = null;
        }

    }

}
