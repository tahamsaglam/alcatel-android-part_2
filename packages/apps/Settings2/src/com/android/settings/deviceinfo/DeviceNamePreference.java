/*
 * Copyright (C) 2016 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file
 * except in compliance with the License. You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the
 * License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the specific language governing
 * permissions and limitations under the License.
 */

package com.android.settings.deviceinfo;

import mst.preference.EditTextPreference;
import mst.preference.PreferenceManager;
import android.content.Context;
import android.content.res.Resources;
import android.text.InputType;
import android.util.AttributeSet;
import android.util.Slog;
import android.view.View;
import android.widget.EditText;

import com.android.settings.CustomEditTextPreference;
import com.android.settings.R;

import android.os.Build;
import android.os.SystemProperties;


public class DeviceNamePreference extends EditTextPreference {
    private static final String TAG = "DeviceNamePreference";
    private String mName;
    private EditText mEditText;
    public DeviceNamePreference(Context context, AttributeSet attrs) {
        super(context, attrs);
        setLayoutResource(R.layout.device_name_preference);
    }

    @Override
    public void onAttachedToHierarchy(PreferenceManager preferenceManager) {
        super.onAttachedToHierarchy(preferenceManager);
        if (mName == null) {
            mName = getCurrentName();
        }
        setSummary(mName);
    }

    private String getCurrentName() {
        //get CurrentName
        mName = SystemProperties.get("persist.sys.devicename", Build.DEVICE);
        return mName;
    }

    @Override
    protected void onBindDialogView(View view) {
        super.onBindDialogView(view);
        //setSummary(mName);
        final EditText editText = (EditText) view.findViewById(com.android.internal.R.id.edit);
        editText.setText(mName);
        editText.setSelection(editText.getText().length());
        mEditText = editText;
    }
    

    @Override
    protected void onDialogClosed(boolean positiveResult) {
        if (positiveResult) {
            mName = mEditText.getText().toString();
            setSummary(mName);
            SystemProperties.set("persist.sys.devicename", mName);
        }
    }
}
