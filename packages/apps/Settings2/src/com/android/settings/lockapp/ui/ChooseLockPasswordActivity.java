package com.android.settings.lockapp.ui;

import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

import com.android.settings.lockapp.activity.ApplicationLockActivity;
import com.android.settings.lockapp.utils.SharedPreferenceUtil;
import com.android.settings.lockapp.utils.StringUtils;
import com.android.settings.R;

import android.app.Activity;
import mst.app.dialog.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import android.text.Editable;
import android.text.InputType;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.TextView.OnEditorActionListener;

public class ChooseLockPasswordActivity extends Activity implements
        OnEditorActionListener, TextWatcher {
    private final static String TAG = "SecurityCenter.ChooseLockPasswordActivity";
    private final static String TCT_APP_LOCK = "tct_app_lock";
    //private TextView headerText;
    private TextView mPasswordEntry;
    private Button goPattern;
    private Stage mUiStage = Stage.Introduction;
    private int mPasswordLength = 4;
    private String mCurrentPassword;
    private String mChosenPassword;
    private String mFirstPin;
    private Boolean isResetPW = false;
    private Boolean isFromSettings;
    private ImageButton mOkButton;
    private Button mDeleteButton;
    private ArrayList<Button> buttonArray = new ArrayList<Button>();
    private String mPasswordConfirmStr = "";
    private ArrayList<TextView> mPasswordEntryArray = new ArrayList<TextView>();

    protected enum Stage {

        Introduction,

        NeedToConfirm,

        ConfirmWrong;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
        getWindow().setStatusBarColor(Color.parseColor("#f8f8f8"));
        setContentView(R.layout.activity_choose_lock_password);
        getActionBar().setTitle(R.string.input_password);
        init();
        //headerText = (TextView) findViewById(R.id.headerText);
        goPattern = (Button) findViewById(R.id.pattern_lock);
        // mPasswordEntryInputDisabler = new
        // TextViewInputDisabler(mPasswordEntry);

        isResetPW = getIntent().getBooleanExtra("reset_password", false);
        if (isResetPW) {
            goPattern.setVisibility(View.INVISIBLE);
            getActionBar().setTitle(this.getResources().getString(R.string.confirm_password));
            mUiStage = Stage.NeedToConfirm;
        }

        isFromSettings = getIntent().getBooleanExtra("from_settings", false);

        goPattern.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isResetPW) {
                    // nothing to-do
                }
                else if (mUiStage == Stage.Introduction) {
                    Intent intent =
                            new Intent(ChooseLockPasswordActivity.this,
                            ChooseLockPatternActivity.class);
                    if (isFromSettings) {
                        intent.putExtra("from_settings", true);
                        intent.addFlags(Intent.FLAG_ACTIVITY_FORWARD_RESULT);
                    }
                    startActivity(intent);
                    ChooseLockPasswordActivity.this.finish();
                } else if (mUiStage == Stage.NeedToConfirm || mUiStage == Stage.ConfirmWrong) {
                    updateStage(Stage.Introduction);
                    clearEditView();
                    goPattern.setText(
                            ChooseLockPasswordActivity.this.getResources().
                            getString(R.string.pattern_password));
                    getActionBar().setTitle(
                            ChooseLockPasswordActivity.this.getResources().
                            getString(R.string.input_password));
                }
            }
        });
    }

    public void init() {
        mPasswordEntryArray.add((TextView)findViewById(R.id.password_entry1));
        mPasswordEntryArray.add((TextView)findViewById(R.id.password_entry2));
        mPasswordEntryArray.add((TextView)findViewById(R.id.password_entry3));
        mPasswordEntryArray.add((TextView)findViewById(R.id.password_entry4));
        mDeleteButton = (Button)findViewById(R.id.mDeleteButton);
        mOkButton = (ImageButton) findViewById(R.id.mOkButton);
        buttonArray.add((Button)findViewById(R.id.mButton0));
        buttonArray.add((Button)findViewById(R.id.mButton1));
        buttonArray.add((Button)findViewById(R.id.mButton2));
        buttonArray.add((Button)findViewById(R.id.mButton3));
        buttonArray.add((Button)findViewById(R.id.mButton4));
        buttonArray.add((Button)findViewById(R.id.mButton5));
        buttonArray.add((Button)findViewById(R.id.mButton6));
        buttonArray.add((Button)findViewById(R.id.mButton7));
        buttonArray.add((Button)findViewById(R.id.mButton8));
        buttonArray.add((Button)findViewById(R.id.mButton9));
        changeButton();
    }

    public void changeButton(){
        for(int i=0;i<10;i++){
            Button button = buttonArray.get(i);
            button.setText(String.valueOf(i));
//            button.getBackground().setAlpha(0); // MODIFIED by kai.wang1, 2016-07-22,BUG-2570987
            final int j = i;
            button.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (mPasswordConfirmStr.length() < 4) {
                        mPasswordConfirmStr = mPasswordConfirmStr+j;
                        updateEditView();
                    }
                }
            });
        }
        mDeleteButton.getBackground().setAlpha(0);
        mOkButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                deletePassword();
            }
        });
    }

    private void updateEditView() {
        if (mUiStage == Stage.ConfirmWrong) {
            mUiStage = Stage.NeedToConfirm;
        }
        if (mPasswordConfirmStr.length() > 0) {
            for (int n=0;n<mPasswordConfirmStr.length();n++) {
                TextView textView = mPasswordEntryArray.get(n);
                textView.setText("*");
            }
            if (mPasswordConfirmStr.length() == 4) {
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        // TODO Auto-generated method stub
                        handleNext();
                    }
                }, 300);
            }
        }
    }

    private void clearEditView(){
        for (int n=0;n<4;n++) {
            TextView textView = mPasswordEntryArray.get(n);
            mPasswordConfirmStr = "";
            textView.setText("");
        }
    }

    private void deletePassword() {
        int n = mPasswordConfirmStr.length();
        if (n > 0) {
            mPasswordEntryArray.get(n-1).setText("");
            mPasswordConfirmStr = mPasswordConfirmStr.substring(0, n-1);
        }
    }

    private void saveChosenPatternAndFinish() {
        SharedPreferenceUtil.editIsNumModel(this, true);
        Settings.System.putInt(getContentResolver(), "tct_app_lock_state", 2);
        String md5 = StringUtils.toMD5(mChosenPassword);
        SharedPreferenceUtil.editNumPassword(this, md5);

        if(isFromSettings) {
            if (SharedPreferenceUtil.readIsFirst(this)) {
                AlertDialog.Builder alert = new AlertDialog.Builder(
                        this);
                alert.setMessage(R.string.password_dialog_message)
                        .setPositiveButton(
                                R.string.password_dialog_set,
                                new DialogInterface.OnClickListener() {
                                    public void onClick(
                                            DialogInterface dialog,
                                            int which) {
                                        Intent intent = new Intent(ChooseLockPasswordActivity.this, ApplicationLockSaveGuardActivity.class);
                                        intent.putExtra("from_settings", true);
                                        intent.addFlags(Intent.FLAG_ACTIVITY_FORWARD_RESULT);
                                        startActivity(intent);
                                        Settings.System.putInt(getContentResolver(), TCT_APP_LOCK, 1);
                                        ChooseLockPasswordActivity.this.finish();
                                    }
                                })
                        .setNegativeButton(
                                R.string.password_dialog_setlater,
                                new DialogInterface.OnClickListener() {
                                    public void onClick(
                                            DialogInterface dialog,
                                            int which) {
                                        setResult(Activity.RESULT_OK);
                                        Settings.System.putInt(getContentResolver(), TCT_APP_LOCK, 1);
                                        SharedPreferenceUtil.editIsFirst(ChooseLockPasswordActivity.this, false);
                                        ChooseLockPasswordActivity.this.finish();
                                    }
                                });
                //add by NJTS zhe.xu for defect 1936414 2016-04-14 begin
                alert.setCancelable(false);
                //add by NJTS zhe.xu for defect 1936414 2016-04-14 end
                AlertDialog mDialog = alert.create();
                mDialog.show();
            } else {
                setResult(Activity.RESULT_OK);
                Settings.System.putInt(getContentResolver(), TCT_APP_LOCK, 1);
                SharedPreferenceUtil.editIsFirst(ChooseLockPasswordActivity.this, false);
                finish();
            }
        }
        // need show password protect dialog;
        else if (SharedPreferenceUtil.readIsFirst(this)) {
            AlertDialog.Builder alert = new AlertDialog.Builder(
                    this);
            alert.setMessage(R.string.password_dialog_message)
                    .setPositiveButton(
                            R.string.password_dialog_set,
                            new DialogInterface.OnClickListener() {
                                public void onClick(
                                        DialogInterface dialog,
                                        int which) {
                                    Intent intent = new Intent(ChooseLockPasswordActivity.this, ApplicationLockSaveGuardActivity.class);
                                    startActivity(intent);
                                    Settings.System.putInt(getContentResolver(), TCT_APP_LOCK, 1);
                                    ChooseLockPasswordActivity.this.finish();
                                }
                            })
                    .setNegativeButton(
                            R.string.password_dialog_setlater,
                            new DialogInterface.OnClickListener() {
                                public void onClick(
                                        DialogInterface dialog,
                                        int which) {
                                    Intent intent = new Intent(ChooseLockPasswordActivity.this, ApplicationLockActivity.class);
                                    startActivity(intent);
                                    SharedPreferenceUtil.editIsFirst(ChooseLockPasswordActivity.this, false);
                                    Settings.System.putInt(getContentResolver(), TCT_APP_LOCK, 1);
                                    ChooseLockPasswordActivity.this.finish();
                                }
                            });
            AlertDialog mDialog = alert.create();
            mDialog.show();
        } else {
            finish();
        }
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count,
            int after) {
        Log.d(TAG, "beforeTextChanged");
    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {
        // TODO Auto-generated method stub
        Log.d(TAG, "onTextChanged");
    }

    @Override
    public void afterTextChanged(Editable s) {
        Log.d(TAG, "afterTextChanged");
        if (mUiStage == Stage.ConfirmWrong) {
            mUiStage = Stage.NeedToConfirm;
        }
        updateUi();
    }

    @Override
    public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
        // Check if this was the result of hitting the enter or "done" key
        if (actionId == EditorInfo.IME_NULL
                || actionId == EditorInfo.IME_ACTION_DONE
                || actionId == EditorInfo.IME_ACTION_NEXT) {
            handleNext();
            return true;
        }
        return false;
    }

    protected void updateStage(Stage stage) {
        final Stage previousStage = mUiStage;
        mUiStage = stage;
        updateUi();
    }

    private void updateUi() {
        final int length = mPasswordConfirmStr.length();
        if (length == mPasswordLength) {
            handleNext();
        }
    }

    public void handleNext() {
        mChosenPassword = mPasswordConfirmStr;
        if (TextUtils.isEmpty(mChosenPassword)) {
            return;
        }
        if (mUiStage == Stage.Introduction) {
            clearEditView();
            mFirstPin = mChosenPassword;
            getActionBar().setTitle(this.getResources().getString(R.string.confirm_password));
            goPattern.setText(this.getResources().getString(R.string.reset_password));
            updateStage(Stage.NeedToConfirm);
        } else if (mUiStage == Stage.NeedToConfirm) {
            if (isResetPW) {
                String md5 = StringUtils.toMD5(mChosenPassword);
                if (md5 !=null && md5.equals(SharedPreferenceUtil.readNumPassword(this))) {
                    clearEditView();
                    mUiStage = Stage.Introduction;
                    getActionBar().setTitle(this.getResources().getString(R.string.new_password));
                    isResetPW = false;
                    goPattern.setVisibility(View.VISIBLE);
                } else {
                    clearEditView();
                    updateStage(Stage.ConfirmWrong);
                    //getActionBar().setTitle(this.getResources().getString(R.string.wrong_password));
                    Toast.makeText(this, this.getResources().getString(R.string.wrong_password), Toast.LENGTH_SHORT).show();
                }
            }
            else {
                if (mFirstPin.equals(mChosenPassword)) {
                    saveChosenPatternAndFinish();
                } else {
                    clearEditView();
                    updateStage(Stage.ConfirmWrong);
                    getActionBar().setTitle(this.getResources().getString(R.string.password_not_compared));
                }
            }
        }
    }
}
