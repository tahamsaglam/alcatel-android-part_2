package com.android.settings.lockapp.utils;

import android.content.Context;
import android.os.Build;

public class SharedPreferenceUtil {
    public static String DEFAULT_SETTING_PREFERENCE = "applicationlock";

    public static void editIsNumModel(Context paramContext, boolean paramBoolean) {
        paramContext
                .getSharedPreferences(
                        DEFAULT_SETTING_PREFERENCE,
                        Build.VERSION.SDK_INT > Build.VERSION_CODES.FROYO ? 4
                                : 0).edit()
                .putBoolean("IsNumModel", paramBoolean).commit();
    }

    public static boolean readIsNumModel(Context paramContext) {
        return paramContext.getSharedPreferences(DEFAULT_SETTING_PREFERENCE,
                Build.VERSION.SDK_INT > Build.VERSION_CODES.FROYO ? 4 : 0)
                .getBoolean("IsNumModel", false);
    }

    public static String readNumPassword(Context paramContext) {
        return paramContext.getSharedPreferences(DEFAULT_SETTING_PREFERENCE,
                Build.VERSION.SDK_INT > Build.VERSION_CODES.FROYO ? 4 : 0)
                .getString("NumPassword", "");
    }

    public static void editNumPassword(Context paramContext, String s) {
        paramContext
                .getSharedPreferences(
                        DEFAULT_SETTING_PREFERENCE,
                        Build.VERSION.SDK_INT > Build.VERSION_CODES.FROYO ? 4
                                : 0).edit().putString("NumPassword", s)
                .commit();
    }

    public static boolean readIsFirst(Context paramContext) {
        return paramContext.getSharedPreferences(DEFAULT_SETTING_PREFERENCE, Build.VERSION.SDK_INT > Build.VERSION_CODES.FROYO ? 4 : 0).getBoolean("IsFirst", true);
    }

    public static void editIsFirst(Context paramContext, boolean flag) {
        paramContext
                .getSharedPreferences(DEFAULT_SETTING_PREFERENCE, Build.VERSION.SDK_INT > Build.VERSION_CODES.FROYO ? 4 : 0).edit().putBoolean("IsFirst", flag)
                .commit();
    }

    public static int readQuestion(Context paramContext) {
        return paramContext.getSharedPreferences(DEFAULT_SETTING_PREFERENCE,
                Build.VERSION.SDK_INT > Build.VERSION_CODES.FROYO ? 4 : 0)
                .getInt("question", 0);
    }

    public static void editQuestion(Context paramContext, int flag) {
        paramContext
                .getSharedPreferences(
                        DEFAULT_SETTING_PREFERENCE,
                        Build.VERSION.SDK_INT > Build.VERSION_CODES.FROYO ? 4
                                : 0).edit().putInt("question", flag).commit();
    }
    
    public static String readQuestionAnswer(Context paramContext) {
        return paramContext.getSharedPreferences(DEFAULT_SETTING_PREFERENCE,
                Build.VERSION.SDK_INT > Build.VERSION_CODES.FROYO ? 4 : 0)
                .getString("questionanswer", "");
    }

    public static void editQuestionAnswer(Context paramContext, String s) {
        paramContext
                .getSharedPreferences(
                        DEFAULT_SETTING_PREFERENCE,
                        Build.VERSION.SDK_INT > Build.VERSION_CODES.FROYO ? 4
                                : 0).edit().putString("questionanswer", s)
                .commit();
    }
    
    public static boolean readIsLockScreenShowed(Context paramContext) {
        return paramContext.getSharedPreferences(DEFAULT_SETTING_PREFERENCE,
                Build.VERSION.SDK_INT > Build.VERSION_CODES.FROYO ? 4 : 0)
                .getBoolean("lockscreenshowed", false);
    }

    public static void editIsLockScreenShowed(Context paramContext, boolean s) {
        paramContext
                .getSharedPreferences(
                        DEFAULT_SETTING_PREFERENCE,
                        Build.VERSION.SDK_INT > Build.VERSION_CODES.FROYO ? 4
                                : 0).edit().putBoolean("lockscreenshowed", s)
                .commit();
    }

    public static boolean readFingerPrintDialogShowed(Context paramContext) {
        return paramContext.getSharedPreferences(DEFAULT_SETTING_PREFERENCE,
                Build.VERSION.SDK_INT > Build.VERSION_CODES.FROYO ? 4 : 0)
                .getBoolean("fingerprintdialog", true);
    }

    public static void editFingerPrintDialogShowed(Context paramContext, boolean s) {
        paramContext
                .getSharedPreferences(
                        DEFAULT_SETTING_PREFERENCE,
                        Build.VERSION.SDK_INT > Build.VERSION_CODES.FROYO ? 4
                                : 0).edit().putBoolean("fingerprintdialog", s)
                .commit();
    }

    public static boolean readShouldReturn(Context paramContext) {
        return paramContext.getSharedPreferences(DEFAULT_SETTING_PREFERENCE,
                Build.VERSION.SDK_INT > Build.VERSION_CODES.FROYO ? 4 : 0)
                .getBoolean("shouldreturn", false);
    }

    public static void editShouldReturn(Context paramContext, boolean s) {
        paramContext
                .getSharedPreferences(
                        DEFAULT_SETTING_PREFERENCE,
                        Build.VERSION.SDK_INT > Build.VERSION_CODES.FROYO ? 4
                                : 0).edit().putBoolean("shouldreturn", s)
                .commit();
    }

    public static void editFingerPrintError(Context paramContext, boolean paramBoolean) {
        paramContext
        .getSharedPreferences(
                DEFAULT_SETTING_PREFERENCE,
                Build.VERSION.SDK_INT > Build.VERSION_CODES.FROYO ? 4
                        : 0).edit().putBoolean("fingerprinterror", paramBoolean)
        .commit();
    }

    public static boolean readFingerPrintError(Context paramContext) {
        return paramContext.getSharedPreferences(DEFAULT_SETTING_PREFERENCE,
                Build.VERSION.SDK_INT > Build.VERSION_CODES.FROYO ? 4 : 0)
                .getBoolean("fingerprinterror", false);
    }
}
