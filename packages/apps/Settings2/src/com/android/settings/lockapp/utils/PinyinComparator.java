package com.android.settings.lockapp.utils;

import java.util.Comparator;
import com.android.settings.lockapp.ui.AppInfoParams;

public class PinyinComparator implements Comparator<AppInfoParams> {

	public int compare(AppInfoParams o1, AppInfoParams o2) {// * > # > A-Z > .
		if (o1.letter.equals("*")
				&& o2.letter.equals("#")) {
			return -1;
		} else if (o1.letter.equals("#")
				&& o2.letter.equals("*")) {
			return 1;
		} else if (o1.letter.equals("*")
				|| o2.letter.equals(".")) {
			return -1;
		} else if (o1.letter.equals(".")
				|| o2.letter.equals("*")) {
			return 1;
		} else if (o1.letter.equals("#")
				|| o2.letter.equals(".")) {
			return -1;
		} else if (o1.letter.equals(".")
				|| o2.letter.equals("#")) {
			return 1;
		} else {
			return o1.letter.compareTo(o2.letter);
		}
	}

}
