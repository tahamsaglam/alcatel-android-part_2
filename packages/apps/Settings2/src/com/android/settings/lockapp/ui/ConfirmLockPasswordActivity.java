package com.android.settings.lockapp.ui;


import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.hardware.fingerprint.FingerprintManager;
import android.hardware.fingerprint.FingerprintManager.AuthenticationCallback;
import android.hardware.fingerprint.FingerprintManager.AuthenticationResult;
import android.os.Bundle;
import android.os.CancellationSignal;
import android.os.Handler;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.TextView.OnEditorActionListener;

import com.android.settings.lockapp.activity.ApplicationLockActivity;
import com.android.settings.lockapp.utils.ApplicationLockUtils;
import com.android.settings.lockapp.utils.SharedPreferenceUtil;
import com.android.settings.lockapp.utils.StringUtils;
import com.android.settings.R;

public class ConfirmLockPasswordActivity extends Activity implements  TextWatcher {
    private final static String TAG = "ConfirmLockPasswordActivity";
    private TextView mPasswordEntry;
    private FingerprintManager mFpm;
    private CancellationSignal mFingerprintCancelSignal;
    private ImageView mFingerprintView;
    private Button mForgotPassword;
    private WindowManager windowManager;
    private Window window;
    private WindowManager.LayoutParams layoutParams;
    private Boolean isFromSettings;
    private ImageButton mOkButton;
    private Button mDeleteButton;
    private ArrayList<Button> buttonArray = new ArrayList<Button>();
    private String mPasswordConfirmStr = "";
    private ArrayList<TextView> mPasswordEntryArray = new ArrayList<TextView>();
    private static Toast toast;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);
        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
        getWindow().setStatusBarColor(Color.parseColor("#f8f8f8"));
        setContentView(R.layout.activity_confirm_lock_password);
        getActionBar().setTitle(R.string.app_lock_pw);
        init();
        mPasswordEntry = (TextView) findViewById(R.id.password_entry);
        mFingerprintView = (ImageView) findViewById(R.id.lock_icon);
        mForgotPassword = (Button) findViewById(R.id.forgot);
        if (!ApplicationLockUtils.shouldForgotPasswordShowed(ConfirmLockPasswordActivity.this)) {
            mForgotPassword.setVisibility(View.INVISIBLE);
        }
        isFromSettings = getIntent().getBooleanExtra("from_settings", false);
        if (isFromSettings) {
               mForgotPassword.setVisibility(View.INVISIBLE);
        }

        mForgotPassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ConfirmLockPasswordActivity.this,
                        ApplicationLockSaveGuardActivity.class);
                intent.putExtra("from_app_forgot", true);
                startActivity(intent);
                finish();
            }
        });
        mFpm = (FingerprintManager) this.getSystemService(Context.FINGERPRINT_SERVICE);
        //authenticateFingerPrint();
    }

    public void init() {
        mPasswordEntryArray.add((TextView)findViewById(R.id.password_entry1));
        mPasswordEntryArray.add((TextView)findViewById(R.id.password_entry2));
        mPasswordEntryArray.add((TextView)findViewById(R.id.password_entry3));
        mPasswordEntryArray.add((TextView)findViewById(R.id.password_entry4));
        mDeleteButton = (Button)findViewById(R.id.mDeleteButton);
        mOkButton = (ImageButton) findViewById(R.id.mOkButton);
        buttonArray.add((Button)findViewById(R.id.mButton0));
        buttonArray.add((Button)findViewById(R.id.mButton1));
        buttonArray.add((Button)findViewById(R.id.mButton2));
        buttonArray.add((Button)findViewById(R.id.mButton3));
        buttonArray.add((Button)findViewById(R.id.mButton4));
        buttonArray.add((Button)findViewById(R.id.mButton5));
        buttonArray.add((Button)findViewById(R.id.mButton6));
        buttonArray.add((Button)findViewById(R.id.mButton7));
        buttonArray.add((Button)findViewById(R.id.mButton8));
        buttonArray.add((Button)findViewById(R.id.mButton9));
        changeButton();
    }

    public void changeButton(){
        for(int i=0;i<10;i++){
            Button button = buttonArray.get(i);
            button.setText(String.valueOf(i));
            final int j = i;
            button.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (mPasswordConfirmStr.length() < 4) {
                        mPasswordConfirmStr = mPasswordConfirmStr+j;
                        updateEditView();
                    }
                }
            });
        }
        mDeleteButton.getBackground().setAlpha(0);
//        mOkButton.getBackground().setAlpha(0); // MODIFIED by kai.wang1, 2016-07-22,BUG-2570987
        mOkButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                deletePassword();
            }
        });
    }

    private void updateEditView() {
        if (mPasswordConfirmStr.length() > 0) {
            for (int n=0;n<mPasswordConfirmStr.length();n++) {
                TextView textView = mPasswordEntryArray.get(n);
                textView.setText("*");
            }
            if (mPasswordConfirmStr.length() == 4) {
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        // TODO Auto-generated method stub
                        handleNext();
                    }
                }, 300);
            }
        }
    }

    private void clearEditView(){
        for (int n=0;n<4;n++) {
            TextView textView = mPasswordEntryArray.get(n);
            mPasswordConfirmStr = "";
            textView.setText("");
        }
    }

    private void deletePassword() {
        int n = mPasswordConfirmStr.length();
        if (n > 0) {
            mPasswordEntryArray.get(n-1).setText("");
            mPasswordConfirmStr = mPasswordConfirmStr.substring(0, n-1);
        }
    }
    private FingerprintManager.AuthenticationCallback mAuthenticationCallback = new AuthenticationCallback() {
        @Override
        public void onAuthenticationFailed() {
            Log.d(TAG, "onAuthenticationFailed");
            showToast(ConfirmLockPasswordActivity.this.getResources().
                    getString(R.string.fingerprint_failed));
        };

        @Override
        public void onAuthenticationSucceeded(AuthenticationResult result) {
            Log.d(TAG, "onAuthenticationSucceeded");
            mFingerprintCancelSignal = null;
            int fpid = result.getFingerprint().getFingerId();
            int tag = mFpm.tctGetTagForFingerprintId(fpid);

            if (tag == FingerprintManager.FP_TAG_COMMON) {
            	Intent intent = new Intent(ConfirmLockPasswordActivity.this,
                    ApplicationLockActivity.class);
                startActivity(intent);
                finish();
            } else {
            	Log.d(TAG, "onAuthenticationFailed");
                showToast(ConfirmLockPasswordActivity.this.getResources().
                    getString(R.string.fingerprint_failed));
                authenticateFingerPrint();
            }
        }

        @Override
        public void onAuthenticationHelp(int helpMsgId, CharSequence helpString) {
            Log.d(TAG, "onAuthenticationHelp");
        }

        @Override
        public void onAuthenticationError(int errMsgId, CharSequence errString) {
            Log.d(TAG, "onAuthenticationError");
            mFingerprintCancelSignal = null;
            if (errMsgId == FingerprintManager.FINGERPRINT_ERROR_LOCKOUT) {
                showToast(ConfirmLockPasswordActivity.this.getResources().
                    getString(R.string.fingerprint_error));
                mFingerprintView.setVisibility(View.INVISIBLE);
                SharedPreferenceUtil.editFingerPrintError(ConfirmLockPasswordActivity.this,true);
            }
        }

        @Override
        public void onAuthenticationAcquired(int acquireInfo) {
            Log.d(TAG, "onAuthenticationAcquired");
        }
    };

    @Override
    protected void onResume() {
        super.onResume();
        authenticateFingerPrint();
    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.d(TAG, "onPause");
        if (mFingerprintCancelSignal != null && !mFingerprintCancelSignal.isCanceled()) {
            mFingerprintCancelSignal.cancel();
        }
        mFingerprintCancelSignal = null;

        finish();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count,
            int after) {
        // TODO Auto-generated method stub

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {
        // TODO Auto-generated method stub

    }

    @Override
    public void afterTextChanged(Editable s) {
        // TODO Auto-generated method stub

    }

    public void handleNext() {
        final String pin = mPasswordConfirmStr;
        String md5 = StringUtils.toMD5(pin);
        if (md5 !=null && md5.equals(SharedPreferenceUtil.readNumPassword(this))) {
            if(isFromSettings) {
                setResult(Activity.RESULT_OK);
                finish();
            } else {
                Log.d(TAG,"confirm OK to ApplicationLockActivity");
                Intent intent = new Intent(ConfirmLockPasswordActivity.this,
                        ApplicationLockActivity.class);
                startActivity(intent);
                finish();
            }
            SharedPreferenceUtil.editFingerPrintError(ConfirmLockPasswordActivity.this,false);
        } else {
            clearEditView();
            Toast.makeText(this,ConfirmLockPasswordActivity.this.getResources().
                    getString(R.string.wrong_password_input_again),
                    Toast.LENGTH_SHORT).show();
        }
    }

    private void authenticateFingerPrint() {
        if (ApplicationLockUtils.isUnlockWithFingerprintPossible(0, mFpm, this)
                && ApplicationLockUtils.isFingerPrint4ApplicationLockOn(this)
                && !SharedPreferenceUtil.readFingerPrintError(this)) {
            if (mFingerprintCancelSignal != null  && !mFingerprintCancelSignal.isCanceled()) {
                mFingerprintCancelSignal.cancel();
            }
            mFingerprintView.setVisibility(View.VISIBLE);
            mFingerprintCancelSignal = new CancellationSignal();
            mFpm.authenticate(null, mFingerprintCancelSignal, 0, mAuthenticationCallback, null, 0);
            //setFingerprintRunningState(FINGERPRINT_STATE_RUNNING);
        } else {
            mFingerprintView.setVisibility(View.INVISIBLE);
        }

    }

    private void showToast(String text) {
            if (null != toast){
                toast.setText(text);
                toast.setDuration(Toast.LENGTH_SHORT);
                toast.show();
	    } else {
                toast = Toast.makeText(ConfirmLockPasswordActivity.this, text, Toast.LENGTH_SHORT);
                toast.show();
            }
    }
}
