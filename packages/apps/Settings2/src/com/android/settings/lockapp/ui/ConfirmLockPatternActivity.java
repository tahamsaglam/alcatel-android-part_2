package com.android.settings.lockapp.ui;

import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.hardware.fingerprint.FingerprintManager;
import android.hardware.fingerprint.FingerprintManager.AuthenticationCallback;
import android.hardware.fingerprint.FingerprintManager.AuthenticationResult;
import android.os.Bundle;
import android.os.CancellationSignal;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Button;
import android.widget.Toast;

import com.android.settings.R;
import com.android.settings.lockapp.activity.ApplicationLockActivity;
import com.android.settings.lockapp.ui.ChooseLockPatternActivity.Stage;
import com.android.settings.lockapp.ui.LockPatternView.Cell;
import com.android.settings.lockapp.ui.LockPatternView.DisplayMode;
import com.android.settings.lockapp.utils.ApplicationLockUtils;
import com.android.settings.lockapp.utils.SharedPreferenceUtil;

public class ConfirmLockPatternActivity extends Activity {
    private final static String TAG = "ConfirmLockPatternActivity";
    protected LockPatternView mLockPatternView;
    private FingerprintManager mFpm;
    private CancellationSignal mFingerprintCancelSignal;
    private ImageView mFingerprintView;
    private Button mForgotPassword;
    private WindowManager windowManager;
    private Window window;
    private WindowManager.LayoutParams layoutParams;
    private Boolean isFromSettings;
    private static Toast toast;

    private enum Stage {
        NeedToUnlock,
        NeedToUnlockWrong,
        LockedOut
    }

    private Runnable mClearPatternRunnable = new Runnable() {
        public void run() {
            mLockPatternView.clearPattern();
        }
    };

    protected LockPatternView.OnPatternListener mChooseNewLockPatternListener =
            new LockPatternView.OnPatternListener() {

            public void onPatternStart() {
                mLockPatternView.removeCallbacks(mClearPatternRunnable);
                patternInProgress();
            }

            public void onPatternCleared() {
                mLockPatternView.removeCallbacks(mClearPatternRunnable);
            }

            public void onPatternDetected(List<LockPatternView.Cell> pattern) {
                mLockPatternView.setEnabled(false);
                startCheckPattern(pattern);
                return;
            }

            private void patternInProgress() {

            }

            @Override
            public void onPatternCellAdded(List<Cell> pattern) {

            }
     };
    

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
        getWindow().setStatusBarColor(Color.parseColor("#f8f8f8"));
        setContentView(R.layout.activity_confirm_lock_pattern);
        getActionBar().setTitle(R.string.app_lock_pw);
        mLockPatternView = (LockPatternView) findViewById(R.id.lockPattern);
        mFingerprintView = (ImageView) findViewById(R.id.lock_icon);
        mForgotPassword = (Button) findViewById(R.id.forgot);
        if (!ApplicationLockUtils.shouldForgotPasswordShowed(ConfirmLockPatternActivity.this)) {
            mForgotPassword.setVisibility(View.INVISIBLE);
        }
        isFromSettings = getIntent().getBooleanExtra("from_settings", false);
        if (isFromSettings) {
               mForgotPassword.setVisibility(View.INVISIBLE);
        }
        mFpm = (FingerprintManager) this.getSystemService(Context.FINGERPRINT_SERVICE);
        //authenticateFingerPrint();
        mLockPatternView.setOnPatternListener(mChooseNewLockPatternListener);
        mLockPatternView.setTactileFeedbackEnabled(true);
        updateStage(Stage.NeedToUnlock);
        mForgotPassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ConfirmLockPatternActivity.this,
                        ApplicationLockSaveGuardActivity.class);
                intent.putExtra("from_app_forgot", true);
                startActivity(intent);
                finish();
            }
        });
    }

    private FingerprintManager.AuthenticationCallback mAuthenticationCallback = new AuthenticationCallback() {
        @Override
        public void onAuthenticationFailed() {
            Log.d(TAG, "onAuthenticationFailed");
            showToast(ConfirmLockPatternActivity.this.getResources().
                    getString(R.string.fingerprint_failed));
        };

        @Override
        public void onAuthenticationSucceeded(AuthenticationResult result) {
            Log.d(TAG, "onAuthenticationSucceeded");
            mFingerprintCancelSignal = null;
            int fpid = result.getFingerprint().getFingerId();
            int tag = mFpm.tctGetTagForFingerprintId(fpid);

            if (tag == FingerprintManager.FP_TAG_COMMON) {
            	Intent intent = new Intent(ConfirmLockPatternActivity.this,
                        ApplicationLockActivity.class);
                startActivity(intent);
                finish();
            } else {
            	Log.d(TAG, "onAuthenticationFailed");
                showToast(ConfirmLockPatternActivity.this.getResources().
                    getString(R.string.fingerprint_failed));
                authenticateFingerPrint();
            }
        }

        @Override
        public void onAuthenticationHelp(int helpMsgId, CharSequence helpString) {
            Log.d(TAG, "onAuthenticationHelp");
        }

        @Override
        public void onAuthenticationError(int errMsgId, CharSequence errString) {
            Log.d(TAG, "onAuthenticationError");
            mFingerprintCancelSignal = null;
            if (errMsgId == FingerprintManager.FINGERPRINT_ERROR_LOCKOUT) {
                showToast(ConfirmLockPatternActivity.this.getResources().
                    getString(R.string.fingerprint_error));
                mFingerprintView.setVisibility(View.INVISIBLE);
                SharedPreferenceUtil.editFingerPrintError(ConfirmLockPatternActivity.this,true);
            }
        }

        @Override
        public void onAuthenticationAcquired(int acquireInfo) {
            Log.d(TAG, "onAuthenticationAcquired");
        }
    };

    @Override
    protected void onResume() {
        super.onResume();
        authenticateFingerPrint();
    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.d(TAG, "onPause");
        if (mFingerprintCancelSignal != null && !mFingerprintCancelSignal.isCanceled()) {
            mFingerprintCancelSignal.cancel();
        }
        mFingerprintCancelSignal = null;

        finish();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }
    
    private void updateStage(Stage stage) {
        switch (stage) {
            case NeedToUnlock:
                mLockPatternView.setEnabled(true);
                mLockPatternView.enableInput();
                mLockPatternView.clearPattern();
                break;
            case NeedToUnlockWrong:
                Toast.makeText(this,
                        this.getResources().getString(R.string.wrong_password_input_again),
                        Toast.LENGTH_SHORT).show();
                mLockPatternView.setDisplayMode(LockPatternView.DisplayMode.Wrong);
                mLockPatternView.setEnabled(true);
                mLockPatternView.enableInput();
                break;
            case LockedOut:
                //mLockPatternView.clearPattern();
                // enabled = false means: disable input, and have the
                // appearance of being disabled.
                mLockPatternView.setEnabled(false); // appearance of being disabled
                break;
        }
    }

    private void startCheckPattern(final List<LockPatternView.Cell> pattern) {
        String patternStr = LockPatternUtilsSelf.patternToString(pattern);
        if (patternStr != null && patternStr.equals(LockPatternUtilsSelf.loadFromPreferences(this))) {
            updateStage(Stage.LockedOut);
            if(isFromSettings) {
                setResult(Activity.RESULT_OK);
                finish();
            } else {
                Intent intent = new Intent(ConfirmLockPatternActivity.this,
                        ApplicationLockActivity.class);
                startActivity(intent);
                finish();
            }
            SharedPreferenceUtil.editFingerPrintError(ConfirmLockPatternActivity.this,false);
        } else {
            updateStage(Stage.NeedToUnlockWrong);
        }
    }

    private void authenticateFingerPrint() {
        if (ApplicationLockUtils.isUnlockWithFingerprintPossible(0, mFpm, this)
                && ApplicationLockUtils.isFingerPrint4ApplicationLockOn(this)
                && !SharedPreferenceUtil.readFingerPrintError(this)) {
            if (mFingerprintCancelSignal != null  && !mFingerprintCancelSignal.isCanceled()) {
                mFingerprintCancelSignal.cancel();
            }
            mFingerprintView.setVisibility(View.VISIBLE);
            mFingerprintCancelSignal = new CancellationSignal();
            mFpm.authenticate(null, mFingerprintCancelSignal, 0, mAuthenticationCallback, null, 0);
            //setFingerprintRunningState(FINGERPRINT_STATE_RUNNING);
        } else {
            mFingerprintView.setVisibility(View.INVISIBLE);
        }
    }

    private void showToast(String text) {
            if (null != toast){
                toast.setText(text);
                toast.setDuration(Toast.LENGTH_SHORT);
                toast.show();
	    } else {
                toast = Toast.makeText(ConfirmLockPatternActivity.this, text, Toast.LENGTH_SHORT);
                toast.show();
            }
    }

}
