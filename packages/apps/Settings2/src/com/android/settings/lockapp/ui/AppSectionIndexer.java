package com.android.settings.lockapp.ui;

import java.util.Arrays;

import android.widget.SectionIndexer;

public class AppSectionIndexer implements SectionIndexer {
    private String[] mSections;//字母集合
    private int[] mPositions;//位置集合
    private int mCount;

    public AppSectionIndexer(String[] sections, int[] counts) {
        if (sections == null || counts == null) {
            throw new NullPointerException();
        }
        if (sections.length != counts.length) {
            throw new IllegalArgumentException("The sections and counts arrays must have the same length");
        }
        this.mSections = sections;
        mPositions = new int[counts.length];
        int position = 0;
        for (int i = 0; i < counts.length; i++) {
            mPositions[i] = position;
            if (counts[i] != 0) {
                position += (counts[i]+1);
            } else {
                position += counts[i];
            }
        }
        mCount = position;
        for (int i = 0;i<mPositions.length;i++){
            android.util.Log.d("AppSectionIndexer","i: "+i+" mPositions[i]: "+mPositions[i]);
        }
    }

    @Override
    public String[] getSections() {
        return mSections;
    }

    // 根据section的索引，获取该分组中的第一个item在listview中的位置；比如A对应的首个item在list中的位置
    @Override
    public int getPositionForSection(int sectionIndex) {
        if (sectionIndex < 0 || sectionIndex >= mSections.length) {
            return -1;
        }
        return mPositions[sectionIndex];
    }

    // 根据item在listview中的位置，获取该item所在的分组的索引;比如首字母A的都为0
    @Override
    public int getSectionForPosition(int position) {
        if (position < 0 || position >= mCount) {
            return -1;
        }
        int index = Arrays.binarySearch(mPositions, position);
        return index >= 0 ? index : -index - 2;
    }
}
