/*
 * Copyright (C) 2008 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/******************************************************************************/
/*                                                               Date:10/2016 */
/*                                PRESENTATION                                */
/*                                                                            */
/*       Copyright 2016 TCL Communication Technology Holdings Limited.        */
/*                                                                            */
/* This material is company confidential, cannot be reproduced in any form    */
/* without the written permission of TCL Communication Technology Holdings    */
/* Limited.                                                                   */
/*                                                                            */
/* -------------------------------------------------------------------------- */
/*  Author :  dongdong.gong                                                   */
/*  Email  :  dongdong.gong@tcl.com                                           */
/*  Role   :  engineer                                                        */
/*  Reference documents :                                                     */
/* -------------------------------------------------------------------------- */
/*  Comments :                                                                */
/*  File     :                                                                */
/*  Labels   :                                                                */
/* -------------------------------------------------------------------------- */
/* ========================================================================== */
/*     Modifications on Features list / Changes Request / Problems Report     */
/* -------------------------------------------------------------------------- */
/*    date   |        author        |         Key          |     comment      */
/* ----------|----------------------|----------------------|----------------- */
/* 10/10/2016|    dongdong.gong     |       3055667        |For Dual Bluetoo- */
/*           |                      |                      |th Dev            */
/* ----------|----------------------|----------------------|----------------- */
/******************************************************************************/


package com.android.settingslib.bluetooth;

import android.bluetooth.BluetoothAdapter;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.Handler;
import android.os.SystemProperties;
import android.support.v7.preference.Preference;
import android.util.Log;

/**
 * BluetoothDiscoverableEnabler is a helper to manage the "Discoverable"
 * checkbox. It sets/unsets discoverability and keeps track of how much time
 * until the the discoverability is automatically turned off.
 */
public final class BluetoothDiscoverableEnabler implements Preference.OnPreferenceClickListener {

    private static final String TAG = "BluetoothDiscoverableEnabler";

    private static final String SYSTEM_PROPERTY_DISCOVERABLE_TIMEOUT =
            "debug.bt.discoverable_time";

    private static final int DISCOVERABLE_TIMEOUT_TWO_MINUTES = 120;
    private static final int DISCOVERABLE_TIMEOUT_FIVE_MINUTES = 300;
    private static final int DISCOVERABLE_TIMEOUT_ONE_HOUR = 3600;
    public static final int DISCOVERABLE_TIMEOUT_NEVER = 0;

    // Bluetooth advanced settings screen was replaced with action bar items.
    // Use the same preference key for discoverable timeout as the old ListPreference.
    private static final String KEY_DISCOVERABLE_TIMEOUT = "bt_discoverable_timeout";
  //[FEATURE]-Add-BEGIN by TCTNB.dongdong.gong,10/10/2016,3055667,
  //For Dual Bluetooth Dev
    private static final String SHARED_PREFERENCES_NAME = "bluetooth_settings";
  //[FEATURE]-Add-END by TCTNB.dongdong.gong

    private static final String VALUE_DISCOVERABLE_TIMEOUT_TWO_MINUTES = "twomin";
    private static final String VALUE_DISCOVERABLE_TIMEOUT_FIVE_MINUTES = "fivemin";
    private static final String VALUE_DISCOVERABLE_TIMEOUT_ONE_HOUR = "onehour";
    private static final String VALUE_DISCOVERABLE_TIMEOUT_NEVER = "never";

    public static final int DEFAULT_DISCOVERABLE_TIMEOUT = DISCOVERABLE_TIMEOUT_NEVER;

    private Context mContext;
    private final Handler mUiHandler;
    private final BluetoothPreferenceUp mDiscoveryPreference;

    private final LocalBluetoothAdapter mLocalAdapter;

    private final SharedPreferences mSharedPreferences;

    private boolean mDiscoverable;
    private int mNumberOfPairedDevices;
    private BluetoothPreferenceUp.notifyChange notifyChange = null;

    private int mTimeoutSecs = -1;
  //[FEATURE]-Add-BEGIN by TCTNB.dongdong.gong,10/10/2016,3055667,
  //For Dual Bluetooth Dev
    private boolean mDualBluetoothAbled = true;
  //[FEATURE]-Add-END by TCTNB.dongdong.gong

    private final BroadcastReceiver mReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (BluetoothAdapter.ACTION_SCAN_MODE_CHANGED.equals(intent.getAction())) {
                int mode = intent.getIntExtra(BluetoothAdapter.EXTRA_SCAN_MODE,
                        BluetoothAdapter.ERROR);
                if (mode != BluetoothAdapter.ERROR) {
                    handleModeChanged(mode);
                }
            }
        }
    };

    private final Runnable mUpdateCountdownSummaryRunnable = new Runnable() {
        public void run() {
            updateCountdownSummary();
        }
    };
//[Feature] Add-start dongdong.gong defect 2641778
    public BluetoothDiscoverableEnabler(Context context, LocalBluetoothAdapter adapter,
    		BluetoothPreferenceUp discoveryPreference) {
        mUiHandler = new Handler();
        mLocalAdapter = adapter;
        mContext = context;
        mDiscoveryPreference = discoveryPreference;
      //[FEATURE]-Add-BEGIN by TCTNB.dongdong.gong,10/10/2016,3055667,
      //For Dual Bluetooth Dev
        mSharedPreferences = context.getSharedPreferences(SHARED_PREFERENCES_NAME, Context.MODE_PRIVATE);
      //[FEATURE]-Add-END by TCTNB.dongdong.gong
        discoveryPreference.setPersistent(false);
    }

    public void setNofityChange(BluetoothPreferenceUp.notifyChange notifyChange){
    	this.notifyChange = notifyChange;
    }

    public void clearNotifyChange(){
    	this.notifyChange = null;
    }
//[Feature] Add-end
    public void resume(Context context) {
        if (mLocalAdapter == null) {
            return;
        }

        if (mContext != context) {
            mContext = context;
        }

        IntentFilter filter = new IntentFilter(BluetoothAdapter.ACTION_SCAN_MODE_CHANGED);
        mContext.registerReceiver(mReceiver, filter);
      //[FEATURE]-Add-BEGIN by TCTNB.dongdong.gong,10/10/2016,3055667,
      //For Dual Bluetooth Dev
        if(!mDualBluetoothAbled)
            mDiscoveryPreference.setOnPreferenceClickListener(this);
      //[FEATURE]-Add-END by TCTNB.dongdong.gong
        mDiscoveryPreference.registeNotify(notifyChange);
        handleModeChanged(mLocalAdapter.getScanMode());
    }

    public void pause() {
        if (mLocalAdapter == null) {
            return;
        }

        mUiHandler.removeCallbacks(mUpdateCountdownSummaryRunnable);
        mContext.unregisterReceiver(mReceiver);
      //[FEATURE]-Add-BEGIN by TCTNB.dongdong.gong,10/10/2016,3055667,
      //For Dual Bluetooth Dev
        if(!mDualBluetoothAbled)
            mDiscoveryPreference.setOnPreferenceClickListener(null);
      //[FEATURE]-Add-END by TCTNB.dongdong.gong
        mDiscoveryPreference.unRegisteNotify();
    }

    public boolean onPreferenceClick(Preference preference) {
        // toggle discoverability
        mDiscoverable = !mDiscoverable;
        setEnabled(mDiscoverable);
        return true;
    }

    private void setEnabled(boolean enable) {
        if (enable) {
            int timeout = getDiscoverableTimeout();
            long endTimestamp = System.currentTimeMillis() + timeout * 1000L;
            LocalBluetoothPreferences.persistDiscoverableEndTimestamp(mContext, endTimestamp);

            mLocalAdapter.setScanMode(BluetoothAdapter.SCAN_MODE_CONNECTABLE_DISCOVERABLE, timeout);
            updateCountdownSummary();

            Log.d(TAG, "setEnabled(): enabled = " + enable + "timeout = " + timeout);

            if (timeout > 0) {
                BluetoothDiscoverableTimeoutReceiver.setDiscoverableAlarm(mContext, endTimestamp);
            } else {
                BluetoothDiscoverableTimeoutReceiver.cancelDiscoverableAlarm(mContext);
            }

        } else {
            mLocalAdapter.setScanMode(BluetoothAdapter.SCAN_MODE_CONNECTABLE);
            BluetoothDiscoverableTimeoutReceiver.cancelDiscoverableAlarm(mContext);
        }
    }

    private void updateTimerDisplay(int timeout) {
        if (getDiscoverableTimeout() == DISCOVERABLE_TIMEOUT_NEVER) {
//            mDiscoveryPreference.setSummary(R.string.bluetooth_is_discoverable_always);
        	mDiscoveryPreference.notifySummaryDiscAlways();
        } else {
            String textTimeout = formatTimeRemaining(timeout);
//            mDiscoveryPreference.setSummary(mContext.getString(R.string.bluetooth_is_discoverable,
//                    textTimeout));
            mDiscoveryPreference.notifySummaryIsDiscover(textTimeout);
        }
    }

    private static String formatTimeRemaining(int timeout) {
        StringBuilder sb = new StringBuilder(6);    // "mmm:ss"
        int min = timeout / 60;
        sb.append(min).append(':');
        int sec = timeout - (min * 60);
        if (sec < 10) {
            sb.append('0');
        }
        sb.append(sec);
        return sb.toString();
    }

    public void setDiscoverableTimeout(int index) {
        String timeoutValue;
        if (mDualBluetoothAbled) {
			switch (index) {
			case 0:
				mTimeoutSecs = DISCOVERABLE_TIMEOUT_TWO_MINUTES;
				timeoutValue = VALUE_DISCOVERABLE_TIMEOUT_TWO_MINUTES;
				break;

			case 1:
				mTimeoutSecs = DISCOVERABLE_TIMEOUT_FIVE_MINUTES;
				timeoutValue = VALUE_DISCOVERABLE_TIMEOUT_FIVE_MINUTES;
				break;

			case 2:
				mTimeoutSecs = DISCOVERABLE_TIMEOUT_ONE_HOUR;
				timeoutValue = VALUE_DISCOVERABLE_TIMEOUT_ONE_HOUR;
				break;

			case 3:
			default:
				mTimeoutSecs = DISCOVERABLE_TIMEOUT_NEVER;
				timeoutValue = VALUE_DISCOVERABLE_TIMEOUT_NEVER;
				break;
			}
		}
        else{
        	switch (index) {
			case 0:
			default:
				mTimeoutSecs = DISCOVERABLE_TIMEOUT_TWO_MINUTES;
				timeoutValue = VALUE_DISCOVERABLE_TIMEOUT_TWO_MINUTES;
				break;

			case 1:
				mTimeoutSecs = DISCOVERABLE_TIMEOUT_FIVE_MINUTES;
				timeoutValue = VALUE_DISCOVERABLE_TIMEOUT_FIVE_MINUTES;
				break;

			case 2:
				mTimeoutSecs = DISCOVERABLE_TIMEOUT_ONE_HOUR;
				timeoutValue = VALUE_DISCOVERABLE_TIMEOUT_ONE_HOUR;
				break;

			case 3:
				mTimeoutSecs = DISCOVERABLE_TIMEOUT_NEVER;
				timeoutValue = VALUE_DISCOVERABLE_TIMEOUT_NEVER;
				break;
			}
        }
        mSharedPreferences.edit().putString(KEY_DISCOVERABLE_TIMEOUT, timeoutValue).apply();
        setEnabled(true);   // enable discovery and reset timer
    }

    private int getDiscoverableTimeout() {
        if (mTimeoutSecs != -1) {
            return mTimeoutSecs;
        }
        int timeout = SystemProperties.getInt(SYSTEM_PROPERTY_DISCOVERABLE_TIMEOUT, -1);
        if (timeout < 0) {
            String timeoutValue;
			if (mDualBluetoothAbled) {
				timeoutValue = mSharedPreferences.getString(
						KEY_DISCOVERABLE_TIMEOUT,
						VALUE_DISCOVERABLE_TIMEOUT_NEVER);
			}
			else{
				timeoutValue = mSharedPreferences.getString(
						KEY_DISCOVERABLE_TIMEOUT,
						VALUE_DISCOVERABLE_TIMEOUT_TWO_MINUTES);
			}
            if (timeoutValue.equals(VALUE_DISCOVERABLE_TIMEOUT_NEVER)) {
                timeout = DISCOVERABLE_TIMEOUT_NEVER;
            } else if (timeoutValue.equals(VALUE_DISCOVERABLE_TIMEOUT_ONE_HOUR)) {
                timeout = DISCOVERABLE_TIMEOUT_ONE_HOUR;
            } else if (timeoutValue.equals(VALUE_DISCOVERABLE_TIMEOUT_FIVE_MINUTES)) {
                timeout = DISCOVERABLE_TIMEOUT_FIVE_MINUTES;
            } else {
                timeout = DISCOVERABLE_TIMEOUT_TWO_MINUTES;
            }
        }
        
        mTimeoutSecs = timeout;
        return timeout;
    }


    public int getDiscoverableTimeoutIndex() {
        int timeout = getDiscoverableTimeout();
        if (mDualBluetoothAbled) {
			switch (timeout) {
			case DISCOVERABLE_TIMEOUT_TWO_MINUTES:
				return 0;

			case DISCOVERABLE_TIMEOUT_FIVE_MINUTES:
				return 1;

			case DISCOVERABLE_TIMEOUT_ONE_HOUR:
				return 2;

			case DISCOVERABLE_TIMEOUT_NEVER:
			default:
				return 3;
			}
		}
        else{
        	switch (timeout) {
			case DISCOVERABLE_TIMEOUT_TWO_MINUTES:
			default:
				return 0;

			case DISCOVERABLE_TIMEOUT_FIVE_MINUTES:
				return 1;

			case DISCOVERABLE_TIMEOUT_ONE_HOUR:
				return 2;

			case DISCOVERABLE_TIMEOUT_NEVER:
				return 3;
			}
        }
    }

    public void setNumberOfPairedDevices(int pairedDevices) {
        mNumberOfPairedDevices = pairedDevices;
        handleModeChanged(mLocalAdapter.getScanMode());
    }

    public void handleModeChanged(int mode) {
        Log.d(TAG, "handleModeChanged(): mode = " + mode);
        if (mode == BluetoothAdapter.SCAN_MODE_CONNECTABLE_DISCOVERABLE) {
            mDiscoverable = true;
            updateCountdownSummary();
        } else {
            mDiscoverable = false;
            setSummaryNotDiscoverable();
        }
    }

    private void setSummaryNotDiscoverable() {
        if (mNumberOfPairedDevices != 0) {
//            mDiscoveryPreference.setSummary(R.string.bluetooth_only_visible_to_paired_devices);
        	mDiscoveryPreference.notifySummaryVisible2Paired();
        } else {
//            mDiscoveryPreference.setSummary(R.string.bluetooth_not_visible_to_other_devices);
            mDiscoveryPreference.notifySummaryInvisible2Other();
        }
    }

    private void updateCountdownSummary() {
        int mode = mLocalAdapter.getScanMode();
        if (mode != BluetoothAdapter.SCAN_MODE_CONNECTABLE_DISCOVERABLE) {
            return;
        }

        long currentTimestamp = System.currentTimeMillis();
        long endTimestamp = LocalBluetoothPreferences.getDiscoverableEndTimestamp(mContext);

        if (currentTimestamp > endTimestamp) {
            // We're still in discoverable mode, but maybe there isn't a timeout.
            updateTimerDisplay(0);
            return;
        }

        int timeLeft = (int) ((endTimestamp - currentTimestamp) / 1000L);
        updateTimerDisplay(timeLeft);

        synchronized (this) {
            mUiHandler.removeCallbacks(mUpdateCountdownSummaryRunnable);
            mUiHandler.postDelayed(mUpdateCountdownSummaryRunnable, 1000);
        }
    }
}
