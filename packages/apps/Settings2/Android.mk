ifeq ($(SMCN_ROM_CONTROL_FLAG), true)
LOCAL_PATH:= $(call my-dir)
include $(CLEAR_VARS)

#ADD-qcrilhook by Dandan.Fang  2016/08/30 TASK2817986
LOCAL_JAVA_LIBRARIES := bouncycastle core-oj telephony-common ims-common qcrilhook telephony-ext
#ADD-libchips by Wanshun.Ni  2016/09/08 TASK2827925
LOCAL_STATIC_JAVA_LIBRARIES := \
    android-support-v4 \
    android-support-v13 \
    android-support-v7-recyclerview \
    android-support-v7-preference \
    android-support-v7-appcompat \
    android-support-v14-preference \
    jsr305 \
    libchips\
    libtclaccount

LOCAL_MODULE_TAGS := optional
#ADD- tct-src by Wanshun.Ni  2016/09/08 TASK2827925
LOCAL_SRC_FILES := \
        $(call all-java-files-under, src) \
        $(call all-java-files-under, tct-src) \
        src/com/android/settings/EventLogTags.logtags \
        libs

LOCAL_RESOURCE_DIR := $(LOCAL_PATH)/res \
    frameworks/support/v7/preference/res \
    frameworks/support/v14/preference/res \
    frameworks/support/v7/appcompat/res \
    frameworks/support/v7/recyclerview/res \
    frameworks/opt/chips/res/

LOCAL_PACKAGE_NAME := Settings2
LOCAL_CERTIFICATE := platform
LOCAL_PRIVILEGED_MODULE := true
LOCAL_OVERRIDES_PACKAGES := Settings

LOCAL_PROGUARD_FLAG_FILES := proguard.flags

LOCAL_AAPT_FLAGS := --auto-add-overlay \
    --extra-packages android.support.v7.preference:android.support.v14.preference:android.support.v17.preference:android.support.v7.appcompat:android.support.v7.recyclerview:com.android.ex.chips


LOCAL_AAPT_FLAGS += -I out/target/common/obj/APPS/mst-framework-res_intermediates/package-export.apk


LOCAL_JAVA_LIBRARIES += mst-framework 


ifneq ($(INCREMENTAL_BUILDS),)
    LOCAL_PROGUARD_ENABLED := disabled
    LOCAL_JACK_ENABLED := incremental
    LOCAL_DX_FLAGS := --multi-dex
    LOCAL_JACK_FLAGS := --multi-dex native
endif

include frameworks/opt/setupwizard/library/common-full-support.mk
include packages/apps/Settings2/SettingsLib_London/common.mk

include $(BUILD_PACKAGE)
include $(BUILD_PLF)
################################################################################
include $(CLEAR_VARS)
LOCAL_PREBUILT_STATIC_JAVA_LIBRARIES := libtclaccount:libs/AcccountSDK_V1.2.8.38_20161014_release.jar
include $(BUILD_MULTI_PREBUILT)

# Use the following include to make our test apk.
ifeq (,$(ONE_SHOT_MAKEFILE))
include $(call all-makefiles-under,$(LOCAL_PATH))
endif
endif
