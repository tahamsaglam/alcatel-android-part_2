package com.mms.wrap;


import java.util.regex.Matcher;
import java.util.regex.Pattern;

import android.content.Context;
import android.util.Log;

import com.qcom.frameworks.mms.TctFrameworksPlf;
import com.qcom.frameworks.mms.TctPatterns;

class WrapQcom extends WrapCommon implements IWrapQcom {

    @Override
    public String getTctDigitsAsteriskAndPlusOnly(Matcher matcher) {
        return TctPatterns.tct_digitsAsteriskAndPlusOnly(matcher);
    }

    @Override
    public Pattern getTctIsraelPattern() {
        return TctPatterns.TCT_PHONE_ISRAEL;
    }

    @Override
    public boolean getBooleanFromFrameworksPlf(Context context,String key) {
        try {
            return TctFrameworksPlf.getBooleanFromFrameworksPlf(context,key);
        } catch (Exception e) {
            e.printStackTrace();
            Log.e(TAG,"getBooleanFromFrameworksPlf error key:" + key);
        }
        return false;
    }
}
