<?xml version="1.0" encoding="utf-8"?>
<!-- Copyright (C) 2011 The Android Open Source Project

     Licensed under the Apache License, Version 2.0 (the "License");
     you may not use this file except in compliance with the License.
     You may obtain a copy of the License at

          http://www.apache.org/licenses/LICENSE-2.0

     Unless required by applicable law or agreed to in writing, software
     distributed under the License is distributed on an "AS IS" BASIS,
     WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
     See the License for the specific language governing permissions and
     limitations under the License.
-->

<resources xmlns:android="http://schemas.android.com/apk/res/android"
    xmlns:xliff="urn:oasis:names:tc:xliff:document:1.2">

    <!-- The name of the Cell Broadcast Receiver app. [CHAR LIMIT=NONE] -->
    <string name="app_label">Cell Broadcasts</string>

    <!-- Label for settings screen. [CHAR LIMIT=NONE] -->
    <string name="sms_cb_settings">Cell Broadcast settings</string>

    <!-- Error message for users that aren't allowed to modify Cell broadcast settings [CHAR LIMIT=none] -->
    <string name="cell_broadcast_settings_not_available">Cell Broadcast settings are not available for this user</string>

    <!-- Text for dismiss button in broadcast message view dialog. [CHAR LIMIT=25] -->
    <string name="button_dismiss">OK</string>

    <!-- Text for list view when empty (no broadcasts). [CHAR LIMIT=200] -->
    <string name="no_cell_broadcasts">There are no active alerts in your area. You can change the Alert settings using the Settings menu option.</string>

    <!-- Menu item for accessing application settings. [CHAR LIMIT=30] -->
    <string name="menu_preferences">Settings</string>
    <!-- Menu item for deleting all broadcasts. [CHAR LIMIT=30] -->
    <string name="menu_delete_all">Delete broadcasts</string>

    <!-- Header for context menu on an individual broadcast. [CHAR LIMIT=30] -->
    <string name="message_options">Message options</string>
    <!-- Context menu item for viewing broadcast details such as time and date. [CHAR LIMIT=30] -->
    <string name="menu_view_details">View details</string>
    <!-- Context menu item to delete a previously received broadcast. [CHAR LIMIT=30] -->
    <string name="menu_delete">Delete broadcast</string>

    <!-- Title of "View details" dialog -->
    <string name="view_details_title">Alert details</string>

    <!-- Confirm Delete -->
    <!-- Delete broadcast confirmation dialog message. [CHAR LIMIT=NONE] -->
    <string name="confirm_delete_broadcast">Delete this broadcast?</string>
    <!-- Delete all broadcasts confirmation dialog message. [CHAR LIMIT=NONE] -->
    <string name="confirm_delete_all_broadcasts">Delete all received broadcast messages?</string>
    <!-- Delete button text for delete broadcast dialog. [CHAR LIMIT=25] -->
    <string name="button_delete">Delete</string>
    <!-- Cancel button text for delete broadcast dialog. [CHAR LIMIT=25] -->
    <string name="button_cancel">Cancel</string>

    <!-- ETWS dialog title for Earthquake Warning. [CHAR LIMIT=50] -->
    <string name="etws_earthquake_warning">Earthquake warning</string>
    <!-- ETWS dialog title for Tsunami Warning. [CHAR LIMIT=50] -->
    <string name="etws_tsunami_warning">Tsunami warning</string>
    <!-- ETWS dialog title for Earthquake and Tsunami Warning. [CHAR LIMIT=50] -->
    <string name="etws_earthquake_and_tsunami_warning">Earthquake and tsunami warning</string>
    <!-- ETWS dialog title for test message. [CHAR LIMIT=50] -->
    <string name="etws_test_message">ETWS test message</string>
    <!-- ETWS dialog title for other emergency type. [CHAR LIMIT=50] -->
    <string name="etws_other_emergency_type">Emergency warning</string>
    <!-- CMAS dialog title for presidential level alert. [CHAR LIMIT=50] -->
    <string name="cmas_presidential_level_alert">Presidential alert</string>
    <!-- CMAS dialog title for extreme alert. [CHAR LIMIT=50] -->
    <string name="cmas_extreme_alert">Emergency alert: Extreme</string>
    <!-- CMAS dialog title for severe alert. [CHAR LIMIT=50] -->
    <string name="cmas_severe_alert">Emergency alert: Severe</string>
    <!-- CMAS dialog title for child abduction emergency (Amber Alert). [CHAR LIMIT=50] -->
    <string name="cmas_amber_alert">Child abduction (Amber alert)</string>
    <!-- CMAS dialog title for required monthly test. [CHAR LIMIT=50] -->
    <string name="cmas_required_monthly_test">Emergency alert monthly test</string>
    <!-- CMAS dialog title for CMAS Exercise. [CHAR LIMIT=50] -->
    <string name="cmas_exercise_alert">Emergency alert (exercise)</string>
    <!-- CMAS dialog title for operator defined use. [CHAR LIMIT=50] -->
    <string name="cmas_operator_defined_alert">Emergency alert (operator)</string>
    <!-- Dialog title for all other message identifiers in the PWS range. [CHAR LIMIT=50] -->
    <string name="pws_other_message_identifiers">Emergency alert</string>
    <!-- Dialog title for all non-emergency cell broadcasts. [CHAR LIMIT=50] -->
    <string name="cb_other_message_identifiers">Cell broadcast</string>

    <!-- Preference category title for emergency alert settings. [CHAR LIMIT=50] -->
    <string name="emergency_alert_settings_title">Emergency alert settings</string>
    <!-- Preference title for enable emergency alerts checkbox. [CHAR LIMIT=30] -->
    <string name="enable_emergency_alerts_title">Turn on notifications</string>
    <!-- Preference summary for enable notifications checkbox. [CHAR LIMIT=50] -->
    <string name="enable_emergency_alerts_summary">Display emergency alert broadcasts</string>
    <!-- Preference title for alert sound duration list. [CHAR LIMIT=30] -->
    <string name="alert_sound_duration_title">Alert sound duration</string>
    <!-- Do not translate. Empty summary for alert duration (set by CellBroadcastSettings). -->
    <string name="alert_sound_duration_summary"></string>
    <!-- Preference title for alert reminder interval list. [CHAR LIMIT=30] -->
    <string name="alert_reminder_interval_title">Alert reminder</string>
    <!-- Do not translate. Empty summary for alert reminder (set by CellBroadcastSettings). -->
    <string name="alert_reminder_interval_summary"></string>
    <!-- Preference title for enable text-to-speech checkbox. [CHAR LIMIT=30] -->
    <string name="enable_alert_speech_title">Speak alert message</string>
    <!-- Preference summary for enable text-to-speech checkbox. [CHAR LIMIT=100] -->
    <string name="enable_alert_speech_summary">Use text-to-speech to speak emergency alert messages</string>

    <!-- Preference category title for ETWS settings. [CHAR LIMIT=50] -->
    <string name="category_etws_settings_title">ETWS settings</string>
    <!-- Preference title for enable ETWS test alerts checkbox. [CHAR LIMIT=30] -->
    <string name="enable_etws_test_alerts_title">Show ETWS test broadcasts</string>
    <!-- Preference summary for enable ETWS test alerts checkbox. [CHAR LIMIT=100] -->
    <string name="enable_etws_test_alerts_summary">Display test broadcasts for Earthquake Tsunami Warning System</string>

    <!-- Preference title for presidential Alerts checkbox. [CHAR LIMIT=30] -->
    <string name="enable_cmas_presidential_alerts_title">Show presidential alerts</string>
    <!-- Preference summary for presidential Alerts checkbox. [CHAR LIMIT=100] -->
    <string name="enable_cmas_presidential_alerts_summary">Always display presidential alerts</string>
    <!-- Preference title for enable CMAS extreme threat alerts checkbox. [CHAR LIMIT=30] -->
    <string name="enable_cmas_extreme_threat_alerts_title">Show extreme threats</string>
    <!-- Preference summary for enable CMAS extreme threat alerts checkbox. [CHAR LIMIT=100] -->
    <string name="enable_cmas_extreme_threat_alerts_summary">Display alerts for extreme threats to life and property</string>
    <!-- Preference title for enable CMAS severe threat alerts checkbox. [CHAR LIMIT=30] -->
    <string name="enable_cmas_severe_threat_alerts_title">Show severe threats</string>
    <!-- Preference summary for enable CMAS severe threat alerts checkbox. [CHAR LIMIT=100] -->
    <string name="enable_cmas_severe_threat_alerts_summary">Display alerts for severe threats to life and property</string>
    <!-- Preference title for enable CMAS amber alerts checkbox. [CHAR LIMIT=50] -->
    <string name="enable_cmas_amber_alerts_title">Show AMBER alerts</string>
    <!-- Preference summary for enable CMAS amber alerts checkbox. [CHAR LIMIT=100] -->
    <string name="enable_cmas_amber_alerts_summary">Display child abduction emergency bulletins (AMBER alert)</string>

    <!-- Preference title for enable CMAS test alerts checkbox. [CHAR LIMIT=30] -->
    <string name="enable_cmas_test_alerts_title">Show CMAS test broadcasts</string>
    <!-- Preference summary for enable CMAS test alerts checkbox. [CHAR LIMIT=100] -->
    <string name="enable_cmas_test_alerts_summary">Display test broadcasts for Commercial Mobile Alert System</string>

    <!-- Preference title for CMAS vibration on/off. [CHAR LIMIT=30] -->
    <string name="enable_alert_vibrate_title">Vibrate</string>
    <!-- Preference summary for CMAS vibration on/off. [CHAR LIMIT=60] -->
    <string name="enable_alert_vibrate_summary">Vibrate on alert</string>
    <string name="enable_alert_tone_title">Alert tone</string>
    <string name="enable_alert_tone_summary">Alert tone on</string>

    <!-- Preference category title for Brazil settings. [CHAR LIMIT=50] -->
    <string name="category_brazil_settings_title">Settings for Brazil</string>
    <!-- Preference category title for India settings. [CHAR LIMIT=50] -->
    <string name="category_india_settings_title">Settings for India</string>
    <!-- Preference title for enable channel 50 alerts (Brazil only). [CHAR LIMIT=30] -->
    <string name="enable_channel_50_alerts_title">Show channel 50 broadcasts</string>
    <!-- Preference summary for enable channel 50 alerts (Brazil only). [CHAR LIMIT=100] -->
    <string name="enable_channel_50_alerts_summary">Channel 50 is used in Brazil for area update information</string>
    <!-- Preference title for enable channel 60 alerts. [CHAR LIMIT=30] -->
    <string name="enable_channel_60_alerts_title">Show channel 60 broadcasts</string>
    <!-- Preference summary for enable channel 60 alerts. [CHAR LIMIT=100] -->
    <string name="enable_channel_60_alerts_summary">Channel 60 is used in India for Operator specific information</string>

    <!-- Preference category title for developer settings. [CHAR LIMIT=50] -->
    <string name="category_dev_settings_title">Developer options</string>

    <!-- CMAS alert category heading (including colon). [CHAR LIMIT=30] -->
    <string name="cmas_category_heading">Alert Category:</string>
    <!-- CMAS category for geophysical alerts. [CHAR LIMIT=50] -->
    <string name="cmas_category_geo">Geophysical</string>
    <!-- CMAS category for meteorological alerts. [CHAR LIMIT=50] -->
    <string name="cmas_category_met">Meteorological</string>
    <!-- CMAS category for general emergency and public safety alerts. [CHAR LIMIT=50] -->
    <string name="cmas_category_safety">Safety</string>
    <!-- CMAS category for security alerts (law enforcement, military, etc.). [CHAR LIMIT=50] -->
    <string name="cmas_category_security">Security</string>
    <!-- CMAS category for rescue and recovery alerts. [CHAR LIMIT=50] -->
    <string name="cmas_category_rescue">Rescue</string>
    <!-- CMAS category for fire suppression and rescue alerts. [CHAR LIMIT=50] -->
    <string name="cmas_category_fire">Fire</string>
    <!-- CMAS category for medical and public health alerts. [CHAR LIMIT=50] -->
    <string name="cmas_category_health">Health</string>
    <!-- CMAS category for pollution and other environmental alerts. [CHAR LIMIT=50] -->
    <string name="cmas_category_env">Environmental</string>
    <!-- CMAS category for transportation alerts. [CHAR LIMIT=50] -->
    <string name="cmas_category_transport">Transportation</string>
    <!-- CMAS category for utility, telecommunication, and other infrastructure alerts. [CHAR LIMIT=50] -->
    <string name="cmas_category_infra">Infrastructure</string>
    <!-- CMAS category for chemical, biological, radiological, nuclear alerts. [CHAR LIMIT=50] -->
    <string name="cmas_category_cbrne">Chemical/Biological/Nuclear/Explosive</string>
    <!-- CMAS category for other alerts. [CHAR LIMIT=50] -->
    <string name="cmas_category_other">Other</string>

    <!-- CMAS response type heading (including colon). [CHAR LIMIT=30] -->
    <string name="cmas_response_heading">Response Type:</string>
    <!-- CMAS response type: take shelter in place. [CHAR LIMIT=50] -->
    <string name="cmas_response_shelter">Shelter</string>
    <!-- CMAS response type: evacuate (relocate). [CHAR LIMIT=50] -->
    <string name="cmas_response_evacuate">Evacuate</string>
    <!-- CMAS response type: make preparations. [CHAR LIMIT=50] -->
    <string name="cmas_response_prepare">Prepare</string>
    <!-- CMAS response type: execute a pre-planned activity. [CHAR LIMIT=50] -->
    <string name="cmas_response_execute">Execute</string>
    <!-- CMAS response type: monitor information sources. [CHAR LIMIT=50] -->
    <string name="cmas_response_monitor">Monitor</string>
    <!-- CMAS response type: avoid hazard. [CHAR LIMIT=50] -->
    <string name="cmas_response_avoid">Avoid</string>
    <!-- CMAS response type: evaluate the information in this message. [CHAR LIMIT=50] -->
    <string name="cmas_response_assess">Assess</string>
    <!-- CMAS response type: no action recommended. [CHAR LIMIT=50] -->
    <string name="cmas_response_none">None</string>

    <!-- CMAS severity heading (including colon). [CHAR LIMIT=30] -->
    <string name="cmas_severity_heading">Severity:</string>
    <!-- CMAS severity type: extreme. [CHAR LIMIT=30] -->
    <string name="cmas_severity_extreme">Extreme</string>
    <!-- CMAS severity type: severe. [CHAR LIMIT=30] -->
    <string name="cmas_severity_severe">Severe</string>

    <!-- CMAS urgency heading (including colon). [CHAR LIMIT=30] -->
    <string name="cmas_urgency_heading">Urgency:</string>
    <!-- CMAS urgency type: take responsive action immediately. [CHAR LIMIT=30] -->
    <string name="cmas_urgency_immediate">Immediate</string>
    <!-- CMAS severity type: severe. [CHAR LIMIT=30] -->
    <string name="cmas_urgency_expected">Expected</string>

    <!-- CMAS certainty heading (including colon). [CHAR LIMIT=30] -->
    <string name="cmas_certainty_heading">Certainty:</string>
    <!-- CMAS certainty type: observed. [CHAR LIMIT=30] -->
    <string name="cmas_certainty_observed">Observed</string>
    <!-- CMAS severity type: severe. [CHAR LIMIT=30] -->
    <string name="cmas_certainty_likely">Likely</string>

    <!-- Message delivery time (including colon). [CHAR LIMIT=30] -->
    <string name="delivery_time_heading">Received:</string>

    <!-- Non-emergency broadcast notification description for multiple unread alerts. -->
    <string name="notification_multiple"><xliff:g id="count">%s</xliff:g> unread alerts.</string>
    <!-- Non-emergency broadcast notification title for multiple unread alerts. -->
    <string name="notification_multiple_title">New alerts</string>

    <!-- Show CMAS opt-out dialog on first non-Presidential alert. [CHAR LIMIT=100] -->
    <string name="show_cmas_opt_out_summary">Show an opt-out dialog after displaying the first CMAS alert (other than Presidential Alert).</string>
    <!-- Show CMAS opt-out dialog on first non-Presidential alert. [CHAR LIMIT=40] -->
    <string name="show_cmas_opt_out_title">Show opt-out dialog</string>

    <!-- CMAS opt-out dialog message. [CHAR LIMIT=160] -->
    <string name="cmas_opt_out_dialog_text">You are currently receiving Emergency Alerts. Would you like to continue receiving Emergency Alerts?</string>
    <!-- Text for positive button in CMAS opt-out dialog. [CHAR LIMIT=25] -->
    <string name="cmas_opt_out_button_yes">Yes</string>
    <!-- Text for negative button in CMAS opt-out dialog. [CHAR LIMIT=25] -->
    <string name="cmas_opt_out_button_no">No</string>

    <!-- Entries listed in the ListPreference for allowed alert durations. [CHAR LIMIT=30] -->
    <string-array name="alert_sound_duration_entries">
      <item>2 seconds</item>
      <item>4 seconds</item>
      <item>6 seconds</item>
      <item>8 seconds</item>
      <item>10 seconds</item>
    </string-array>

    <!-- Do not translate. Values that are retrieved from the ListPreference.
         These must match the alert_sound_duration_entries above. -->
    <string-array name="alert_sound_duration_values">
      <item>2</item>
      <item>4</item>
      <item>6</item>
      <item>8</item>
      <item>10</item>
    </string-array>

    <!-- Entries in the ListPreference for alert reminder intervals. [CHAR LIMIT=30] -->
    <string-array name="alert_reminder_interval_entries">
      <item>Once</item>
      <item>Every 2 minutes</item>
      <item>Every 15 minutes</item>
      <item>Off</item>
    </string-array>

    <!-- Do not translate. Values that are retrieved from the ListPreference.
         These must match the alert_reminder_interval_entries list above. -->
    <string-array name="alert_reminder_interval_values">
      <item>1</item>
      <item>2</item>
      <item>15</item>
      <item>0</item>
    </string-array>

<!-- [SOLUTION]-Add-BEGIN by TCTNB.(JiangLong Pan), 07/20/2016, SOLUTION-2520283,-->
<!-- Porting CellBroadcast Related -->
    <!-- CMAS dialog title for Spanish presidential level alert. [CHAR LIMIT=50] -->
    <string name="cmas_spanish_presidential_level_alert">Spanish presidential alert</string>
    <!-- CMAS dialog title for Spanish extreme alert. [CHAR LIMIT=50] -->
    <string name="cmas_spanish_extreme_alert">Spanish emergency alert: Extreme</string>
    <!-- CMAS dialog title for Spanish severe alert. [CHAR LIMIT=50] -->
    <string name="cmas_spanish_severe_alert">Spanish emergency alert: Severe</string>
    <!-- CMAS dialog title for Spanish child abduction emergency (Amber Alert). [CHAR LIMIT=50] -->
    <string name="cmas_spanish_amber_alert">Spanish child abduction (Amber alert)</string>
    <!-- CMAS dialog title for Spanish required monthly test. [CHAR LIMIT=50] -->
    <string name="cmas_spanish_required_monthly_test">Spanish emergency alert monthly test</string>
    <!-- CMAS dialog title for Spanish CMAS Exercise. [CHAR LIMIT=50] -->
    <string name="cmas_spanish_exercise_alert">Spanish emergency alert (exercise)</string>
    <!-- CMAS dialog title for Spanish operator defined use. [CHAR LIMIT=50] -->
    <string name="cmas_spanish_operator_defined_alert">Spanish emergency alert (operator)</string>

    <!-- Preference title for enable CMAS Spanish Language alerts checkbox. -->
    <string name="enable_cmas_spanish_language_alerts_title">Spanish Language</string>
    <!-- Preference summary for enable CMAS Spanish Language alerts checkbox. -->
    <string name="enable_cmas_spanish_language_alerts_summary">Display alerts for Spanish language message</string>

    <!-- Non-emergency broadcast notification title for multiple unread alerts. -->
    <string name="notification_multiple_title_cb">New alerts</string>
    <string name="notification_multiple_title_cmas">Emergency Alerts</string>

    <!--CBC notification with pop up and tone alert + vibrate in CHILE -->
    <!--chile request for smcbs Alerta de Emergencia -->
    <string name="title_chile_cb_dialog">Emergency Alert</string>
    <string name="title_netherlands_cmas_dialog">NL-Alert</string>

    <string name="action_bar_title_choose_messages">Choose conversation</string>
    <string name="one_selected_message_to_be_deleted">1 message will be deleted.</string>
    <string name="multi_selected_message_to_be_deleted"><xliff:g id="count">%s</xliff:g> messages will be deleted.</string>
    <string name="confirm_dialog_title">Delete?</string>

    <string name="cbsetting">Cell broadcast</string>
    <string name="summary_cbsetting">Set cell broadcast channel</string>

    <!--    cb setting string start-->
    <string name="title_receive_channel_list">Select all channels</string>
    <string name="title_channel_list">Channel list</string>
    <string name="summary_channel_list">Select or not select all channels</string>
    <string name="title_add_channel">Add channel</string>
    <string name="summary_add_channel">Add channel name and channel index</string>
    <string name="channel_name">Channel name (1-10 characters)</string>
    <string name="channel_index">Channel number (0~999)</string>
    <string name="enable_channel">Enable channel</string>
    <string name="ChannelNameOut">The channel name you entered already exceeds its max (14 characters), please adjust!</string>
    <string name="title_cell_broadcast">Normal Cell broadcast</string>

    <!--    start add channel-->
    <string name="channel_label">Add channel</string>
    <string name="channel_ok">OK</string>
    <string name="channel_cancel">Cancel</string>
    <!--    end add channel-->

    <!-- start  channel dialog  item -->
    <string name="channel_dialog_items_disable">Disable</string>
    <string name="channel_dialog_items_enable">Enable</string>
    <string name="channel_dialog_items_edit">Edit</string>
    <string name="channel_dialog_items_delete">Delete</string>

    <!--    channel index judge start-->
    <string name="ChannelExist">The channel index already exist!</string>
    <string name="ChannelNotNumeric">Channel index must be between 1~999!</string>
    <string name="ChannelIsNull">Channel index can not be null!</string>
    <!--    end-->

    <!--cb language preference -->
    <string name="title_cb_language">Language</string>
    <string name="summary_cb_language">Set languages</string>
    <string name="title_language">Select language</string>
    <string name="language_dialog_cancel">Cancel</string>
    <string name="all_languages">All languages</string>
    <!--end -->

    <!--show phone or message dialog Add BEGIN by gang-chen-->
    <string name="call_phone_att">Call</string>
    <string name="Send_message_att">Send message</string>
    <!--show phone or message dialog Add END by gang-chen-->

    <!--Set dedicated Cell broadcast MI for Israel Programs-->
    <string name="title_channel_mode">Select channels</string>
    <string-array name="pref_cb_channel_mode_entries">
        <item>All channels(0-999)</item>
        <item>My channel(channel list)</item>
    </string-array>
    <string-array name="pref_cb_channel_mode_values">
        <item>0</item>
        <item>1</item>
    </string-array>
    <string name="smscb_channels_title">SMSCB channels</string>

    <string name="enable_alert_audio_title">Audio</string>
    <string name="enable_alert_audio_summary">Audio on alert</string>
<!-- [SOLUTION]-Add-END by TCTNB.(JiangLong Pan)-->

    <!-- set cellbroadcast enable or disable, default value is true-->
    <bool name="def_custome_cell_broadcast_layout">false</bool>
    <string name="add_channel">Add Channel</string>
    <string name="channel_list">Channel List</string>
    <string name="receive_broad_cast">Receive Cell BroadCast SMS</string>
    <string name="cb_channel_dialog_channel_name">Channel name (0-10 characters)</string>
    <string name="cb_channel_dialog_channel_number">Channel number (0-65534)</string>
    <string name="cb_channel_dialog_channel_state">Enable the channel</string>
    <string name="cb_error_channel_num">Please input channel number(0-65534).</string>
    <string name="cb_error_channel_name">The length of Channel name is not correct(1-10characters).</string>
    <string name="cb_menu_tile">Channel Operation</string>
    <string name="cb_menu_add_channel">Add Channel</string>
    <string name="cb_menu_channel_list">Channel list</string>
    <string name="enable">Enable</string>
    <string name="disable">Disable</string>
    <string name="cb_error_channel_id_exist">The channel id exists.</string>
    <string name="cb_channel_dialog_edit_channel">Edit Channel</string>
    <string name="cb_default_new_channel_name">New Channel</string>
    <string name="cb_menu_edit">Edit</string>
    <string name="cb_menu_delete">Delete broadcast</string>
    <string name="cell_broadcast_setting">Cell Broadcast settings</string>
    <string name="error_updating_title">Call settings error</string>
    <string name="reading_settings">Reading settings</string>
    <string name="updating_settings">Updating settings</string>
    <string name="error_finish_dialog_title">Network or SIM card error.</string>
    <string name="close_dialog">OK</string>
    <string name="updating_title">Call settings</string>
    <string name="done_text">Done</string>

    <!-- CellBroadcast channel 50 enabled or not,default value is true -->
    <bool name="def_channel_50_enabled">true</bool>
    <!-- Customize 50 into filter list -->
    <string name="cb_channel_50_name">Channel 50</string>
</resources>
