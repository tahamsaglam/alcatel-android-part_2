/******************************************************************************/
/*                                                               Date:04/2013 */
/*                                PRESENTATION                                */
/*                                                                            */
/*       Copyright 2013 TCL Communication Technology Holdings Limited.       */
/*                                                                            */
/* This material is company confidential, cannot be reproduced in any form   */
/* without the written permission of TCL Communication Technology Holdings   */
/* Limited.                                                                   */
/*                                                                            */
/* -------------------------------------------------------------------------- */
/*  Author :  jianglong.pan                                                   */
/*  Email  :  jianglong.pan@tcl.com                                           */
/*  Role   :                                                                  */
/*  Reference documents :                                                     */
/* -------------------------------------------------------------------------- */
/*  Comments :                                                                */
/*  File     :                                                                */
/*  Labels   :                                                                */
/* -------------------------------------------------------------------------- */
/* ========================================================================== */
/*     Modifications on Features list / Changes Request / Problems Report     */
/* -------------------------------------------------------------------------- */
/*    date   |        author        |         Key          |     comment      */
/* ----------|----------------------|----------------------|----------------- */
/* 04/06/2013|        bo.xu         |      FR-400302       |[SMS]Cell broadc- */
/*           |                      |                      |ast SMS support   */
/* ----------|----------------------|----------------------|----------------- */
/* 04/11/2013|     Dandan.Fang      |       FR400297       |CBC notification  */
/*           |                      |                      |with pop up and   */
/*           |                      |                      |tone alert + vib- */
/*           |                      |                      |rate in CHILE     */
/* ----------|----------------------|----------------------|----------------- */
/* 06/15/2013|        bo.xu         |      CR-451418       |Set dedicated Ce- */
/*           |                      |                      |ll broadcast MI   */
/*           |                      |                      |for Israel Progr- */
/*           |                      |                      |ams               */
/* ----------|----------------------|----------------------|----------------- */
/* 06/27/2013|     Dandan.Fang      |       PR477879       |[CB]Mobile recei- */
/*           |                      |                      |ve CB when disab- */
/*           |                      |                      |le CB             */
/* ----------|----------------------|----------------------|----------------- */
/* 09/06/2013|     yugang.jia       |      FR-516039       |[SMS]Cell broadc- */
/*           |                      |                      |ast SMS support   */
/* ----------|----------------------|----------------------|----------------- */
/* 09/12/2014|      fujun.yang      |        772564        |new CLID variable */
/*           |                      |                      |to re-activate CB */
/*           |                      |                      |for NL            */
/* ----------|----------------------|----------------------|----------------- */
/* 09/22/2014|      tianming.lei    |        793727        |New requirements  */
/*           |                      |                      |for CB Channel    */
/* ----------|----------------------|----------------------|----------------- */
/* 19/07/2016|    jianglong.pan     |SOLUTION-2520283      |Porting CMASS     */
/* ----------|----------------------|----------------------|----------------- */
/******************************************************************************/

package com.android.cellbroadcastreceiver;

import android.app.ActionBar;
import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.os.RemoteException;
import android.preference.ListPreference;
import android.preference.Preference;
import android.preference.Preference.OnPreferenceChangeListener;
import android.preference.PreferenceActivity;
import android.preference.PreferenceCategory;
import android.preference.PreferenceScreen;
import android.telephony.SmsManager;
import android.telephony.SubscriptionInfo;
import android.telephony.SubscriptionManager;
import android.telephony.TelephonyManager;
import android.view.MenuItem;
import android.widget.Toast;

import android.util.Log;

import com.android.cellbroadcastreceiver.CellBroadcast.Channel;
import com.android.internal.telephony.PhoneConstants;
import com.android.internal.telephony.SubscriptionController;
import com.mms.wrap.WrapManager;

import java.util.HashMap;

//Set dedicated Cell broadcast MI for Israel Programs
public class CBMSettingActivity extends PreferenceActivity implements
        Preference.OnPreferenceClickListener, OnPreferenceChangeListener {
    CBSUtills cbu;

    private PreferenceCategory channel_list = null;

    private PreferenceScreen cb_language = null;//[FEATURE]-Del by TCTNB.bo.xu,04/10/2013,FR-400302,delete cb language

    private PreferenceScreen channel_add = null;
    //[BUGFIX]-Del by TCTNB.bo.xu,06/15/2013,CR-451418,
    //Set dedicated Cell broadcast MI for Israel Programs
    //CheckBoxPreference receive_channel = null;

    static final int EDIT_CHANNEL_REQUEST = 0;

    static final int ADD_CHANNEL_REQUEST = 1;

    private static final String LOG_TAG = "CBMSettingActivity";
    /*PR 920761   - SH SW4 Framework - Jerry Zheng - add tag to control debug info */
    static final boolean DEBUG = true;

    String ENABLE = "Enable";

    String DISABLE = "Disable";
    //[BUGFIX]-Add-BEGIN by TCTNB.bo.xu,06/15/2013,CR-451418,
    //Set dedicated Cell broadcast MI for Israel Programs
    private ListPreference mChannelMode = null;
    private static final String PREFS_NAME = "com.android.cellbroadcastreceiver_preferences";
    private String channelMode = "1";
    SharedPreferences settings;
    //[BUGFIX]-Add-END by TCTNB.bo.xu

    private int subDescription = PhoneConstants.SUB1;
    private final String SIM_Language = "content://com.jrd.provider.CellBroadcast/CBLanguage/sub";
    private final String SIM_Channle = "content://com.jrd.provider.CellBroadcast/Channel/sub";
    public static Uri SIMLanguageUri = null;
    public static Uri SIMChannleUri = null;
    //[FEATURE]-Add-BEGIN by TSCD.fujun.yang,09/12/2014,772564,new CLID variable to re-activate CB for NL
    private String READSIMCARDCHANNELACTION = "android.telephony.SmsManager.CBMSettingActivity.ACTION";
    private SimCardChannelReceiver simcardchannelreceiver = null;
    private boolean mEnableSingleSIM = false;
    private TelephonyManager mTelephonyManager;
    private int ranType;

    private class SimCardChannelReceiver extends BroadcastReceiver {
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            Log.i(LOG_TAG, "receiver--readsimcardchannel-done");
            if (READSIMCARDCHANNELACTION.equals(action)) {
                if (null != listener) {
                    try {
                        int num = intent.getIntExtra("num", 0);
                        String cbindex = intent.getStringExtra("cbindex");
                        String cbable = intent.getStringExtra("cbable");
                        if (true == DEBUG)
                            Log.i(LOG_TAG, "num=" + num + " cbindex=" + cbindex + " cbable" + cbable);
                        listener.onFinished(num, cbindex, cbable);
                    } catch (RemoteException e) {
                        // ignore it
                    }
                }
            }
        }
    }

    public void registerSimCardChannelRecevier() {
        Log.i(LOG_TAG, "registerReceiver-simcardchannelrecevier");
        //[FEATURE]-Add-BEGIN by TSCD.fujun.yang,09/15/2014,772564,new CLID variable to re-activate CB for NL
        if (simcardchannelreceiver == null) {
            simcardchannelreceiver = new SimCardChannelReceiver();
        }
        //[FEATURE]-Add-END by TSCD.fujun.yang
        IntentFilter filter = new IntentFilter();
        filter.addAction(READSIMCARDCHANNELACTION);
        registerReceiver(simcardchannelreceiver, filter);
    }

    //[FEATURE]-Add-END by TSCD.fujun.yang
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // Load the preferences from an XML resource
        addPreferencesFromResource(R.xml.cbsetting_preference);
        mEnableSingleSIM = getResources().getBoolean(R.bool.def_cellbroadcastreceiver_enable_single_sim);
        Log.i(LOG_TAG, "mEnableSingleSIM" + mEnableSingleSIM);

        if (TelephonyManager.getDefault().isMultiSimEnabled()) {
            Intent intent = getIntent();
            subDescription = intent.getIntExtra(PhoneConstants.SUBSCRIPTION_KEY, PhoneConstants.SUB1);
            if (mEnableSingleSIM) {
                subDescription = PhoneConstants.SUB1;
            }
            Log.i(LOG_TAG, "multisimcard-subDescription" + subDescription);
            SIMLanguageUri = Uri.parse(SIM_Language + subDescription);
            SIMChannleUri = Uri.parse(SIM_Channle + subDescription);
            if (true == DEBUG) {
                Log.i(LOG_TAG, "onCreate-subDescription=" + subDescription);
                Log.i(LOG_TAG, "onCreate-SIMLanguageUri=" + SIMLanguageUri);
                Log.i(LOG_TAG, "-onCreate-SIMChannleUri=" + SIMChannleUri);
            }
        }

        //[BUGFIX]-Mod-BEGIN by TSCD.tianming.lei,09/22/2014,793727
        boolean registReceiver = getResources().getBoolean(R.bool.def_registerSimCardChannelReceiver_on);
        if (registReceiver) {
            //[FEATURE]-Add-BEGIN by TSCD.fujun.yang,09/12/2014,772564,new CLID variable to re-activate CB for NL
            registerSimCardChannelRecevier();
            //[FEATURE]-Add-END by TSCD.fujun.yang
        }
        //[BUGFIX]-Mod-End by TSCD.tianming.lei
        cbu = new CBSUtills(this);
        initClicker();
        initCBLanguage();//[FEATURE]-Del by TCTNB.bo.xu,04/10/2013,FR-400302,delete cb language

        // [BUGFIX]-Add-BEGIN by TCTNB.Dandan.Fang,06/27/2013,PR477879,
        // [CB][mobile receive CB when disable CB
        // initChannelList();
        // [BUGFIX]-Add-END by TCTNB.Dandan.Fang

        // sync db data with sim card cb data
        //[BUGFIX]-Add-BEGIN by TCTNB.bo.xu,06/15/2013,CR-451418,
        //Set dedicated Cell broadcast MI for Israel Programs
        mChannelMode = (ListPreference) findPreference("pref_key_choose_channel");
        //[modify]-begin-by-chaobing.huang-defect1953332
        settings = this.getSharedPreferences(PREFS_NAME, 0);
        channelMode = settings.getString("pref_key_choose_channel", "1");
        if(getResources().getBoolean(R.bool.def_enableLaunchDeviceReset)) {
            mChannelMode.setValue(channelMode);
        }
        updataChannelMode(mChannelMode.getValue());
        mChannelMode.setOnPreferenceChangeListener(this);
        //[modify]-end-by-chaobing.huang-defect1953332

        if (TelephonyManager.getDefault().isMultiSimEnabled()) {
            SharedPreferences.Editor editor = settings.edit();


            if (settings.getString("pref_key_choose_channel_sim2", "FF").equals("FF")) {
                editor.putString("pref_key_choose_channel_sim2", "1");

            }
            if (settings.getString("pref_key_choose_channel_sim1", "FF").equals("FF")) {
                editor.putString("pref_key_choose_channel_sim1", "1");

            }
            editor.commit();
        }

        //[BUGFIX]-Add-END by TCTNB.bo.xu
        //[BUGFIX]-Mod-BEGIN by TCTNB.bo.xu,06/15/2013,CR-451418,
        //Set dedicated Cell broadcast MI for Israel Programs

        SmsManager manager = null;
        if (TelephonyManager.getDefault().isMultiSimEnabled() && subDescription != -1) {
           //[Defect 899771]-mod-BEGIN by TCTSH.gang-chen@tcl.com 11/12/2015
            //bug-fix-BEGIN for select channelMode always pop up simpick dialog add by gang-chen FIXME: 12/13/15
            Log.d(LOG_TAG,"onCreate - subDescription : "+subDescription);
            SubscriptionManager mSubscriptionManager = SubscriptionManager.from(CBMSettingActivity.this);
            SubscriptionInfo mSubInfoRecord = mSubscriptionManager.getActiveSubscriptionInfoForSimSlotIndex(subDescription);
            int subId = SmsManager.getDefaultSmsSubscriptionId();//[Defect1161102]-modify by gang-chen 2015/12/16
            if (mSubInfoRecord != null) {
                subId = mSubInfoRecord.getSubscriptionId();
            }
            manager = SmsManager.getSmsManagerForSubscriptionId(subId);
            //bug-fix-END gang-chen FIXME: 12/13/15
            //[Defect 899771]-mod-END by TCTSH.gang-chen@tcl.com 11/12/2015
			if (subDescription == 0) {
                channelMode = settings.getString("pref_key_choose_channel_sim1", "1");
            } else if (subDescription == 1) {
                channelMode = settings.getString("pref_key_choose_channel_sim2", "1");
            }
            if ("1".equalsIgnoreCase(channelMode)) {
                //[FEATURE]-Add-BEGIN by TSCD.fujun.yang,09/12/2014,772564,new CLID variable to re-activate CB for NL
                //getCBMConfig();
                Log.i(LOG_TAG, "oncreate--read-channel-subDescription=" + subDescription);
                WrapManager.getInstance().getCellBroadcastConfig(manager);
                //[FEATURE]-Add-END by TSCD.fujun.yang
            }
        } else {
            manager = SmsManager.getDefault();
            if ("1".equalsIgnoreCase(channelMode)) {
                //[FEATURE]-Add-BEGIN by TSCD.fujun.yang,09/12/2014,772564,new CLID variable to re-activate CB for NL
                //getCBMConfig();
                Log.i(LOG_TAG, "oncreate--read-channel");
                WrapManager.getInstance().getCellBroadcastConfig(manager);
                //[FEATURE]-Add-END by TSCD.fujun.yang
            }
        }

        //[BUGFIX]-Add-END by TCTNB.bo.xu

        // [BUGFIX]-Add-BEGIN by TCTNB.Dandan.Fang,06/27/2013,PR477879 ,
        // [CB][mobile receive CB when disable CB
        hideMyChannelList();
        // [BUGFIX]-Add-END by TCTNB.Dandan.Fang
        //[FEATURE]-Add-BEGIN by TSCD.fujun.yang,09/12/2014,772564,new CLID variable to re-activate CB for NL
        ActionBar actionBar = getActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        //[FEATURE]-Add-END by TSCD.fujun.yang

        mTelephonyManager = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
        if (mTelephonyManager.getPhoneType() == TelephonyManager.PHONE_TYPE_CDMA) {
            ranType = SmsManager.CELL_BROADCAST_RAN_TYPE_CDMA;
        } else if (mTelephonyManager.getPhoneType() == TelephonyManager.PHONE_TYPE_GSM) {
            ranType = SmsManager.CELL_BROADCAST_RAN_TYPE_GSM;
        }

    }

    //[FEATURE]-Add-BEGIN by TSCD.fujun.yang,09/12/2014,772564,new CLID variable to re-activate CB for NL
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            this.finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    //[FEATURE]-Add-END by TSCD.fujun.yang
    //[FEATURE]-Add-BEGIN by TSCD.fujun.yang,09/15/2014,772564,new CLID variable to re-activate CB for NL
    @Override
    protected void onDestroy() {
        // TODO Auto-generated method stub
        super.onDestroy();
        if (simcardchannelreceiver != null) {
            Log.i(LOG_TAG, "unregisterReceiver-simcardchannelreceiver");
            unregisterReceiver(simcardchannelreceiver);
            simcardchannelreceiver = null;
        }
    }

    //[FEATURE]-Add-END by TSCD.fujun.yang
    void initClicker() {
        channel_add = (PreferenceScreen) findPreference("channel_add");
        channel_add.setOnPreferenceClickListener(this);
//[FEATURE]-Del-BEGIN by TCTNB.bo.xu,04/10/2013,FR-400302,delete cb language
        cb_language = (PreferenceScreen) findPreference("cb_language");
        cb_language.setOnPreferenceClickListener(this);
//[FEATURE]-Del-END by TCTNB.bo.xu
        //[BUGFIX]-Del by TCTNB.bo.xu,06/15/2013,CR-451418,
        //Set dedicated Cell broadcast MI for Israel Programs
        //receive_channel = (CheckBoxPreference) findPreference("receive_channel");
        //receive_channel.setOnPreferenceClickListener(this);
    }

    //[FEATURE]-Del-BEGIN by TCTNB.bo.xu,04/10/2013,FR-400302,delete cb language
    private void initCBLanguage() {
        int k;
        if (TelephonyManager.getDefault().isMultiSimEnabled() && (subDescription != -1L)) {
            k = cbu.queryCBLanguage(subDescription);
        } else {
            k = cbu.queryCBLanguage();
        }
        log("cbLanguage is : " + k);
        // String s =
        // "German,English,Italian,French,Spanish,Dutch,Swedish,Danish,Portuguese,Finnish,Norwegian,Greek,Turkish,Hungarian,Polish,All languages";
        // String[] sa = s.split(",");

        String[] sa = getResources().getStringArray(R.array.cb_language_items);
        String lan;
        lan = sa[k];
        cb_language.setSummary(lan);
    }

    //[FEATURE]-Del-END by TCTNB.bo.xu
    private void initChannelList() {
        channel_list = (PreferenceCategory) findPreference("channel_list");
        channel_list.removeAll();
        Cursor c = null;
        if (TelephonyManager.getDefault().isMultiSimEnabled() && (subDescription != -1L)) {
            c = cbu.queryChannel(subDescription);
        } else {
            c = cbu.queryChannel();
        }
        if (true == DEBUG) Log.i(LOG_TAG, "initChanneList--enter");
        if (c == null) {
            Log.i(LOG_TAG, "initChanneList--return");
            return;
        }
        String channelName = "";
        String channelEnable = "";
        String channelId = "";
        String channelIndex = "";

        //[BUGFIX]-Add-BEGIN by TCTNB.meng.tong,12/29/2012,353549,
        //Cell broadcast channel list error
        boolean selected = false;
        boolean isSelectAll = true;
        //[BUGFIX]-Add-END by TCTNB.meng.tong

        if (c.getCount() > 0) {
            c.moveToFirst();
            while (!c.isAfterLast()) {
                channelName = "";
                channelEnable = "";
                //[BUGFIX]-Mod-BEGIN by TCTNB.YuTao.Yang,12/26/2012,380033,Modify preference summary language display
                String openStatus = "";
                channelId = "";
                channelIndex = "";
                Preference channelPref = new Preference(this);
                channelName = c.getString(c.getColumnIndex(Channel.NAME));
                channelId = c.getString(c.getColumnIndex(Channel._ID));
                channelIndex = c.getString(c.getColumnIndex(Channel.INDEX));
                channelEnable = c.getString(c.getColumnIndex(Channel.Enable));

                channelPref.setKey(channelId + "");// channel index
                channelPref.setTitle(channelName + "(" + channelIndex + ")");// channel
                // name(index)

                if (channelEnable.equalsIgnoreCase(ENABLE)) {
                    openStatus = getString(R.string.enable).toString();

                    //[BUGFIX]-Add-BEGIN by TCTNB.meng.tong,12/29/2012,353549,
                    //Cell broadcast channel list error
                    selected = true;
                    //[BUGFIX]-Add-END by TCTNB.meng.tong

                } else if (channelEnable.equalsIgnoreCase(DISABLE)) {
                    openStatus = getString(R.string.disable).toString();

                    //[BUGFIX]-Add-BEGIN by TCTNB.meng.tong,12/29/2012,353549,
                    //Cell broadcast channel list error
                    selected = false;
                    //[BUGFIX]-Add-END by TCTNB.meng.tong

                }

                //[BUGFIX]-Add-BEGIN by TCTNB.meng.tong,12/29/2012,353549,
                //Cell broadcast channel list error
                isSelectAll = isSelectAll & selected;
                //[BUGFIX]-Del by TCTNB.bo.xu,06/15/2013,CR-451418,
                //Set dedicated Cell broadcast MI for Israel Programs
                //receive_channel.setChecked(isSelectAll);
                //[BUGFIX]-Add-END by TCTNB.meng.tong

                channelPref.setSummary(openStatus);// channel enable/disable
                //[BUGFIX]-Mod-END by TCTNB.YuTao.Yang

                channelPref.setOnPreferenceClickListener(this);
                //[FEATURE]-Add-BEGIN by TCTNB.ye.shen,12/12/2012,FR-313552,
                //Cell broadcast SMS support
                Boolean isCmas = false;
                if (channelIndex.length() == 4) {//check for CMAS
                    int channelIndexNumber = Integer.parseInt(channelIndex);
                    if (channelIndexNumber >= 4370 && channelIndexNumber <= 4381)
                        isCmas = true;
                }
                if (!isCmas) {
                    //[FEATURE]-Add-END by TCTNB.ye.shen

                    // [BUGFIX]-Add-BEGIN by TCTNB.Dandan.Fang,06/27/2013,PR477879 ,
                    // [CB][mobile receive CB when disable CB
                    // channel_list.addPreference(channelPref);
                    if (TelephonyManager.getDefault().isMultiSimEnabled()) {
                        if (subDescription == 0) {
                            channelMode = settings.getString("pref_key_choose_channel_sim1", "1");
                        } else if (subDescription == 1) {
                            channelMode = settings.getString("pref_key_choose_channel_sim2", "1");
                        }
                    } else {
                        channelMode = settings.getString("pref_key_choose_channel", "1");
                    }
                }
                if (true == DEBUG) Log.i(LOG_TAG, "channelMode=" + channelMode);
                if ("1".equalsIgnoreCase(channelMode)) {
                    Log.d(LOG_TAG,"channel mode is my channel list,display all default channel and channel added by user");
                    // channel mode is "my channel list", display all default channel and channel added by user
                    if (!isCmas) {
                        channel_list.addPreference(channelPref);
                    }
                } else {
                    // channel mode is "0-999", display default channel which index >1000
                    if (channelIndex.length() == 4 && !isCmas) {
                        channel_list.addPreference(channelPref);
                    }
                }
                // [BUGFIX]-Add-END by TCTNB.Dandan.Fang

                c.moveToNext();
            }
        }
        c.close();

    }

    Preference preferenceModify;

    String channelIdModify = "";

    AlertDialog ad;

    public boolean onPreferenceClick(Preference preference) {
        if (preference == channel_add) {
            Intent in = new Intent(CBMSettingActivity.this, ChannelSetActivity.class);
            if (TelephonyManager.getDefault().isMultiSimEnabled() && (subDescription != -1L)) {
                in.putExtra("channeladd", true);
                in.putExtra(PhoneConstants.SUBSCRIPTION_KEY, subDescription);
            }
            startActivityForResult(in, ADD_CHANNEL_REQUEST);
//[FEATURE]-Del-BEGIN by TCTNB.bo.xu,04/10/2013,FR-400302,delete cb language
        } else if (preference == cb_language) {
            int k;
            if (TelephonyManager.getDefault().isMultiSimEnabled() && (subDescription != -1L)) {
                k = cbu.queryCBLanguage(subDescription);
            } else {
                k = cbu.queryCBLanguage();
            }
            ad = new AlertDialog.Builder(CBMSettingActivity.this)
                    .setTitle(R.string.title_language)
                    .setSingleChoiceItems(R.array.cb_language_items, k,
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int whichButton) {
                                    // save value to language db
                                    if (TelephonyManager.getDefault().isMultiSimEnabled() && (subDescription != -1L)) {
                                        cbu.saveCBLanguage(whichButton + "", subDescription);
                                    } else {
                                        cbu.saveCBLanguage(whichButton + "");
                                    }
                                    initCBLanguage();
                                    ad.dismiss();
                                }
                            })
                    .setNegativeButton(R.string.language_dialog_cancel,
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int whichButton) {
                                    ad.dismiss();
                                    /* User clicked No so do some stuff */
                                }
                            }).create();
            ad.show();
//[FEATURE]-Del-END by TCTNB.bo.xu
            //[BUGFIX]-Del-BEGIN by TCTNB.bo.xu,06/15/2013,CR-451418,
            //Set dedicated Cell broadcast MI for Israel Programs
        } /*else if (preference == receive_channel) {
            // update channel db enable state
            ContentValues values = new ContentValues();
            values.put(Channel.Enable, receive_channel.isChecked() ? ENABLE : DISABLE);
            cbu.updateChannel(values);
            initChannelList();
            // enable : set channel list enable ,can select some channel
            // disable : set channel list disable,can select some channel
            // set channel to phone
            setCBMConfig();
        //[BUGFIX]-Del-END by TCTNB.bo.xu
        }*/ else if (preference == mChannelMode) {
        } else {
            log("onPreferenceClick come in");
            final String channelId = preference.getKey();
            String channelNameTemp = "";
            Cursor c = null;
            if (TelephonyManager.getDefault().isMultiSimEnabled() && (subDescription != -1L)) {
                c = cbu.queryChannel(subDescription);
            } else {
                c = cbu.queryChannel();
            }
            if (c != null && c.getCount() > 0) {
                c.moveToFirst();
                while (!c.isAfterLast()) {
                    if (channelId.equalsIgnoreCase(c.getString(c.getColumnIndex(Channel._ID)))) {
                        channelNameTemp = c.getString(c.getColumnIndex(Channel.Enable));
                        break;
                    } else {
                        c.moveToNext();
                    }
                }
            }
            final String channelName = channelNameTemp;
            if (c != null) {
                c.close();
            }
            //[BUGFIX]-Add-END by TCTNB.YuTao.Yang

            final Preference fpreference = preference;
            final int channel_dialog_items;

            //[FEATURE]-Add-BEGIN by TCTNB.Dandan.Fang,04/11/2013,FR400297,
            //CBC notification with pop up and tone alert + vibrate in CHILE
            //SMSCB channels should not be customizable or editable by the end user.
            if (cbu == null) {
                cbu = new CBSUtills(CBMSettingActivity.this);
            }
            if (TelephonyManager.getDefault().isMultiSimEnabled() && (subDescription != -1L)) {
                if (cbu.isForbiddenToModifyPredefinedChannels(channelId, subDescription)) {
                    log("Predefined channels cannot be modified.");
                    Toast.makeText(this, "Predefined channels cannot be modified.", Toast.LENGTH_LONG).show();
                    return false;
                }
            } else {
                if (cbu.isForbiddenToModifyPredefinedChannels(channelId)) {
                    log("Predefined channels cannot be modified.");
                    Toast.makeText(this, "Predefined channels cannot be modified.", Toast.LENGTH_LONG).show();
                    return false;
                }
            }

            //[FEATURE]-Add-END by TCTNB.Dandan.Fang

            if (channelName.equalsIgnoreCase(ENABLE)) {
                channel_dialog_items = R.array.channel_dialog_items_disable;
            } else {
                channel_dialog_items = R.array.channel_dialog_items_enable;
            }

            AlertDialog ad = new AlertDialog.Builder(CBMSettingActivity.this)
                    .setTitle(preference.getTitle())
                    .setItems(channel_dialog_items, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            log(which + "");
                            // Toast.makeText(CBMSettingActivity.this,which+"",0).show();
                            switch (which) {
                                case 0: {
                                    // disable/enable
                                    String changeEnable = channelName.equalsIgnoreCase(ENABLE) ? DISABLE
                                            : ENABLE;
                                    Log.i(LOG_TAG, "onclick---disable/enable");
                                    //[BUGFIX]-Mod-BEGIN by TCTNB.YuTao.Yang,12/26/2012,380033,Modify preference summary language display
                                    String openStatus = "";
                                    if (channelName.equalsIgnoreCase(ENABLE)) {
                                        openStatus = getString(R.string.disable).toString();
                                    } else if (channelName.equalsIgnoreCase(DISABLE)) {
                                        openStatus = getString(R.string.enable).toString();
                                    }

                                    // set channel disable/enable into db
                                    ContentValues values = new ContentValues();
                                    values.put(CellBroadcast.Channel.Enable, changeEnable);
                                    if (TelephonyManager.getDefault().isMultiSimEnabled() && (subDescription != -1L)) {
                                        cbu.updateChannel(channelId, values, subDescription);
                                    } else {
                                        cbu.updateChannel(channelId, values);
                                    }
                                    fpreference.setSummary(openStatus);
                                    // set channel disable/enable into phone

                                    initChannelList();
                                    //[BUGFIX]-Mod-END by TCTNB.YuTao.Yang
                                    //[BUGFIX]-Mod-BEGIN by TCTNB.bo.xu,06/15/2013,CR-451418,
                                    //Set dedicated Cell broadcast MI for Israel Programs
                                    if (TelephonyManager.getDefault().isMultiSimEnabled()) {
                                        if (subDescription == 0) {
                                            channelMode = settings.getString("pref_key_choose_channel_sim1", "1");
                                        } else if (subDescription == 1) {
                                            channelMode = settings.getString("pref_key_choose_channel_sim2", "1");
                                        }
                                        if ("1".equalsIgnoreCase(channelMode)) {
                                            setCBMConfig();
                                        }
                                    } else {
                                        if ("1".equalsIgnoreCase(channelMode)) {
                                            setCBMConfig();
                                        }
                                    }
                                    //[BUGFIX]-Mod-END by TCTNB.bo.xu
                                    break;
                                }
                                case 1:
                                    // edit
                                    // dispisAddlay cbsetactivity
                                    Log.i(LOG_TAG, "onclick---edit");
                                    Intent in = new Intent(CBMSettingActivity.this,
                                            ChannelSetActivity.class);
                                    in.putExtra("id", channelId);
                                    if (TelephonyManager.getDefault().isMultiSimEnabled() && (subDescription != -1L)) {
                                        in.putExtra(PhoneConstants.SUBSCRIPTION_KEY, subDescription);
                                    }
                                    channelIdModify = channelId;
                                    preferenceModify = fpreference;
                                    startActivityForResult(in, EDIT_CHANNEL_REQUEST);
                                    break;
                                case 2: {
                                    //[BUGFIX]-Add-BEGIN by TCTNB.Tongyuan.Lv, 03/08/2013, PR-408496,
                                    // fix the normal cb channel missed after set up CMAS
                                    // first disable the channel
                                    Log.i(LOG_TAG, "onclick---delete");
                                    Cursor c = null;
                                    if (TelephonyManager.getDefault().isMultiSimEnabled() && (subDescription != -1L)) {
                                        c = cbu.queryChannel(channelId, subDescription);
                                    } else {
                                        c = cbu.queryChannel(channelId);
                                    }
                                    int index;
                                    if (c != null && c.getCount() > 0) {
                                        c.moveToFirst();
                                        if (TelephonyManager.getDefault().isMultiSimEnabled() && (subDescription != -1L)) {
                                            //bug-fix-BEGIN by gang-chen// FIXME: 12/13/15
                                            Log.d(LOG_TAG, "case 2 -subDescription : " + subDescription);
                                            SubscriptionManager mSubscriptionManager = SubscriptionManager.from(CBMSettingActivity.this);
                                            int subId = 0;
                                            if (mEnableSingleSIM) {
                                                subDescription = PhoneConstants.SUB1;
                                            }
                                            Log.d(LOG_TAG, "case 2 -subDescription final: " + subDescription);

                                            SubscriptionInfo mSubInfoRecord = mSubscriptionManager.getActiveSubscriptionInfoForSimSlotIndex(subDescription);
                                            if (mSubInfoRecord != null) {
                                                subId = mSubInfoRecord.getSubscriptionId();
                                            }
                                            SmsManager manager = SmsManager.getSmsManagerForSubscriptionId(subId);
                                            // manager = SmsManager.getSmsManagerForSubscriptionId(subDescription);
                                            //bug-fix-END by gang-chen// FIXME: 12/13/15
                                            index = Integer.parseInt(c.getString(c.getColumnIndex(Channel.INDEX)));
                                            //[BUGFIX]-Mod-BEGIN by TCTNB.bo.xu,06/15/2013,CR-451418,
                                            //Set dedicated Cell broadcast MI for Israel Programs
                                            if ("1".equalsIgnoreCase(channelMode)) {

                                                manager.disableCellBroadcastRange(index, index, ranType);
                                            }
                                        } else {
                                            SmsManager manager = SmsManager.getDefault();
                                            index = Integer.parseInt(c.getString(c.getColumnIndex(Channel.INDEX)));
                                            //[BUGFIX]-Mod-BEGIN by TCTNB.bo.xu,06/15/2013,CR-451418,
                                            //Set dedicated Cell broadcast MI for Israel Programs
                                            if ("1".equalsIgnoreCase(channelMode)) {
                                                manager.disableCellBroadcastRange(index, index, ranType);
                                            }
                                        }
                                        //[BUGFIX]-Mod-END by TCTNB.bo.xu
                                    }

                                    if (c != null) {
                                        c.close();
                                    }
                                    //[BUGFIX]-Add-END by TCTNB.Tongyuan.Lv

                                    // delete channel from db
                                    if (TelephonyManager.getDefault().isMultiSimEnabled() && (subDescription != -1L)) {
                                        cbu.deleteChannel(channelId, subDescription);
                                    } else {
                                        cbu.deleteChannel(channelId);
                                    }
                                    channel_list.removePreference(fpreference);
                                    // delete channel from phone
                                    //[BUGFIX]-Mod-BEGIN by TCTNB.bo.xu,06/15/2013,CR-451418,
                                    //Set dedicated Cell broadcast MI for Israel Programs
                                    if ("1".equalsIgnoreCase(channelMode)) {
                                        setCBMConfig();
                                    }
                                    //[BUGFIX]-Mod-END by TCTNB.bo.xu
                                    break;
                                }
                                default:
                                    break;

                            }
                        }
                    }).create();
            ad.show();

        }
        return false;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        log("requestCode:" + requestCode + "\n+resultCode:" + resultCode);
        if (requestCode == EDIT_CHANNEL_REQUEST) {
            if (channelIdModify.equals(""))
                return;
            Cursor c = null;
            if (TelephonyManager.getDefault().isMultiSimEnabled() && (subDescription != -1L)) {
                c = cbu.queryChannel(channelIdModify, subDescription);
            } else {
                c = cbu.queryChannel(channelIdModify);
            }
            if (c != null && c.getCount() > 0) {
                c.moveToFirst();
                String channelName = "";
                String channelEnable = "";
                String channelIndex = "";
                channelName = c.getString(c.getColumnIndex(Channel.NAME));
                channelIndex = c.getString(c.getColumnIndex(Channel.INDEX));

                //[BUGFIX]-Add-BEGIN by TCTNB.YuTao.Yang,12/26/2012,380033,Modify preference summary language display
                String channelEnablefTemp = c.getString(c.getColumnIndex(Channel.Enable));
                // init control text
                if (channelEnablefTemp.equalsIgnoreCase(ENABLE)) {
                    channelEnable = getString(R.string.enable).toString();
                } else {
                    channelEnable = getString(R.string.disable).toString();
                }

                //[BUGFIX]-Add-END by TCTNB.YuTao.Yang

                // init control text
                preferenceModify.setTitle(channelName + "(" + channelIndex + ")");
                preferenceModify.setSummary(channelEnable);
            }
            if (c != null) {
                c.close();
            }
        } else if (resultCode == ADD_CHANNEL_REQUEST) {
            initChannelList();
        }
    }

    boolean isSelectAll = true;

    void setCBMConfig() {
        log("setCbChannel");
        SmsManager manager = null;
        com.android.internal.telephony.gsm.SmsBroadcastConfigInfo[] cbi;
        if (TelephonyManager.getDefault().isMultiSimEnabled() && (subDescription != -1L)) {
            //bug-fix-BEGIN for Simpick dialog awlays pop up when enable/disable channel  by gang-chen// FIXME: 12/13/15
            SubscriptionManager mSubscriptionManager = SubscriptionManager.from(CBMSettingActivity.this);
            int subId = 0;
            if (mEnableSingleSIM) {
                subDescription = PhoneConstants.SUB1;
            }
            Log.d(LOG_TAG, "in setCBMConfig -subDescription : " + subDescription);

            SubscriptionInfo mSubInfoRecord = mSubscriptionManager.getActiveSubscriptionInfoForSimSlotIndex(subDescription);
            if (mSubInfoRecord != null) {
                subId = mSubInfoRecord.getSubscriptionId();
            }
             manager = SmsManager.getSmsManagerForSubscriptionId(subId);
            //bug-fix END by gang-chen// FIXME: 12/13/15

            cbi = cbu.getSmsBroadcastConfigInfo(subDescription);
        } else {
            manager = SmsManager.getDefault();
            cbi = cbu.getSmsBroadcastConfigInfo();
        }
        //[BUGFIX]-Add-BEGIN by TCTNB.Dandan.Fang,06/27/2013,PR477879  ,
        //[CB][mobile receive CB when disable CB
     /*      if(cbi != null){
               int num = cbi.length;
               for (int i=0; i<num;i++) {
                   int index = cbi[i].getFromServiceId();
                   if (cbi[i].isSelected()) {
                       manager.enableCellBroadcastRange(index,index);
                   } else {
                       manager.disableCellBroadcastRange(index,index,0);
                   }
               }
           }*/

        if ("1".equalsIgnoreCase(channelMode)) {
            Log.d(LOG_TAG,"channel mode is my channel list");
            // channelMode is "MyChannelList"
            if (cbi != null) {
                int num = cbi.length;
                for (int i = 0; i < num; i++) {
                    int index = cbi[i].getFromServiceId();
                    Log.i(LOG_TAG, "setCBMConfig-MyChannelList-index=" + index);
                    if (cbi[i].isSelected()) {
                        if (TelephonyManager.getDefault().isMultiSimEnabled() && (subDescription != -1L)) {
                            manager.enableCellBroadcastRange(index, index, ranType);
                        } else {
                            manager.enableCellBroadcastRange(index, index, ranType);
                        }
                    } else {
                        if (TelephonyManager.getDefault().isMultiSimEnabled() && (subDescription != -1L)) {
                            manager.disableCellBroadcastRange(index, index, ranType);
                        } else {
                            manager.disableCellBroadcastRange(index, index, ranType);
                        }
                    }
                }
            }
        } else {
            Log.d(LOG_TAG,"channelMode is 0-999, we just need to enabled channel which index >1000");
            //channelMode is "0-999", we just need to enabled channel which index >1000
            if (cbi != null) {
                int num = cbi.length;
                for (int i = 0; i < num; i++) {
                    int index = cbi[i].getFromServiceId();
                    Log.i(LOG_TAG, "setCBMConfig-0-999-index=" + index);
                    if (index >= 1000) {
                        if (cbi[i].isSelected()) {
                            if (TelephonyManager.getDefault().isMultiSimEnabled() && (subDescription != -1L)) {
                                manager.enableCellBroadcastRange(index, index, ranType);
                            } else {
                                manager.enableCellBroadcastRange(index, index, ranType);
                            }
                        } else {
                            if (TelephonyManager.getDefault().isMultiSimEnabled() && (subDescription != -1L)) {
                                manager.disableCellBroadcastRange(index, index, ranType);
                            } else {
                                manager.disableCellBroadcastRange(index, index, ranType);
                            }
                        }
                    }
                }
            }
        }
        //[BUGFIX]-Add-END by TCTNB.Dandan.Fang

    }

    HashMap getIndexName() {
        Cursor c = null;
        if (TelephonyManager.getDefault().isMultiSimEnabled() && (subDescription != -1L)) {
            c = cbu.queryChannel(subDescription);
        } else {
            c = cbu.queryChannel();
        }
        String channelName = "";
        String channelIndex = "";
        HashMap IndexName = new HashMap<String, String>();
        if (c != null && c.getCount() > 0) {
            c.moveToFirst();
            while (!c.isAfterLast()) {
                channelName = "";
                channelIndex = "";
                channelName = c.getString(c.getColumnIndex(Channel.NAME));
                channelIndex = c.getString(c.getColumnIndex(Channel.INDEX));
                if (!IndexName.containsKey(channelIndex)) {
                    IndexName.put(channelIndex, channelName);
                }
                c.moveToNext();
            }
        }
        if (c != null) {
            c.close();
        }
        return IndexName;
    }

    void log(String mes) {
        if (true == DEBUG)
            Log.d(LOG_TAG, mes + "\n");
    }

    private String index = null;

    private IListener.Stub listener = new IListener.Stub() {

        @Override
        public void onFinished(int num, String a, String b) throws RemoteException {
            if (true == DEBUG) Log.i(LOG_TAG, "listener");
            HashMap indexName = getIndexName();
            String[] index = a.split("\\;");
            String[] cbable = b.split("\\;");
            ContentValues values = new ContentValues();
            for (int i = 0; i < num; i++) {
                //[BUGFIX]-Mod-BEGIN by TCTNB.Tongyuan.Lv, 03/08/2013, PR-408496,
                //fix the normal cb channel missed after set up CMAS
                int indexId = -1;
                try {
                    indexId = Integer.parseInt(index[i]);
                } catch (NumberFormatException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }

                // insert into
                if (!((indexId >= com.android.internal.telephony.gsm.SmsCbConstants.MESSAGE_ID_CMAS_FIRST_IDENTIFIER)
                        && (indexId <= com.android.internal.telephony.gsm.SmsCbConstants.MESSAGE_ID_CMAS_LAST_IDENTIFIER))) {
                    boolean selected = "enable".equals(cbable[i]) ? true : false;
                    isSelectAll = isSelectAll & selected;
                    values.put(CellBroadcast.Channel.NAME,
                            indexName.containsKey(index[i].trim()) ? indexName.get(index[i])
                                    + "" : "");
                    values.put(CellBroadcast.Channel.INDEX, index[i]);
                    values.put(CellBroadcast.Channel.Enable, selected ? ENABLE : DISABLE);

                    if (!indexName.containsKey(index[i].trim())) {
                        log("add new channel from modem: " + index[i]);
                        if (TelephonyManager.getDefault().isMultiSimEnabled() && (subDescription != -1L)) {
                            cbu.addChannel(values, subDescription);
                        } else {
                            cbu.addChannel(values);
                        }
                    } else {
                        log("update old channel from modem: " + index[i]);
                        if (TelephonyManager.getDefault().isMultiSimEnabled() && (subDescription != -1L)) {
                            if (true == DEBUG)
                                Log.i(LOG_TAG, "Channel_CONTENT_URI=" + Uri.withAppendedPath(Channel.CONTENT_URI, "sub" + subDescription));
                            getContentResolver().update(Uri.withAppendedPath(Channel.CONTENT_URI, "sub" + subDescription), values,
                                    CellBroadcast.Channel.INDEX + "='" + index[i] + "'", null);
                        } else
                            getContentResolver().update(Channel.CONTENT_URI, values,
                                    CellBroadcast.Channel.INDEX + "='" + index[i] + "'", null);
                    }
                    //[BUGFIX]-Mod-END by TCTNB.Tongyuan.Lv
                    values.clear();
                }
            }
            //[BUGFIX]-Del by TCTNB.bo.xu,06/15/2013,CR-451418,
            //Set dedicated Cell broadcast MI for Israel Programs
            initChannelList();
        }
    };

    //[BUGFIX]-Add-BEGIN by TCTNB.bo.xu,06/15/2013,CR-451418,
    //Set dedicated Cell broadcast MI for Israel Programs
    public boolean onPreferenceChange(Preference preference, Object newValue) {
        boolean result = false;
        if (preference == mChannelMode) {
            updataChannelMode(newValue);
            SharedPreferences.Editor editor = settings.edit();
            if (TelephonyManager.getDefault().isMultiSimEnabled()) {
                if (subDescription == 0) {
                    editor.putString("pref_key_choose_channel_sim1", (String) newValue);
                } else if (subDescription == 1) {
                    editor.putString("pref_key_choose_channel_sim2", (String) newValue);
                }
            } else {
                editor.putString("pref_key_choose_channel", (String) newValue);
            }
            editor.commit();
            SmsManager manager = null;
            if (TelephonyManager.getDefault().isMultiSimEnabled()) {
                //bug-fix-BEGIN for select channelMode always pop up simpick dialog add by gang-chen FIXME: 12/13/15
                if(mEnableSingleSIM){
                    subDescription = PhoneConstants.SUB1;
                }
                SubscriptionManager mSubscriptionManager = SubscriptionManager.from(CBMSettingActivity.this);
                SubscriptionInfo mSubInfoRecord = mSubscriptionManager.getActiveSubscriptionInfoForSimSlotIndex(subDescription);
                int subId = PhoneConstants.SUB1;
                if (mSubInfoRecord != null) {
                    subId = mSubInfoRecord.getSubscriptionId();
                }
                manager = SmsManager.getSmsManagerForSubscriptionId(subId);
                //bug-fix-END by gang-chen FIXME: 12/13/15
                if (subDescription == 0) {
                    channelMode = settings.getString("pref_key_choose_channel_sim1", "1");
                } else if (subDescription == 1) {
                    channelMode = settings.getString("pref_key_choose_channel_sim2", "1");
                }
            } else {
                channelMode = settings.getString("pref_key_choose_channel", "1");
                manager = SmsManager.getDefault();
            }
            // [BUGFIX]-Add-BEGIN by TCTNB.Dandan.Fang,06/27/2013,PR477879 ,
            // [CB][mobile receive CB when disable CB
            hideMyChannelList();
            // [BUGFIX]-Add-END by TCTNB.Dandan.Fang
            if ("0".equalsIgnoreCase(((String) newValue))) {
                // [BUGFIX]-Add-BEGIN by TCTNB.Dandan.Fang,07/06/2013,PR477879 ,
                // [CB][mobile receive CB when disable CB
                // if channel mode changes to 0-999, we should disabled the channel added by user firstly,
                // then enable channel 0-999
                com.android.internal.telephony.gsm.SmsBroadcastConfigInfo[] cbi;
                if (TelephonyManager.getDefault().isMultiSimEnabled() && (subDescription != -1L)) {
                    cbi = cbu.getSmsBroadcastConfigInfo(subDescription);
                } else {
                    cbi = cbu.getSmsBroadcastConfigInfo();
                }
                if (cbi != null) {
                    int num = cbi.length;
                    for (int i = 0; i < num; i++) {
                        int index = cbi[i].getFromServiceId();
                        Log.i(LOG_TAG, "index=" + index);
                        if (index < 1000) {
                            if (TelephonyManager.getDefault().isMultiSimEnabled() && (subDescription != -1L)) {
                                manager.disableCellBroadcastRange(index, index, ranType);
                            } else {
                                manager.disableCellBroadcastRange(index, index, ranType);
                            }
                        }
                    }
                }
                // [BUGFIX]-Add-END by TCTNB.Dandan.Fang
                if (TelephonyManager.getDefault().isMultiSimEnabled() && (subDescription != -1L)) {
                    manager.enableCellBroadcastRange(0, 999, ranType);
                } else {
                    manager.enableCellBroadcastRange(0, 999, ranType);
                }
            } else {
                if (TelephonyManager.getDefault().isMultiSimEnabled() && (subDescription != -1L)) {
                    manager.disableCellBroadcastRange(0, 999, ranType);
                } else {
                    manager.disableCellBroadcastRange(0, 999, ranType);
                }
                // [BUGFIX]-Add-BEGIN by TCTNB.Dandan.Fang,06/27/2013,PR477879 ,
                // [CB][mobile receive CB when disable CB
                // setCBMConfig();
                // [BUGFIX]-Add-END by TCTNB.Dandan.Fang
            }
            // [BUGFIX]-Add-BEGIN by TCTNB.Dandan.Fang,06/27/2013,PR477879 ,
            // [CB][mobile receive CB when disable CB
            setCBMConfig();
            // [BUGFIX]-Add-END by TCTNB.Dandan.Fang
            result = true;
        }
        return result;
    }

    private void updataChannelMode(Object value) {
        CharSequence[] summaries = getResources().getTextArray(R.array.pref_cb_channel_mode_entries);
        CharSequence[] values = mChannelMode.getEntryValues();
        for (int i = 0; i < values.length; i++) {
            if (values[i].equals(value)) {
                mChannelMode.setSummary(summaries[i]);
                break;
            }
        }
    }
    //[BUGFIX]-Add-END by TCTNB.bo.xu

    // [BUGFIX]-Add-BEGIN by TCTNB.Dandan.Fang,06/27/2013,PR477879 ,
    // [CB][mobile receive CB when disable CB
    private void hideMyChannelList() {
        initChannelList();
        if ("0".equalsIgnoreCase(channelMode)) {
            // channel mode is (0-999), hide my channel list, diabled "add channel " preference
            channel_add.setEnabled(false);
        } else {
            // channel mode is (my channel list), display my channel list
            channel_add.setEnabled(true);
        }
    }
    // [BUGFIX]-Add-END by TCTNB.Dandan.Fang
}
