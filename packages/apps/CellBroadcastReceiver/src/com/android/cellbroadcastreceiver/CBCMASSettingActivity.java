/*
 * Copyright (C) 2011 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */

package com.android.cellbroadcastreceiver;

import com.android.internal.telephony.PhoneConstants;

import android.os.Bundle;
import android.preference.CheckBoxPreference;
import android.preference.Preference;
import android.preference.PreferenceActivity;
import android.telephony.SubscriptionManager;
import android.telephony.TelephonyManager;
import android.content.Intent;
import android.content.ActivityNotFoundException;

public class CBCMASSettingActivity extends PreferenceActivity {

    private final String TAG = "CBCMASSettingActivity";
    public static final String KEY_ENABLE_CB_FUNCTION = "key_enable_sms_cb";
    public static final String KEY_NORMAL_CB_SETTING = "key_cb_setting";
    public static final String KEY_CMAS_SETTING = "key_cmas_setting";
    private CheckBoxPreference mCBEnable;
    private Preference mNormalCBSetting;
    private Preference mCMASSetting;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        addPreferencesFromResource(R.xml.cbcmassetting_preferences);
        mCBEnable = (CheckBoxPreference)this.findPreference(KEY_ENABLE_CB_FUNCTION);
        if(mCBEnable != null){
            mCBEnable.setOnPreferenceChangeListener(onPreferenceChangeListener);
        }

        mNormalCBSetting = (Preference)this.findPreference(KEY_NORMAL_CB_SETTING);
        if(mNormalCBSetting != null){
            mNormalCBSetting.setOnPreferenceClickListener(onPreferenceClickListener);
        }
        mCMASSetting = (Preference)this.findPreference(KEY_CMAS_SETTING);
        if(mCMASSetting != null){
            mCMASSetting.setOnPreferenceClickListener(onPreferenceClickListener);
        }
    }

    private void sendCBSetIntent(boolean start) {
        String ActionString = "com.android.cellbroadcastreceiver.setstartup";
        int sc_sim_id = PhoneConstants.SUB1;
        sc_sim_id = SubscriptionManager.getDefaultSmsSubscriptionId();
        Intent intent = new Intent(ActionString);
        intent.putExtra("startup", start);
        intent.putExtra(PhoneConstants.SUBSCRIPTION_KEY, sc_sim_id);
        this.sendBroadcast(intent);
    }

    Preference.OnPreferenceClickListener onPreferenceClickListener = new Preference.OnPreferenceClickListener() {
        @Override
        public boolean onPreferenceClick(Preference preference) {
            // TODO Auto-generated method stub
            if(preference.getKey().equals(KEY_NORMAL_CB_SETTING)){
                try{
                    Intent intent = new Intent(CBCMASSettingActivity.this, CBMSettingActivity.class);
                    startActivity(intent);
                } catch (final ActivityNotFoundException e) {
                    e.printStackTrace();
                }
            }else if(preference.getKey().equals(KEY_CMAS_SETTING)){
                try {
                    Intent intent = new Intent(CBCMASSettingActivity.this, CellBroadcastSettings.class);
                    startActivity(intent);
                }catch (final ActivityNotFoundException e) {
                    e.printStackTrace();
                }
            }else{
                android.util.Log.i(TAG,"wrong Preference click");
            }
            return true;
        }
    };

    Preference.OnPreferenceChangeListener onPreferenceChangeListener = new Preference.OnPreferenceChangeListener(){
        public boolean onPreferenceChange(Preference preference, Object newValue){
            boolean isChecked = ((Boolean) newValue).booleanValue();
            if(preference.getKey().equals(KEY_ENABLE_CB_FUNCTION)){
                if(isChecked){
                    sendCBSetIntent(true);
                }else{
                    sendCBSetIntent(false);
                }
            }else{
                android.util.Log.i(TAG,"wrong Preference change");
            }
            return true;
        }
    };

}
