package com.tct.android.tools.feature.app;

import static com.tct.android.tools.feature.app.Main.LOG;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.HashMap;

import com.tct.android.tools.feature.plf2fq.FqJava;
import com.tct.android.tools.feature.plf2fq.Perso;
import com.tct.android.tools.feature.plf2fq.PlfParser;
import com.tct.android.tools.feature.plf2fq.SplfParser;
import com.tct.utils.AndroidPath;

/**
 * Commands Line: tct_feature plf2fq 
 * -1-make Commands: 
 * -2-Input File: ./vender/tct/source/feature_query/plf2fq.xml 
 * -3-Output Dir: ./out/target/common/obj/JAVA_LIBRARIES/tct.feature_query_intermediates/src/
 * -4-Product: eg. Srcibe5 Vipper ... 
 * (change to) 
 * Commands Line: tct_feature plf2fq 
 * -1-make Commands: 
 * -2-Input File: $(LOCAL_PATH) (e.g. vender/tct/source/feature_query) 
 * -3-Output Dir: $(OUTPATH) (e.g. out/target/product/$(TARGET_PRODUCT)/tct_intermediates/feature/$(LOCAL_MODULE) 
 * -4-Product: eg. Srcibe5 Vipper ...
 */

public class Plf2fq implements Main.Command {
	private static final String PLF_COMMON_DIR = "device/tct/common/perso/plf";
	private static final String PLF2FQ_FILE = "plf2fq.xml";
	private static final String MK_FILE = "plf2fq.mk";

	private String mRelativeInputDir = null; // "like: ../../"

	private boolean mIsMakePerso;
	private String mInputDir = null;
	private String mOutDir = null;
	private String mProduct = null;

	public int action(String[] args) {
		if (args.length == 5) {
			LOG.i(args[1] + args[2] + args[3] + args[4]);
			mIsMakePerso = args[1].contains("perso");
			mInputDir = AndroidPath.plainPath(args[2]);
			mRelativeInputDir = AndroidPath.getRelativeDir(mInputDir);
			mOutDir = AndroidPath.plainPath(args[3]);
			mProduct = args[4].trim();
			plf2fq();
		} else {
			LOG.e("COMMAND ERROR: args.LEN=" + args.length);
		}
		return 0;
	}

	private void plf2fq() {
		final FqJava fqj = FqJava.create(mInputDir + "/" + PLF2FQ_FILE);
		fqj.init(new FqJava.Listener() {
			public void onNewPlf(String module, String path) {
				if (path.contains(PLF_COMMON_DIR)) {
					Plfs plfs = new Plfs();
					plfs.newModule = module;
					plfs.init(new File(path), mProduct);
					HashMap<String, FqJava.Field> fields = new HashMap<String, FqJava.Field>();
					if (plfs.plf != null) {
						PlfParser.parse(plfs.plf.getPath(), fields);
					}
					if (plfs.xplf != null) {
						PlfParser.parse(plfs.xplf.getPath(), fields);
					}
					if (!mIsMakePerso && plfs.splf != null) {
						SplfParser.parse(plfs.splf.getPath(), fields);
					}
					for (FqJava.Field f : fields.values()) {
						f.type = Perso.convertType(f.type);
						fqj.updateField(module, f);
					}
				}
			}
		});
		fqj.genJava(mOutDir + "/src");
		writeMkFile();
	}
	
	private void writeMkFile() {
		FileWriter fw = null;
		try {
			fw = new FileWriter(new File(mOutDir + "/" + MK_FILE));
			fw.write("LOCAL_SRC_FILES += " + mRelativeInputDir + mOutDir + "/src/com/tct/feature/PLF.java"); 
		} catch (IOException e) {
		} finally {
			try {
				fw.close();
			} catch (IOException e) {
			}
		}
	}

	static class Plfs {
		File plf = null;
		File xplf = null;
		File splf = null;
		String plfModuleString = null;
		String newModule = null;

		boolean init(File plfDir, String product) {
			if (!plfDir.exists() || !plfDir.isDirectory()) {
				LOG.e("PlfDir is NOT exits. DIR=" + plfDir.getPath());
				return false;
			}
			File[] files = plfDir.listFiles();
			if (files == null || files.length == 0) {
				LOG.e("NO plf or xplf");
				return false;
			}
			for (File f : files) {
				if (f.isFile()) {
					String fName = f.getName();
					if (fName.endsWith(".plf") && fName.startsWith("isdm_")) {
						plfModuleString = fName
								.substring(5, fName.length() - 4);
						plf = f;
					} else if (fName.endsWith(".xplf")
							&& fName.startsWith("isdm_")) {
						plfModuleString = fName
								.substring(5, fName.length() - 5);
						xplf = f;
					}
				}
			}
			if (plf == null && xplf == null) {
				LOG.e("NO plf or xplf");
				return false;
			}
			if (plfDir.getPath().contains(PLF_COMMON_DIR)) {
				String splfpathString = PLF_COMMON_DIR.replace("common",
						product);
				File sFile = new File(plfDir.getPath().replace(PLF_COMMON_DIR,
						splfpathString)
						+ "/isdm_" + plfModuleString + ".splf");
				if (sFile.exists()) {
					splf = sFile;
				}
			}
			return true;
		}
	}
}
