package com.tct.android.tools.feature.app;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Collection;

import com.tct.android.tools.feature.global.GenGlobalJava;
import com.tct.android.tools.feature.global.GenGlobalKernel;
import com.tct.android.tools.feature.global.GenGlobalLk;
import com.tct.android.tools.feature.global.GenGlobalMk;
import com.tct.android.tools.feature.global.GenGlobalNative;
import com.tct.android.tools.feature.global.GlobalConf;
import com.tct.android.tools.feature.global.GlobalConf.Section;
import com.tct.utils.AndroidPath;


//Command Line: tct_feature global $(OUTDIR) $(TARGET_PRODUCT)   EXCEPT JAVA
//Command Line: tct_feature global $(LOCAL_PATH) $(OUTDIR)/$(LOCAL_MODULE) $(TARGET_PRODUCT)   OLNY JAVA

public class Global implements Main.Command {
	private final static String TARGET_CAPACITY_BUILD = "make|native|kernel|lk";
	private final static String TARGET_CAPACITY_FQ = "java";
	private final static String FQ_MK_FILE = "global_fq.mk";
	
	private String fq_local_path = "vender/tct/source/feature_query";
	private String fq_relative_path = AndroidPath.getRelativeDir(fq_local_path);
	private String max_target_capacity = TARGET_CAPACITY_BUILD;
	
	private String mOutDir = null;
	private String mProduct = null;

	public int action(String[] args) {
		Collection<Section> sections = null;
		if (args.length == 3) {
			max_target_capacity = TARGET_CAPACITY_BUILD;
			mOutDir = AndroidPath.plainPath(args[1]);
			mProduct = args[2].trim();
		} else if (args.length == 4) {
			max_target_capacity = TARGET_CAPACITY_FQ;
			fq_local_path = AndroidPath.plainPath(args[1]);
			fq_relative_path = AndroidPath.getRelativeDir(fq_local_path);
			mOutDir = AndroidPath.plainPath(args[2]);
			mProduct = args[3].trim();
		} else {
			return ACT_STATUS_INVALID_PARAM;
		}
		GlobalConf gc = new GlobalConf(mProduct);
		if (gc.parse()) {
			sections = gc.getSections();
		}
		if (!sections.isEmpty()) {
		    processSections(sections);
		}
		return ACT_STATUS_OK;
	}
	
	private void processSections(Collection<Section> sections) {
		if (TARGET_CAPACITY_FQ.equals(max_target_capacity)) {
			processJava(sections);
			FileWriter fw = null;
			try {
				fw = new FileWriter(new File(mOutDir + "/" + FQ_MK_FILE));
				fw.write("LOCAL_SRC_FILES += " + fq_relative_path + mOutDir + "/src/com/tct/feature/Global.java \n"); 
			} catch (IOException e) {
			} finally {
				try {
					fw.close();
				} catch (IOException e) {
				}
			}
		} else {
			processGlobal(sections);
		}
	}
	
	private void processJava(Collection<Section> sections) {
		GenGlobalJava ggj = new GenGlobalJava(mOutDir);
		for(Section section :sections) {
			if (section.target().contains("java") || section.target().contains("all")) {
				ggj.write(section.finalName(), section.type(), section.value());
			}
		}
		ggj.close();
	}
	
	private void processGlobal(Collection<Section> sections) {
		GenGlobalMk genMK = new GenGlobalMk(mOutDir);
		GenGlobalNative genNative = new GenGlobalNative(mOutDir);
		GenGlobalKernel genKernel = new GenGlobalKernel(mOutDir);
		GenGlobalLk genLk = new GenGlobalLk(mOutDir);
		for(Section section :sections) {
			String target = section.target();
			if (target.contains("all")) {
				target = TARGET_CAPACITY_BUILD + "|" + TARGET_CAPACITY_FQ;
			}
			if (target.contains("make") && max_target_capacity.contains("make")) {
				genMK.write(section);
			}
			if (target.contains("native") && max_target_capacity.contains("native")) {
				genNative.write(section);
			}
			if (target.contains("kernel") && max_target_capacity.contains("kernel")) {
				genKernel.write(section);
			}
			if (target.contains("lk") && max_target_capacity.contains("lk")) {
				genLk.write(section);
			}
		}
		genMK.close();
		genNative.close();
		genKernel.close();
		genLk.close();
	}
}
