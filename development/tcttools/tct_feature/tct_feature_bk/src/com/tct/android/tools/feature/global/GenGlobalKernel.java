package com.tct.android.tools.feature.global;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import com.tct.android.tools.feature.global.GlobalConf.Section;

public class GenGlobalKernel {
	private final static String KERNEL_MK_FILE = "global_kernel.mk";
	private FileWriter fw;
	
	public GenGlobalKernel(String outDir) {
		File file  = new File(outDir);
        if (!file.exists()) {
            file.mkdirs();
        }
        String outputJavaFile = file.getPath() + "/" + KERNEL_MK_FILE;
        try {
            fw = new FileWriter(outputJavaFile);
        }catch (IOException e) {
        }
	}
	
	
	public void write(Section section) {
		try {
			if ("integer".equalsIgnoreCase(section.type())) {
				fw.write("KBUILD_CFLAGS += -D" + section.finalName() + "=" + section.value() + "\n");
			} else if ("boolean".equalsIgnoreCase(section.type())) {
				if ("true".equalsIgnoreCase(section.value())) {
					fw.write("KBUILD_CFLAGS += -D" + section.finalName() + "\n");
				}
			} else if ("string".equalsIgnoreCase(section.type())) {
				fw.write("KBUILD_CFLAGS += -D" + section.finalName() + "=\\\"" + section.value() + "\\\"\n");
			}
		} catch (IOException e) {
		}
	}
	
	public void close(){
		try {
			fw.close();
		} catch (IOException e) {
		}
	}
}
