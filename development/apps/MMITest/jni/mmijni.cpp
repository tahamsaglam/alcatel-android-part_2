/******************************************************************************/
/*                                                               Date:09/2012 */
/*                             PRESENTATION                                   */
/*                                                                            */
/*      Copyright 2014 TCL Communication Technology Holdings Limited.         */
/*                                                                            */
/* This material is company confidential, cannot be reproduced in any form    */
/* without the written permission of TCL Communication Technology Holdings    */
/* Limited.                                                                   */
/*                                                                            */
/* -------------------------------------------------------------------------- */
/* Author:  Shishun Liu                                                       */
/* E-Mail:  Shishun.Liu@tcl.com                                               */
/* Role  :  MMITest                                                           */
/* Reference documents :                                                      */
/* -------------------------------------------------------------------------- */
/* Comments:                                                                  */
/* File    : /development/apps/MMITest/jni/mmijni.cpp                         */
/* Labels  :                                                                  */
/* -------------------------------------------------------------------------- */
/* ========================================================================== */
/* Modifications on Features list / Changes Request / Problems Report         */
/* -------------------------------------------------------------------------- */
/* date    | author         | key                | comment (what, where, why) */
/* --------|----------------|--------------------|--------------------------- */
/* 08/14/12| Shishun.liu    |                    | add for efuse check        */
/*         |                |                    |                            */
/*---------|----------------|--------------------|--------------------------- */
/******************************************************************************/

#define LOG_TAG "MMITest.mmijni"
#define LOG_NDEBUG 0
#include <utils/Log.h>

#include <string.h>
#include <errno.h>
#include <stdio.h>
#include <fcntl.h>
#include "jni.h"

static jint getOemfuseStatus(JNIEnv *env, jobject thisz) {
    int ret = 0;
    unsigned int fuse[] = {
            0x0000000C,
            0x0000000F,
            0xC5015862, 0x0090EE1E, 0xEDE0F0D7, 0x006B60B1, 0x87057EB0,
            0x004FD21B, 0xDFFAF369, 0x00DC7FD1, 0xF0B4D4E0, 0x00000000,
            0x00303030, 0x00000000 };
    char buff[128];
    int fd;
    int i, oemread, oemwrite;
    if ((fd = open("/dev/oemfuse", O_RDONLY)) < 0) {
        ALOGE("Open /dev/oemfuse error");
        return errno;
    }
    memset(buff, 0, sizeof(buff));
    if ((ret = read(fd, buff, 8)) < 8) {
        ALOGV("Read oemfuse error");
        return errno;
    }

    for (int k = 0; k < 2; k++) {
        ALOGV("%d,   %x,   %x", k, *((unsigned int *) &buff[k * 4]), fuse[k]);
    }

    for (i = 0; i < 2; i++) {
        if (*((unsigned int *) &buff[i * 4]) != fuse[i])
            break;
    }
    ALOGE("end read buffer i %d", i);

    if (i == 2)
    {
        ALOGV("Blow efuse successfully");
        return 0;
    }

    ALOGE("Blow efuse failed");
    return 1;
}

static const char *classPathName = "com/android/mmi/util/MMIJNI";

static JNINativeMethod methods[] = { { "getOemfuseStatus", "()I", (void *) getOemfuseStatus } };

/*
 * Register several native methods for one class.
 */
static int registerNativeMethods(JNIEnv* env, const char* className,
        JNINativeMethod* gMethods, int numMethods) {
    jclass clazz;

    clazz = env->FindClass(className);
    if (clazz == NULL) {
        ALOGE("Native registration unable to find class '%s'", className);
        return JNI_FALSE;
    }
    if (env->RegisterNatives(clazz, gMethods, numMethods) < 0) {
        ALOGE("RegisterNatives failed for '%s'", className);
        return JNI_FALSE;
    }

    return JNI_TRUE;
}

/*
 * Register native methods for all classes we know about.
 *
 * returns JNI_TRUE on success.
 */
static int registerNatives(JNIEnv* env) {
    if (!registerNativeMethods(env, classPathName, methods,
            sizeof(methods) / sizeof(methods[0]))) {
        return JNI_FALSE;
    }

    return JNI_TRUE;
}

// ----------------------------------------------------------------------------

/*
 * This is called by the VM when the shared library is first loaded.
 */

typedef union {
    JNIEnv* env;
    void* venv;
} UnionJNIEnvToVoid;

jint JNI_OnLoad(JavaVM* vm, void* reserved) {
    UnionJNIEnvToVoid uenv;
    uenv.venv = NULL;
    jint result = -1;
    JNIEnv* env = NULL;

    ALOGI("JNI_OnLoad");

    if (vm->GetEnv(&uenv.venv, JNI_VERSION_1_4) != JNI_OK) {
        ALOGE("ERROR: GetEnv failed");
        goto bail;
    }
    env = uenv.env;

    if (registerNatives(env) != JNI_TRUE) {
        ALOGE("ERROR: registerNatives failed");
        goto bail;
    }

    result = JNI_VERSION_1_4;

    bail: return result;
}
