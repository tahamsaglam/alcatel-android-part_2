package com.android.mmi;

import android.os.Handler;
import android.os.Message;
import android.provider.Settings;

import com.android.mmi.util.JRDClient;

public class SensorCalibration extends TestBase {
    private int mAutoBrightnessMode;
    private static final int CAL_SUCCESS = 0;
    private JRDClient mClient;

    @Override
    public void run() {
        // TODO Auto-generated method stub
        mAutoBrightnessMode = Settings.System.getInt(
                mContext.getContentResolver(), Settings.System.SCREEN_BRIGHTNESS_MODE, -1);
        if(mAutoBrightnessMode == Settings.System.SCREEN_BRIGHTNESS_MODE_AUTOMATIC){
            Settings.System.putInt(mContext.getContentResolver(),
                    Settings.System.SCREEN_BRIGHTNESS_MODE, Settings.System.SCREEN_BRIGHTNESS_MODE_MANUAL);
        }

        this.setDefTextMessage("waiting for calibrating...");
        mClient = new JRDClient();
        new Thread(Wait4Cal).start();
    }

    @Override
    public void resume() {
        // TODO Auto-generated method stub
        super.resume();
    }

    @Override
    public void pause() {
        // TODO Auto-generated method stub
        super.pause();
    }

    @Override
    public void destroy() {
        // TODO Auto-generated method stub
        super.destroy();
        if(mAutoBrightnessMode == Settings.System.SCREEN_BRIGHTNESS_MODE_AUTOMATIC){
            Settings.System.putInt(mContext.getContentResolver(),
                    Settings.System.SCREEN_BRIGHTNESS_MODE, mAutoBrightnessMode);
        }
    }

    private Runnable Wait4Cal = new Runnable() {

        @Override
        public void run() {
            // TODO Auto-generated method stub
//            int result =  SensorCalibrationJNI.Sensor_cal_test();
//            Message msg = new Message();//function 1,send message to UI thread.
//            msg.what = result;
//            mHandler.sendMessage(msg);
        };
    };

    Handler mHandler = new Handler() {
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case CAL_SUCCESS:
                    SensorCalibration.this.setDefTextMessage("Calibrate OK\nCalibration result is "+ msg.what);
                    mClient.write_calibration();
                    setPassButtonEnable(true);
                    break;
                default:
                    SensorCalibration.this.setDefTextMessage("Calibrate Failed\nCalibration result is " + msg.what);
                    break;
            }
            super.handleMessage(msg);
        }
    };
}
