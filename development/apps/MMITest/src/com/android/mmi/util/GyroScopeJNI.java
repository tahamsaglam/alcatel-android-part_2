package com.android.mmi.util;

public class GyroScopeJNI{

       private static final String TAG = "GyroScopeJNI";

        static{
        try {
            System.loadLibrary("gyroscop_test");
        }
        catch(UnsatisfiedLinkError e) {
            MMILog.e("MMITest", "Can't find load libgyroscop_test\n"+e.toString());
        }
    }

        static private native int runNativeSensorTest(int sensorID, int dataType, int testType,
              boolean saveToRegistry, boolean applyCalNow);

        static public synchronized int runGyroScopeTest(){

                int nativeTestResult = runNativeSensorTest(10,0,5,true, true);
                MMILog.d(TAG, "runGyroScopeTest nativeTestResult= "+nativeTestResult);
                return nativeTestResult;
        }

        static public synchronized int runGSensorTest(){

            int nativeTestResult = runNativeSensorTest(0,0,5,true, true);
            MMILog.d(TAG, "runGSensorTest nativeTestResult= "+nativeTestResult);
            return nativeTestResult;
    }
}
