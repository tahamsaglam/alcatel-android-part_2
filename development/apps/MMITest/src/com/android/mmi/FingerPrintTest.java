/******************************************************************************/
/*                                                               Date:05/2014 */
/*                             PRESENTATION                                   */
/*                                                                            */
/*      Copyright 2014 TCL Communication Technology Holdings Limited.         */
/*                                                                            */
/* This material is company confidential, cannot be reproduced in any form    */
/* without the written permission of TCL Communication Technology Holdings    */
/* Limited.                                                                   */
/*                                                                            */
/* -------------------------------------------------------------------------- */
/* Author:  Shishun Liu                                                       */
/* E-Mail:  shishun.liu@tcl.com                                               */
/* Role  :  MMITest                                                           */
/* Reference documents :  TCL NB NPI MMI TEST SPEC_V1.5.pdf                   */
/* -------------------------------------------------------------------------- */
/* Comments:                                                                  */
/* File    : /development/apps/MMITest/src/com/android/mmi/FingerPrint.java   */
/* Labels  :                                                                  */
/* -------------------------------------------------------------------------- */
/* ========================================================================== */
/* Modifications on Features list / Changes Request / Problems Report         */
/* -------------------------------------------------------------------------- */
/* date    | author         | key                | comment (what, where, why) */
/* --------|----------------|--------------------|--------------------------- */
/* 04/11/15| Shishun.Liu    |                    | Create for fingerprint test*/
/*---------|----------------|--------------------|--------------------------- */
/******************************************************************************/
package com.android.mmi;

import android.graphics.Bitmap;
import android.graphics.Color;
import android.os.Handler;
import android.os.Message;
import android.util.Log;

import com.android.mmi.util.MMILog;
import com.android.mmi.util.FpApiHelper;
import com.synaptics.fingerprint.Fingerprint;
import com.synaptics.fingerprint.DeviceInfo;

public class FingerPrintTest extends TestBase {
    private Handler mHandler;
    private FpApiHelper mFpApiHelper;

    @Override
    public void run() {
        // TODO Auto-generated method stub
        mHandler = new MemberHandler();
        mFpApiHelper = new FpApiHelper(mContext, mHandler);

        try {
            mFpApiHelper.doRequestSensorInfo();
        }
        catch(Exception e) {
            e.printStackTrace();
            setDefTextMessage("crash during test");
        }
    }

    @Override
    public void resume() {
        // TODO Auto-generated method stub
        super.resume();
    }

    @Override
    public void pause() {
        // TODO Auto-generated method stub
        super.pause();
        mHandler.removeCallbacksAndMessages(null);
        mFpApiHelper.doCancel();
        // put doCleanup here not in destroy, because in AUTO1/AUTO2 test mode,
        // the next test item is fingerprintEnroll, which will use FpApiHelper
        mFpApiHelper.doCleanup();
    }

    @Override
    public void destroy() {
        // TODO Auto-generated method stub
        super.destroy();

    }

    private class MemberHandler extends Handler {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);

            switch (msg.what) {
            case FpApiHelper.MSG_SCIPT_TEST_PASS:
                setDefTextMessage(String.valueOf(msg.obj), Color.GREEN);
                setPassButtonEnable(true);
                break;
            case FpApiHelper.MSG_SCIPT_TEST_FAIL:
                setDefTextMessage(String.valueOf(msg.obj), Color.RED);
                break;
            case FpApiHelper.MSG_SCIPT_TEST_SENSOR_INFO:
                if(msg.obj instanceof DeviceInfo) {
                    DeviceInfo sensorInfo = (DeviceInfo)msg.obj;
                    setDefActionMessage(DeviceInfo2String(sensorInfo));
                    switch (sensorInfo.flexId) {
                    case 179:
                        mFpApiHelper.doRequestSensorScriptTest(
                                Fingerprint.VCS_SNSR_TEST_ID179_73A_MET_NO_STIMULUS_SCRIPT_ID, true);
                        break;
                    case 5179:
                        mFpApiHelper.doRequestSensorScriptTest(
                                Fingerprint.VCS_SNSR_TEST_ID5179_73A_MET_NO_STIMULUS_SCRIPT_ID, true);
                        break;
                    default:
                        setDefTextMessage("Test not supported for this flexid", Color.RED);
                        break;
                    }
                }
                break;
            }
        }
    }

    private String DeviceInfo2String(DeviceInfo sensorInfo) {
        String string = "";

        //Product Id
        string += "Product Id: " + Integer.toHexString(sensorInfo.productId);

        // Proeject Id or Sensor Device Id
        //string += "\nProject Id: " + Integer.toHexString(sensorInfo.projectId );

        //Flex Id
        string += "\nFlex Id: " + sensorInfo.flexId;

        //Serial Number
        string += String.format("\nSerial Number: %02x%02x%02x%02x%02x%02x",
                        sensorInfo.serialNumber[0], sensorInfo.serialNumber[1],
                        sensorInfo.serialNumber[2], sensorInfo.serialNumber[3],
                        sensorInfo.serialNumber[4], sensorInfo.serialNumber[5]);

        //Firmware version
        string += "\nFirmware Version:" + sensorInfo.fwVersion;

        //Firmware extension version
        if (sensorInfo.fwExtVersion != null) {
            string += "\nFirmware Extension Version:" + sensorInfo.fwExtVersion;
        }

        //Silicon version
        if (sensorInfo.siliconVersion != 0) {
            string += "\nSilicon Version:" + sensorInfo.siliconVersion;
        }

        return string;
    }
}
