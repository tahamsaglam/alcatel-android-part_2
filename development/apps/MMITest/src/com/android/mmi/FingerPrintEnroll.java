/******************************************************************************/
/*                                                               Date:05/2014 */
/*                             PRESENTATION                                   */
/*                                                                            */
/*      Copyright 2014 TCL Communication Technology Holdings Limited.         */
/*                                                                            */
/* This material is company confidential, cannot be reproduced in any form    */
/* without the written permission of TCL Communication Technology Holdings    */
/* Limited.                                                                   */
/*                                                                            */
/* -------------------------------------------------------------------------- */
/* Author:  Shishun Liu                                                       */
/* E-Mail:  shishun.liu@tcl.com                                               */
/* Role  :  MMITest                                                           */
/* Reference documents :  TCL NB NPI MMI TEST SPEC_V1.5.pdf                   */
/* -------------------------------------------------------------------------- */
/* Comments:                                                                  */
/* File    : /development/apps/MMITest/src/com/android/mmi/ChargeLed.java     */
/* Labels  :                                                                  */
/* -------------------------------------------------------------------------- */
/* ========================================================================== */
/* Modifications on Features list / Changes Request / Problems Report         */
/* -------------------------------------------------------------------------- */
/* date    | author         | key                | comment (what, where, why) */
/* --------|----------------|--------------------|--------------------------- */
/* 05/08/14| Shishun.Liu    |                    | Porting for MMITest        */
/*---------|----------------|--------------------|--------------------------- */
/******************************************************************************/
package com.android.mmi;

import com.android.mmi.TestBase.LAYOUTTYPE;
import com.android.mmi.util.FpApiHelper;
import com.android.mmi.util.MMILog;
import android.os.Handler;
import android.os.Message;
import android.widget.ImageView;
import android.widget.TextView;

import com.synaptics.fingerprint.CapturedImageData;
import com.synaptics.fingerprint.Fingerprint;

public class FingerPrintEnroll extends TestBase {
    private Handler mHandler;
    private FpApiHelper mFpApiHelper;
    private ImageView mImageFP;
    private boolean mFinishTest = false;

    @Override
    public void create(CommonActivity a) {
        // TODO Auto-generated method stub
        super.create(a);
        setContentView(R.layout.test_fingerprint_enroll, LAYOUTTYPE.LAYOUT_CUST_WITH_PASSFAIL);
        mContext.mTextMessage = (TextView)this.findViewById(R.id.textview_common_message);
        mContext.mTextAction = (TextView)this.findViewById(R.id.textview_common_action);
        mImageFP = (ImageView)findViewById(R.id.imgFingerPrint);
    }

    @Override
    public void run() {
        // TODO Auto-generated method stub
        mHandler = new MemberHandler();
        mFpApiHelper = new FpApiHelper(mContext, mHandler);

        setPassButtonEnable(false);
        setButtonAnimateVisible();
    }

    @Override
    public void resume() {
        // TODO Auto-generated method stub
        super.resume();
        if(!mFinishTest) {
            setDefTextMessage("Please touch and lift your finger");
            mFpApiHelper.doIdentifyLoop(0);
        }
    }

    @Override
    public void pause() {
        // TODO Auto-generated method stub
        super.pause();
     // Cancel if any operation pending
        mFpApiHelper.doCancel();
    }

    @Override
    public void destroy() {
        // TODO Auto-generated method stub
        mFpApiHelper.doCleanup();
        super.destroy();
    }

    private class MemberHandler extends Handler {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);

            switch (msg.what) {
//            case FpApiHelper.MSG_LOG_INFO:
//                MMILog.i(TAG, String.valueOf(msg.obj));
//                setDefTextMessage(String.valueOf(msg.obj));
//                break;
            case FpApiHelper.MSG_IDENTFY_SHOW_IMG:
                if(msg.obj instanceof CapturedImageData) {
                    CapturedImageData imgData = (CapturedImageData)msg.obj;
                    String quality = mFpApiHelper.getImgQualityDesc(imgData.quality);
                    setDefActionMessage(quality);
                    try {
                        mImageFP.setImageBitmap(imgData.fingerprint);
                    } catch (Exception e) {
                        MMILog.e(TAG, "Decoding fingerprint img failed!");
                        e.printStackTrace();
                    }
                    if(imgData.quality==Fingerprint.VCS_IMAGE_QUALITY_GOOD) {
                        setDefTextMessage("");
                        mFinishTest = true;
                        setPassButtonEnable(true);
                    }
                }
                break;
            }
        }
    }
}
