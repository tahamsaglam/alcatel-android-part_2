/******************************************************************************/
/*                                                               Date:05/2014 */
/*                             PRESENTATION                                   */
/*                                                                            */
/*      Copyright 2014 TCL Communication Technology Holdings Limited.         */
/*                                                                            */
/* This material is company confidential, cannot be reproduced in any form    */
/* without the written permission of TCL Communication Technology Holdings    */
/* Limited.                                                                   */
/*                                                                            */
/* -------------------------------------------------------------------------- */
/* Author:  Shishun Liu                                                       */
/* E-Mail:  shishun.liu@tcl.com                                               */
/* Role  :  MMITest                                                           */
/* Reference documents :  TCL NB NPI MMI TEST SPEC_V1.5.pdf                   */
/* -------------------------------------------------------------------------- */
/* Comments:                                                                  */
/* File    : /development/apps/MMITest/src/com/android/mmi/KeypadBacklight.java*/
/* Labels  :                                                                  */
/* -------------------------------------------------------------------------- */
/* ========================================================================== */
/* Modifications on Features list / Changes Request / Problems Report         */
/* -------------------------------------------------------------------------- */
/* date    | author         | key                | comment (what, where, why) */
/* --------|----------------|--------------------|--------------------------- */
/* 15/01/15| Shishun.Liu    |                    | create for test in selinux */
/*---------|----------------|--------------------|--------------------------- */
/******************************************************************************/
package com.android.mmi;

import com.android.mmi.util.MMILog;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Handler;
import android.os.PowerManager;
import android.os.SystemClock;
import android.provider.Settings;
import android.view.WindowManager;

public class LcdBacklight extends TestBase {
    private int mCount = 0;
    private final int COUNT_MAX = 3;

    private int mSystemScreenBrightMode;
    private int mSystemScreenBrigntness;
    private PowerManager mPowerManager;
    private AlarmManager mWakeUpAlarmManager;
    private PendingIntent mPendingIntent;
    private WakeUpReceiver mWakeUpReceiver = new WakeUpReceiver();
    private final String WAKE_UP_ACTION = "mmi_system_wake_up";

    private Handler mHandler = new Handler();

    private Runnable mRunnable = new Runnable() {
        @Override
        public void run() {
            // TODO Auto-generated method stub
            mCount++;
            if(mCount<=COUNT_MAX) {
                if(mPowerManager.isInteractive()) {
                    MMILog.d(TAG, "set alarm and go to sleep");
                    mWakeUpAlarmManager.set(AlarmManager.RTC_WAKEUP, System.currentTimeMillis() + 1000, mPendingIntent);
                    mPowerManager.goToSleep(SystemClock.uptimeMillis());
                }
                else{
                    MMILog.d(TAG, "isInteractive=false, skip lcd setting");
                }
            }
        }
    };

    @Override
    public void run() {
        // TODO Auto-generated method stub
        setFailButtonEnable(false);
        dismissKeyGuard();
        mSystemScreenBrightMode = Settings.System.getInt(mContext.getContentResolver(),
                Settings.System.SCREEN_BRIGHTNESS_MODE, -1);
        if(mSystemScreenBrightMode==Settings.System.SCREEN_BRIGHTNESS_MODE_MANUAL) {
            mSystemScreenBrigntness = Settings.System.getInt(mContext.getContentResolver(),
                    Settings.System.SCREEN_BRIGHTNESS, -1);
        }

        mPowerManager = (PowerManager)mContext.getSystemService(Context.POWER_SERVICE);
        mWakeUpAlarmManager = (AlarmManager) mContext.getSystemService(Context.ALARM_SERVICE);
        mPendingIntent = PendingIntent.getBroadcast(mContext, 0, new Intent(WAKE_UP_ACTION), PendingIntent.FLAG_UPDATE_CURRENT);
        
        IntentFilter filter = new IntentFilter();
        filter.addAction(WAKE_UP_ACTION);
        mContext.registerReceiver(mWakeUpReceiver, filter);

        if(mSystemScreenBrightMode==Settings.System.SCREEN_BRIGHTNESS_MODE_AUTOMATIC) {
            Settings.System.putInt(mContext.getContentResolver(),
                    Settings.System.SCREEN_BRIGHTNESS_MODE, Settings.System.SCREEN_BRIGHTNESS_MODE_MANUAL);
        }
        mHandler.postDelayed(mRunnable, 1000);
        setDefTextMessage(R.string.backlight_message_lcd);
    }

    @Override
    public void resume() {
        // TODO Auto-generated method stub
        super.resume();
    }

    @Override
    public void pause() {
        // TODO Auto-generated method stub
        super.pause();
    }

    private class WakeUpReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            // TODO Auto-generated method stub
            MMILog.d(TAG, "wake up");
            mPowerManager.wakeUp(SystemClock.uptimeMillis());
            if(mCount<COUNT_MAX) {
                mHandler.postDelayed(mRunnable, 1000);
            }
            else {
                setPassButtonEnable(true);
                setFailButtonEnable(true);
            }
        }
    }

    @Override
    public void destroy() {
        // TODO Auto-generated method stub
        mContext.unregisterReceiver(mWakeUpReceiver);
        mHandler.removeCallbacks(mRunnable);
        if(mSystemScreenBrightMode==Settings.System.SCREEN_BRIGHTNESS_MODE_AUTOMATIC) {
            Settings.System.putInt(mContext.getContentResolver(),
                    Settings.System.SCREEN_BRIGHTNESS_MODE, mSystemScreenBrightMode);
        }
        else {
            Settings.System.putInt(mContext.getContentResolver(),
                    Settings.System.SCREEN_BRIGHTNESS, mSystemScreenBrigntness);
        }
        super.destroy();
    }

    private void dismissKeyGuard(){
        mContext.getWindow().addFlags(WindowManager.LayoutParams.FLAG_DISMISS_KEYGUARD);
    }

    @Override
    public void onBackPressed() {
        // TODO Auto-generated method stub
    }
}
