/******************************************************************************/
/*                                                               Date:05/2014 */
/*                             PRESENTATION                                   */
/*                                                                            */
/*      Copyright 2014 TCL Communication Technology Holdings Limited.         */
/*                                                                            */
/* This material is company confidential, cannot be reproduced in any form    */
/* without the written permission of TCL Communication Technology Holdings    */
/* Limited.                                                                   */
/*                                                                            */
/* -------------------------------------------------------------------------- */
/* Author:  Shishun Liu                                                       */
/* E-Mail:  shishun.liu@tcl.com                                               */
/* Role  :  MMITest                                                           */
/* Reference documents :  TCL NB NPI MMI TEST SPEC_V1.5.pdf                   */
/* -------------------------------------------------------------------------- */
/* Comments:                                                                  */
/* File    : /development/apps/MMITest/src/com/android/mmi/Proximity.java     */
/* Labels  :                                                                  */
/* -------------------------------------------------------------------------- */
/* ========================================================================== */
/* Modifications on Features list / Changes Request / Problems Report         */
/* -------------------------------------------------------------------------- */
/* date    | author         | key                | comment (what, where, why) */
/* --------|----------------|--------------------|--------------------------- */
/* 05/08/14| Shishun.Liu    |                    | Porting for MMITest        */
/*---------|----------------|--------------------|--------------------------- */
/******************************************************************************/
package com.android.mmi;

import com.android.mmi.util.MMILog;

import android.content.Context;
import android.graphics.Color;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.widget.TextView;

public class Proximity extends TestBase implements SensorEventListener {
    private SensorManager mSensorManager;
    private TextView mProxMain;
    private TextView mProxFar;
    private TextView mProxNear;
    private TextView mProxStatus;
    private boolean mIsFar = false;
    private boolean mIsNear = false;

    @Override
    public void create(CommonActivity a) {
        // TODO Auto-generated method stub
        super.create(a);
        setContentView(R.layout.test_proximity, LAYOUTTYPE.LAYOUT_CUST_WITH_PASSFAIL);
    }

    @Override
    public void run() {
        // TODO Auto-generated method stub
        mSensorManager = (SensorManager) mContext.getSystemService(Context.SENSOR_SERVICE);
        mProxMain = (TextView) findViewById(R.id.prox_main);
        mProxFar = (TextView) findViewById(R.id.prox_far);
        mProxNear = (TextView) findViewById(R.id.prox_near);
        mProxStatus = (TextView) findViewById(R.id.prox_status);
    }

    @Override
    public void resume() {
        // TODO Auto-generated method stub
        super.resume();
        mSensorManager.registerListener(this, mSensorManager.getDefaultSensor(Sensor.TYPE_PROXIMITY),
                SensorManager.SENSOR_DELAY_NORMAL);
        setPassButtonEnable(false);
    }

    @Override
    public void pause() {
        // TODO Auto-generated method stub
        super.pause();
        if (mSensorManager != null) {
            mSensorManager.unregisterListener(this);
        }
    }

    public void onAccuracyChanged(Sensor sensor, int accuracy) {

    }

    public void onSensorChanged(SensorEvent event) {
        int type = event.sensor.getType();

        if (type == Sensor.TYPE_PROXIMITY) {
            float[] value = event.values;

            MMILog.i(TAG, "value - " + value);
            MMILog.i(TAG, "value[0] - " + value[0]);
            MMILog.i(TAG, "value[1] - " + value[1]);
            MMILog.i(TAG, "value[2] - " + value[2]);

            float result = value[SensorManager.DATA_X];

            if (result == 0) {
                mIsNear = true;
                mProxStatus.setTextColor(Color.WHITE);
                mProxStatus.setText("Near");
                mProxMain.setTextSize(20);
                mProxMain.setTextColor(Color.GRAY);
            } else if (result >= 1.0f) {
                mIsFar = true;
                mProxStatus.setTextColor(Color.WHITE);
                mProxStatus.setText("Far");
                mProxMain.setTextSize(20);
                mProxMain.setTextColor(Color.WHITE);
            }

            if (mIsNear == true) {
                mProxNear.setText("near : OK");
            } else {
                mProxNear.setText("near : not tested");
            }

            if (mIsFar == true) {
                mProxFar.setText("far : OK");
            } else {
                mProxFar.setText("far : not tested");
            }
        }
        if (mIsNear == true && mIsFar == true) {
            setPassButtonEnable(true);
        }
    }
}