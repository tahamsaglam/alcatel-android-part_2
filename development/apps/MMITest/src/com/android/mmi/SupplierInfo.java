package com.android.mmi;

import java.io.FileInputStream;
import android.os.SystemProperties;

import com.android.mmi.util.SysClassManager;

public class SupplierInfo extends TestBase {

    @Override
    public void run() {
        // TODO Auto-generated method stub
        String info = "TP: " + readFileString(SysClassManager.SUPPLIER_TP)
                + "\nLCD: " + readFileString(SysClassManager.SUPPLIER_LCD)
                + "\nPrimary Camera: " + readFileString(SysClassManager.SUPPLIER_PRI_CAMERA)
                + "\nSecondary Camera: " + readFileString(SysClassManager.SUPPLIER_SEC_CAMERA);
        // read camera OTP status
        String status = SystemProperties.get("persist.camera.otp.status", "fail").trim();
        if(status.equalsIgnoreCase("ok")) {
            info += ("\nCamera OTP: " + status);
            this.setPassButtonEnable(true);
        }
        else {
            String statusValue = SystemProperties.get("persist.camera.otp.status.value", "").trim();
            info += ("\nCamera OTP: " + status + ", " + statusValue);
            this.setPassButtonEnable(false);
        }
        this.setDefTextMessage(info);
    }

    private String readFileString(String path) {
        SysClassManager fileManager = SysClassManager.getInstance();
        FileInputStream fileInputStream = fileManager.fileInputOpen(path);
        if(fileInputStream!=null) {
            fileManager.setFileInputStream(fileInputStream);
            return fileManager.readFileInputString().trim();
        }
        else {
            return "";
        }
    }
}
