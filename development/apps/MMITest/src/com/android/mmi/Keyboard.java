/******************************************************************************/
/*                                                               Date:05/2014 */
/*                             PRESENTATION                                   */
/*                                                                            */
/*      Copyright 2014 TCL Communication Technology Holdings Limited.         */
/*                                                                            */
/* This material is company confidential, cannot be reproduced in any form    */
/* without the written permission of TCL Communication Technology Holdings    */
/* Limited.                                                                   */
/*                                                                            */
/* -------------------------------------------------------------------------- */
/* Author:  Shishun Liu                                                       */
/* E-Mail:  shishun.liu@tcl.com                                               */
/* Role  :  MMITest                                                           */
/* Reference documents :  TCL NB NPI MMI TEST SPEC_V1.5.pdf                   */
/* -------------------------------------------------------------------------- */
/* Comments:                                                                  */
/* File    : /development/apps/MMITest/src/com/android/mmi/Keyboard.java      */
/* Labels  :                                                                  */
/* -------------------------------------------------------------------------- */
/* ========================================================================== */
/* Modifications on Features list / Changes Request / Problems Report         */
/* -------------------------------------------------------------------------- */
/* date    | author         | key                | comment (what, where, why) */
/* --------|----------------|--------------------|--------------------------- */
/* 05/08/14| Shishun.Liu    |                    | Porting for MMITest        */
/*---------|----------------|--------------------|--------------------------- */
/******************************************************************************/
package com.android.mmi;

import com.android.mmi.util.MMILog;
import android.content.ContentResolver;
import android.os.Handler;
import android.os.SystemProperties;
import android.provider.Settings;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;


public class Keyboard extends TestBase {
    private final static int NOT_BUTTON_INDEX_CALL = 0;
    private int[] notButtonArray = { NOT_BUTTON_INDEX_CALL, };
    protected Button[] buttonArray;
    protected int[] buttonResourceArray;
    protected String[] buttonStringArray;
    protected int visibleButtonLength = 0;
    protected int pressedButtonLength = 1;

    private Handler objHandler = new Handler();

//    private ContentResolver mContentResolver = null;
//    private int tctMMITest = 0;


    @Override
    public void create(CommonActivity a) {
        // TODO Auto-generated method stub
        super.create(a);
        setContentView(R.layout.test_keypress_idol4s, LAYOUTTYPE.LAYOUT_CUST_WITH_PASSFAIL);
    }

    @Override
    public void resume() {
        // TODO Auto-generated method stub
        super.resume();
//        if(tctMMITest == 0) {
//            Settings.Global.putInt(mContentResolver, TCT_MMITEST, 1);
//        }

        SystemProperties.set("dev.tct.MMITestPower", "true");
    }

    @Override
    public void pause() {
        // TODO Auto-generated method stub
//        if(tctMMITest == 0) {
//            Settings.Global.putInt(mContentResolver, TCT_MMITEST, 0);
//        }
        SystemProperties.set("dev.tct.MMITestPower", "false");
        super.pause();
    }

    @Override
    public void run() {
        // TODO Auto-generated method stub
//        mContentResolver = mContext.getContentResolver();
//        tctMMITest = Settings.Global.getInt(mContentResolver, TCT_MMITEST, 0);

        buttonStringArray = mContext.getResources().getStringArray(R.array.key_text);
        final int allButtonLength = buttonStringArray.length;

        //visibleButtonLength = allButtonLength - notButtonArray.length;
        visibleButtonLength = allButtonLength - 3 ;

        buttonArray = new Button[allButtonLength];
        buttonResourceArray = new int[allButtonLength];

        // index 0 is CALL
        for (int i = 1; i < allButtonLength; i++) {
/*                if(i==4) {
                    buttonArray[i] = null;
                    continue; // skip power on key
                }*/
            buttonResourceArray[i] = mContext.getResources().getIdentifier(
                    "key_btn_" + i, "id", mContext.getPackageName());
            buttonArray[i] = (Button) findViewById(buttonResourceArray[i]);
            buttonArray[i].setText(buttonStringArray[i]);
            buttonArray[i].setFocusable(false);

            if(i == 3 || i == 1 || i == 2){// 3 --> Back, 1 --> Home, 2 --> Menu
                View parent = (View) buttonArray[i].getParent();
                parent.setVisibility(View.INVISIBLE);
                buttonArray[i] = null;
            }
        }
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        return true;
    }

    @Override
    public boolean onKeyUp(int keyCode, KeyEvent event) {
        MMILog.d(TAG, "keyup: "+keyCode);
        int index = getKeyEvent(keyCode);
        setButtonPressed(index);
        checkAllKeyPress();
        return true;
    }

    private void checkAllKeyPress() {
        if (visibleButtonLength == pressedButtonLength) {
            onPassClick();
        }
    }

    private void setButtonPressed(int buttonIndex) {
        if (buttonIndex > 0 && buttonIndex < buttonArray.length
                && buttonArray[buttonIndex]!=null) {
            if (buttonArray[buttonIndex].isEnabled()) {
                pressedButtonLength++;
                buttonArray[buttonIndex].setEnabled(false);
            }
            if (!buttonArray[buttonIndex].isPressed()) {
                buttonArray[buttonIndex].setPressed(true);
                objHandler.postDelayed(mKeyboard, 500);
            }
        }

    }
    private Runnable mKeyboard = new Runnable() {

        @Override
        public void run() {
             // TODO Auto-generated method stub
            for (int i =0; i< buttonArray.length; i++) {
                if (buttonArray[i] != null && buttonArray[i].isPressed()) {
                    buttonArray[i].setPressed(false);
                }
            }
        }
     };

    private int getKeyEvent(int keyCode) {
        int resultCode = -1000;

        switch (keyCode) {
        case KeyEvent.KEYCODE_MENU: {
            resultCode = 2;
            break;
        }
        case KeyEvent.KEYCODE_HOME: {
            resultCode = 1;
            break;
        }

        case KeyEvent.KEYCODE_POWER: {
            resultCode = 4;
            break;
        }
        case KeyEvent.KEYCODE_BACK: {
            resultCode = 3;
            break;
        }
        case KeyEvent.KEYCODE_ENDCALL: {
            resultCode = 5;
            break;
        }
        case KeyEvent.KEYCODE_VOLUME_UP: {
            resultCode = 5;
            break;
        }
        case KeyEvent.KEYCODE_VOLUME_DOWN: {
            resultCode = 6;
            break;
        }
        case KeyEvent.KEYCODE_BOOM:
        case KeyEvent.KEYCODE_CAMERA: {
            resultCode = 7;
            break;
        }
        default:
            return resultCode = -9999;
        }

        return resultCode;
    }

}
