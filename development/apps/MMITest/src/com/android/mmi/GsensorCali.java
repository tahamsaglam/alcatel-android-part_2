/******************************************************************************/
/*                                                               Date:05/2014 */
/*                             PRESENTATION                                   */
/*                                                                            */
/*      Copyright 2014 TCL Communication Technology Holdings Limited.         */
/*                                                                            */
/* This material is company confidential, cannot be reproduced in any form    */
/* without the written permission of TCL Communication Technology Holdings    */
/* Limited.                                                                   */
/*                                                                            */
/* -------------------------------------------------------------------------- */
/* Author:  Shishun Liu                                                       */
/* E-Mail:  shishun.liu@tcl.com                                               */
/* Role  :  MMITest                                                           */
/* Reference documents :  TCL NB NPI MMI TEST SPEC_V1.5.pdf                   */
/* -------------------------------------------------------------------------- */
/* Comments:                                                                  */
/* File    : /development/apps/MMITest/src/com/android/mmi/Gsensor.java       */
/* Labels  :                                                                  */
/* -------------------------------------------------------------------------- */
/* ========================================================================== */
/* Modifications on Features list / Changes Request / Problems Report         */
/* -------------------------------------------------------------------------- */
/* date    | author         | key                | comment (what, where, why) */
/* --------|----------------|--------------------|--------------------------- */
/* 05/08/14| Shishun.Liu    |                    | Porting for MMITest        */
/*---------|----------------|--------------------|--------------------------- */
/******************************************************************************/
package com.android.mmi;

import android.content.ContentResolver;
import android.os.AsyncTask;
import android.os.Handler;
import android.provider.Settings;
import android.provider.Settings.SettingNotFoundException;

import com.android.mmi.traceability.TraceabilityStruct; //MODIFIED by Liu Shishun, 2016-03-30,BUG-1868091
import com.android.mmi.util.GyroScopeJNI;
import com.android.mmi.util.MMILog;
import com.qualcomm.qti.sensors.core.sensortest.SensorsReg;

public class GsensorCali extends TestBase {
    private int degreeRotation_90 = 0;
    private int degreeRotation_180 = 0;
    private ContentResolver mContentResolver = null;
    private enum CALI_STATUS{
        e_NOT_CALI,
        e_BEING_CALI,
        e_HAVE_CALI
    };
    private CALI_STATUS status = CALI_STATUS.e_NOT_CALI;
    private Handler mHandler = new Handler();
     /*MODIFIED-BEGIN by Liu Shishun, 2016-03-30,BUG-1868091*/
    private double mdx;
    private double mdy;
    private double mdz;
     /*MODIFIED-END by Liu Shishun,BUG-1868091*/

    @Override
    public void run() {
        // TODO Auto-generated method stub
        mContentResolver = mContext.getContentResolver();
        try {
            degreeRotation_90 = Settings.System.getInt(mContentResolver,
                    Settings.System.ACCELEROMETER_ROTATION);
//            degreeRotation_180 = Settings.System.getInt(mContentResolver,
//                    Settings.System.REVERSIBLE_ROTATION);
        } catch (SettingNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    @Override
    public void resume() {
        // TODO Auto-generated method stub
        super.resume();

        if (degreeRotation_90 == 1) {
            Settings.System.putInt(mContentResolver,
                    Settings.System.ACCELEROMETER_ROTATION, 0);
        }
//        if (degreeRotation_180 == 1) {
//            Settings.System.putInt(mContentResolver,
//                    Settings.System.REVERSIBLE_ROTATION, 0);
//        }
        if(status == CALI_STATUS.e_NOT_CALI) {
            setDefTextMessage("Being Cali...");
//            mHandler.postDelayed(mCaliRunnable, 1000); // wait 1000 ms so g-sensor is release completely
        }
    }

    @Override
    public void pause() {
        // TODO Auto-generated method stub
        super.pause();

        mHandler.removeCallbacks(mCaliRunnable);
        if (degreeRotation_90 == 1) {
            Settings.System.putInt(mContentResolver,
                    Settings.System.ACCELEROMETER_ROTATION, 1);
        }
//        if (degreeRotation_180 == 1) {
//            Settings.System.putInt(mContentResolver,
//                    Settings.System.REVERSIBLE_ROTATION, 1);
//        }
    }

    @Override
    public void destroy() {
        // TODO Auto-generated method stub
        super.destroy();
    }

    private Runnable mCaliRunnable = new Runnable() {

        @Override
        public void run() {
            // TODO Auto-generated method stub
            new SensorTestTask().execute();
        }
    };

    private class SensorTestTask extends AsyncTask<Object, Void, Integer> {

        @Override
        protected Integer doInBackground(Object... params) {
            status = CALI_STATUS.e_BEING_CALI;
            int rv;
             /*MODIFIED-BEGIN by Liu Shishun, 2016-03-30,BUG-1868091*/
            if(0==GyroScopeJNI.runGSensorTest()) {
                if(readSensorRegistry()) {
                    if(writeValueToTrace()) {
                        return 0;
                    }
                    else {
                        return 3;
                    }
                }
                else {
                    return 2;
                }
            }
            else {
                return 1;
            }
        }

        @Override
        protected void onPostExecute(Integer testResult) {
            status = CALI_STATUS.e_HAVE_CALI;
            switch (testResult) {
            case 0:
                GsensorCali.this.setDefTextMessage("Cali test: OK");
                GsensorCali.this.setDefActionMessage("dx : " + mdx + "\ndy : " + mdy + "\ndz : " + mdz);
                setPassButtonEnable(true);
                break;
            case 1:
                GsensorCali.this.setDefTextMessage("Cali test: NOK");
                break;
            case 2:
                GsensorCali.this.setDefTextMessage("Cali test: OK");
                GsensorCali.this.setDefActionMessage("Failed to read cali value");
                break;
            case 3:
                GsensorCali.this.setDefTextMessage("Cali test: OK");
                GsensorCali.this.setDefActionMessage("Failed to store cali value");
                break;
            default:
                GsensorCali.this.setDefTextMessage("Unknown error");
                break;
            }
        }
    }

    private boolean writeValueToTrace() {
        try {
            TraceabilityStruct traceabilityStruct = new TraceabilityStruct();
            byte result[] = new byte[12];
            int value = toInt(mdx);
            if(value<0) {
                value = value*(-1);
                result[3] = (byte) ( ((value >> 24) & 0xFF) | 0x80);
            }
            else {
                result[3] = (byte) ((value >> 24) & 0xFF);
            }
            result[0] = (byte) (value & 0xFF);
            result[1] = (byte) ((value >> 8) & 0xFF);
            result[2] = (byte) ((value >> 16) & 0xFF);

            value = toInt(mdy);
            if(value<0) {
                value = value*(-1);
                result[7] = (byte) ( ((value >> 24) & 0xFF) | 0x80);
            }
            else {
                result[7] = (byte) ((value >> 24) & 0xFF);
            }
            result[4] = (byte) (value & 0xFF);
            result[5] = (byte) ((value >> 8) & 0xFF);
            result[6] = (byte) ((value >> 16) & 0xFF);

            value = toInt(mdz);
            if(value<0) {
                value = value*(-1);
                result[11] = (byte) ( ((value >> 24) & 0xFF) | 0x80);
            }
            else {
                result[11] = (byte) ((value >> 24) & 0xFF);
            }
            result[8] = (byte) (value & 0xFF);
            result[9] = (byte) ((value >> 8) & 0xFF);
            result[10] = (byte) ((value >> 16) & 0xFF);
            return traceabilityStruct.putItem(TraceabilityStruct.ID.INFO_TEMP_CALI_VALUE, result);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    private int toInt(double value) {
        int int_value = new Double(value*1000).intValue();
        MMILog.d(TAG, "toInt() = "+int_value);
        return int_value;
    }
     /*MODIFIED-END by Liu Shishun,BUG-1868091*/

    private boolean readSensorRegistry() {
        if( SensorsReg.open()==0 ) {
            byte readResult[] = null;
            try{
                // dx=reg[1], dy=reg[0], dz=reg[2]*(-1)

                readResult = SensorsReg.getRegistryValue(1);
                 /*MODIFIED-BEGIN by Liu Shishun, 2016-03-30,BUG-1868091*/
                mdx = toOffsetValue(readResult);
                MMILog.d(TAG, "reg[1] : "+toHexString(readResult) + " => " + mdx);

                readResult = SensorsReg.getRegistryValue(0);
                mdy = toOffsetValue(readResult);
                MMILog.d(TAG, "reg[0] : "+toHexString(readResult) + " => " + mdy);

                readResult = SensorsReg.getRegistryValue(2);
                mdz = toOffsetValue(readResult)*(-1);
                MMILog.d(TAG, "reg[2] : "+toHexString(readResult) + " => " + mdz);
                 /*MODIFIED-END by Liu Shishun,BUG-1868091*/

                return true;
            } catch(Exception e){
                MMILog.e(TAG, "read registry error, " + e.getLocalizedMessage());
            }
            SensorsReg.close();
        }
        return false;
    }

    private double toOffsetValue(byte[] bytes) {
        if(bytes.length !=4) {
            return 0;
        }
        // to int variable value
        int a = bytes[3]&0xff;
        for(int i=bytes.length-2; i >=0  ; i--){
            a = (a<<8) | (bytes[i]&0xff);
        }

        // to offset value
        return a/65536.0;
    }

    public static String toHexString(byte[] bytes){
        StringBuffer sb = new StringBuffer(bytes.length*2);
        for(int i = bytes.length - 1; i >= 0 ; i--){
            sb.append(toHex(bytes[i]>>4));
            sb.append(toHex(bytes[i]));
        }

        return sb.toString();
    }

    private static char toHex(int input){
        final char[] hexDigit = { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F' };
        return hexDigit[input & 0xF];
    }
}
