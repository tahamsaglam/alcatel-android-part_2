/******************************************************************************/
/*                                                               Date:05/2014 */
/*                             PRESENTATION                                   */
/*                                                                            */
/*      Copyright 2014 TCL Communication Technology Holdings Limited.         */
/*                                                                            */
/* This material is company confidential, cannot be reproduced in any form    */
/* without the written permission of TCL Communication Technology Holdings    */
/* Limited.                                                                   */
/*                                                                            */
/* -------------------------------------------------------------------------- */
/* Author:  Shishun Liu                                                       */
/* E-Mail:  shishun.liu@tcl.com                                               */
/* Role  :  MMITest                                                           */
/* Reference documents :  TCL NB NPI MMI TEST SPEC_V1.5.pdf                   */
/* -------------------------------------------------------------------------- */
/* Comments:                                                                  */
/* File    : /development/apps/MMITest/src/com/android/mmi/Proximity.java     */
/* Labels  :                                                                  */
/* -------------------------------------------------------------------------- */
/* ========================================================================== */
/* Modifications on Features list / Changes Request / Problems Report         */
/* -------------------------------------------------------------------------- */
/* date    | author         | key                | comment (what, where, why) */
/* --------|----------------|--------------------|--------------------------- */
/* 05/08/14| Shishun.Liu    |                    | Porting for MMITest        */
/*---------|----------------|--------------------|--------------------------- */
/******************************************************************************/
package com.android.mmi;

import java.io.BufferedReader;
import java.io.InputStreamReader;

import com.android.mmi.traceability.TraceabilityStruct;
import com.android.mmi.util.MMILog;

import android.os.Handler;
import android.widget.TextView;

public class ProximityRawdata extends TestBase {

    private TextView mProxMain;

    private Handler mHandler = new Handler();
    private boolean isUpdatingRaw = false;
    private boolean isSavingRaw = false;
    private int mCurRaw = 0;
    private TraceabilityStruct traceabilityStruct;

    @Override
    public void create(CommonActivity a) {
        // TODO Auto-generated method stub
        super.create(a);
        setContentView(R.layout.test_proximity_rawdata, LAYOUTTYPE.LAYOUT_CUST_WITH_PASSFAIL);
    }

    @Override
    public void run() {
        // TODO Auto-generated method stub
        mProxMain = (TextView) findViewById(R.id.prox_main);
        mProxMain.setText(getStateString(mCurRaw));

        try {
            traceabilityStruct = new TraceabilityStruct();
        } catch (Exception e) {
            e.printStackTrace();
        }

        mHandler.postDelayed(updateRaw, 500);
        setPassButtonEnable(true);
    }

    @Override
    public void resume() {
        // TODO Auto-generated method stub
        super.resume();
        mHandler.postDelayed(updateRaw, 500);
        isUpdatingRaw = false;
    }

    @Override
    public void pause() {
        // TODO Auto-generated method stub
        super.pause();
        mHandler.removeCallbacks(updateRaw);
    }

    private Runnable updateRaw = new Runnable() {
        @Override
        public void run() {
            // TODO Auto-generated method stub
            getRawdata(1, false);
        }
    };

    private Runnable saveRaw = new Runnable() {
        @Override
        public void run() {
            // TODO Auto-generated method stub
            isSavingRaw = true;
            getRawdata(5, true);
        }
    };

    private String getStateString(int raw) {
        return String.format(mContext.getString(R.string.prox_main_raw), raw);
    }

    @Override
    public void onPassClick() {
        setPassButtonEnable(false);
        mHandler.post(saveRaw);
    }

    private void getRawdata(int times, boolean save) {
        int value = 0;
        try {
            int count = 0;

            while (true) {
                if (isUpdatingRaw) {
                    if (count < 10) {
                        MMILog.d(TAG, "sleep 1s to wait previous cmd finish");
                        count++;
                        Thread.sleep(1000);
                    } else {
                        break;
                    }
                } else {
                    break;
                }
            }

            String cmd = "sns_proximity_test -s 40 -c " + String.valueOf(times);
            isUpdatingRaw = true;
            Process p = Runtime.getRuntime().exec(cmd);
            p.waitFor();
            BufferedReader br = new BufferedReader(new InputStreamReader(p.getInputStream()), 4096);
            String line = "";
            int real_times = 0;
            while ((line = br.readLine()) != null) {
                MMILog.i(TAG, "line: " + line);
                if (line.startsWith("ID=")) {
                    int start = line.indexOf("Val=");
                    int end = line.indexOf("Quality");
                    String[] val_range = line.substring(start, end).split(",");
                    int val = Integer.valueOf(val_range[1]);
                    value += val;
                    real_times++;
                }
            }
            value /= real_times;
            mCurRaw = value;
            MMILog.d(TAG, "value: " + value + " real_times: " + real_times);
        } catch (Exception e) {
            MMILog.e(TAG, "getRawdata fail! " + e.getMessage());
        }

        isUpdatingRaw = false;
        mProxMain.setText(getStateString(mCurRaw));

        if (save) {
            byte[] result = new byte[18];
            result[0] = result[17] = (byte) ('@');
            result[1] = (byte) (value & 0xff);
            result[2] = (byte) ((value >> 8) & 0xff);
            result[3] = (byte) ((value >> 16) & 0xff);
            result[4] = (byte) ((value >> 24) & 0xff);

            if (traceabilityStruct != null) {
                traceabilityStruct.putItem(TraceabilityStruct.ID.PSENSOR, result);
            }

            setPassButtonEnable(true);
            super.onPassClick();
        } else {
            if (!isSavingRaw) {
                mHandler.postDelayed(updateRaw, 500);
            }
        }
    }
}