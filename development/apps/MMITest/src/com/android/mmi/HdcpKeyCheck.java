/******************************************************************************/
/*                                                               Date:09/2012 */
/*                             PRESENTATION                                   */
/*                                                                            */
/*      Copyright 2015 TCL Communication Technology Holdings Limited.         */
/*                                                                            */
/* This material is company confidential, cannot be reproduced in any form    */
/* without the written permission of TCL Communication Technology Holdings    */
/* Limited.                                                                   */
/*                                                                            */
/* -------------------------------------------------------------------------- */
/* Author:  Shishun Liu                                                       */
/* E-Mail:  shishun.liu@tcl-mobile.com                                        */
/* Role  :  MMITest                                                           */
/* Reference documents :  TCT NB NPI MMI TEST SPEC_V1.6.pdf                   */
/* -------------------------------------------------------------------------- */
/* Comments:                                                                  */
/* File    : /development/apps/MMITest/src/com/android/mmi/HdcpKeyCheck.java  */
/* Labels  :                                                                  */
/* -------------------------------------------------------------------------- */
/* ========================================================================== */
/* Modifications on Features list / Changes Request / Problems Report         */
/* -------------------------------------------------------------------------- */
/* date    | author         | key                | comment (what, where, why) */
/* --------|----------------|--------------------|--------------------------- */
/* 01/04/15| Shishun.Liu    |   PR-887772        | add HDCP key check         */
/*---------|----------------|--------------------|--------------------------- */
/******************************************************************************/
package com.android.mmi;

import java.io.File;

import com.android.mmi.util.JRDClient;

public class HdcpKeyCheck extends TestBase{
    private final String mInitStatusHead = "Init:";
    private String mHdcpKeyStatus;

    @Override
    public void run() {
        // TODO Auto-generated method stub
        try {
            getHDCPKeyStatus();
        } catch (Exception e) {
            // TODO: handle exception
        }
        setDefTextMessage(mHdcpKeyStatus);
    }

    private void getHDCPKeyStatus(){
        JRDClient jrdClient = new JRDClient();
        long l_initStatus = 0;
        byte[] byteInitStatus = jrdClient.readHdcpInitStatus();
        if(byteInitStatus.length ==4 ) {
            l_initStatus += (byteInitStatus[0]);
            l_initStatus += (byteInitStatus[1]<<8);
            l_initStatus += (byteInitStatus[2]<<16);
            l_initStatus += (byteInitStatus[3]<<24);
            mHdcpKeyStatus = mInitStatusHead + String.format(" 0x%08X", l_initStatus);
        }
        if(byteInitStatus.length!=4
                || l_initStatus != 0x00000000) {
            mHdcpKeyStatus += "(NOK)";
        }
        else {
            setPassButtonEnable(true);
        }

        mHdcpKeyStatus += ("\nsfs(bytes):\n" + getKeyFileSizeList("/persist/data/sfs"));
        mHdcpKeyStatus += ("\ntz(bytes):\n" + getKeyFileSizeList("/persist/data/tz"));
    }

    private String getKeyFileSizeList(String path) {
        String sizeList = "";
        File[] keyFiles = new File(path).listFiles();
        for(File file:keyFiles) {
            if(file.isFile()) {
                sizeList += (file.length() + "\n");
            }
        }
        return sizeList.trim();
    }

}