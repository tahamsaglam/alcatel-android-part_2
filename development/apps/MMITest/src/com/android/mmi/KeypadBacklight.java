/******************************************************************************/
/*                                                               Date:05/2014 */
/*                             PRESENTATION                                   */
/*                                                                            */
/*      Copyright 2014 TCL Communication Technology Holdings Limited.         */
/*                                                                            */
/* This material is company confidential, cannot be reproduced in any form    */
/* without the written permission of TCL Communication Technology Holdings    */
/* Limited.                                                                   */
/*                                                                            */
/* -------------------------------------------------------------------------- */
/* Author:  Shishun Liu                                                       */
/* E-Mail:  shishun.liu@tcl.com                                               */
/* Role  :  MMITest                                                           */
/* Reference documents :  TCL NB NPI MMI TEST SPEC_V1.5.pdf                   */
/* -------------------------------------------------------------------------- */
/* Comments:                                                                  */
/* File    : /development/apps/MMITest/src/com/android/mmi/KeypadBacklight.java*/
/* Labels  :                                                                  */
/* -------------------------------------------------------------------------- */
/* ========================================================================== */
/* Modifications on Features list / Changes Request / Problems Report         */
/* -------------------------------------------------------------------------- */
/* date    | author         | key                | comment (what, where, why) */
/* --------|----------------|--------------------|--------------------------- */
/* 03/23/15| Shishun.Liu    |                    | create for selinux test    */
/*---------|----------------|--------------------|--------------------------- */
/******************************************************************************/
package com.android.mmi;

import com.android.mmi.util.MMILog;

//import android.os.Handler;
//import android.os.IHardwareService;
//import android.os.RemoteException;
//import android.os.ServiceManager;

public class KeypadBacklight extends TestBase {

    @Override
    public void run() {
        //
    }

/*    private boolean mLightEnabled;
    private IHardwareService mLight;
    private int mCount = 0;
    private final int COUNT_MAX = 6;

    private Handler mHandler = new Handler();

    private Runnable mRunnable = new Runnable() {
        @Override
        public void run() {
            // TODO Auto-generated method stub
            mCount++;
            if(mCount<=COUNT_MAX) {
//                try {
//                    MMILog.d(TAG, "set button ligth on/off");
//                    mLight.setButtonLightEnabled((mCount%2)==1);
//                }
//                catch(RemoteException e) {
//                    MMILog.e(TAG, "remote call for turn on button light failed.");
//                    return;
//                }
                if(mCount<COUNT_MAX) {
                    mHandler.postDelayed(mRunnable, 1000);
                }
            }
        }
    };

    @Override
    public void run() {
        // TODO Auto-generated method stub
        mLight = IHardwareService.Stub.asInterface(
                ServiceManager.getService("hardware"));
        mLightEnabled = (mLight != null);
        MMILog.d(TAG, "mLightEnabled = " + mLightEnabled);
        setDefTextMessage(R.string.backlight_message_keypad);
        if(mLightEnabled) {
            setPassButtonEnable(true);
        }
    }

    @Override
    public void resume() {
        // TODO Auto-generated method stub
        super.resume();
        if(mLightEnabled) {
            mHandler.postDelayed(mRunnable, 200);
        }
    }

    @Override
    public void pause() {
        // TODO Auto-generated method stub
        super.pause();
        mHandler.removeCallbacks(mRunnable);
    }
*/
}
