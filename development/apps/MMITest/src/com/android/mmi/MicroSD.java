/******************************************************************************/
/*                                                               Date:05/2014 */
/*                             PRESENTATION                                   */
/*                                                                            */
/*      Copyright 2014 TCL Communication Technology Holdings Limited.         */
/*                                                                            */
/* This material is company confidential, cannot be reproduced in any form    */
/* without the written permission of TCL Communication Technology Holdings    */
/* Limited.                                                                   */
/*                                                                            */
/* -------------------------------------------------------------------------- */
/* Author:  Shishun Liu                                                       */
/* E-Mail:  shishun.liu@tcl.com                                               */
/* Role  :  MMITest                                                           */
/* Reference documents :  TCL NB NPI MMI TEST SPEC_V1.5.pdf                   */
/* -------------------------------------------------------------------------- */
/* Comments:                                                                  */
/* File    : /development/apps/MMITest/src/com/android/mmi/MicroSD.java       */
/* Labels  :                                                                  */
/* -------------------------------------------------------------------------- */
/* ========================================================================== */
/* Modifications on Features list / Changes Request / Problems Report         */
/* -------------------------------------------------------------------------- */
/* date    | author         | key                | comment (what, where, why) */
/* --------|----------------|--------------------|--------------------------- */
/* 05/08/14| Shishun.Liu    |                    | Porting for MMITest        */
/*---------|----------------|--------------------|--------------------------- */
/******************************************************************************/
package com.android.mmi;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

import com.android.mmi.util.MMILog;

import android.content.Context;
import android.os.Environment;
import android.os.Handler;
import android.os.storage.StorageEventListener;
import android.os.storage.StorageManager;
import android.os.storage.StorageVolume;
import libcore.io.IoUtils;

public class MicroSD extends TestBase {

    public static final String STRING_TESTFILE_NAME = "/test.txt";
    public static final byte VALUE_TEST_BYTE = 9;

    public static /*final*/ String SD_DIR = "/storage/sdcard1";

    private boolean isReadDataSame;

    private StorageManager storageManager;
    private StorageEventListener storageEventListener;

    private Handler mSdHandler;
    protected int mUserSelectMode = Value.USER_SELECT_MODE_UNKNOW;


    @Override
    public void run() {
        // TODO Auto-generated method stub
        storageManager = (StorageManager) mContext.getSystemService(Context.STORAGE_SERVICE);

        StorageVolume[] storageVolumeList = storageManager.getVolumeList();
        if (storageVolumeList != null) {
            for (StorageVolume volume : storageVolumeList) {
                String path = volume.getPath();
                boolean removeable = volume.isRemovable();
                MMILog.d(TAG, "path: " + path + " removeable: " + removeable);
                if(removeable){
                    SD_DIR = path;
                    break;
                }
            }
        }

        storageEventListener = new MemberStorageEventListener();
        mSdHandler = new Handler();

        runTest();
    }

    private void runTest() {
        if(isSDcardMounted()) {
            MMILog.i("MMITEST_GIM_MEMORYCARD", "RESULT:SD CARD ALREADY MOUNTED");
            if(startFileReadTest()) {
                setDefTextMessage(R.string.sdcard_read_success);
                setPassButtonEnable(true);
                if(Value.isAutoTest){
                    mSdHandler.postDelayed(mSdFinish, 2000);
                }
            }
            else {
                MMILog.i("MMITEST_GIM_MEMORYCARD", "RESULT:WRITE/READ FAIL");
                setDefTextMessage(R.string.sdcard_read_fail);
            }
        }
        else {
            MMILog.i("MMITEST_GIM_MEMORYCARD", "RESULT:NO SD CARD");
            setDefTextMessage(R.string.sdcard_mount_fail);
        }
    }

    @Override
    public void resume() {
        // TODO Auto-generated method stub
        super.resume();
        storageManager.registerListener(storageEventListener);
    }

    @Override
    public void pause() {
        // TODO Auto-generated method stub
        super.pause();
        storageManager.unregisterListener(storageEventListener);
    }

    @Override
    public void destroy() {
        // TODO Auto-generated method stub
        super.destroy();
        mSdHandler.removeCallbacks(mSdFinish);
    }

    /* *****************************************************************************
     * METHOD get external memory status
     * ****************************************
     * ***********************************
     */
    private boolean isSDcardMounted() {
        String state = "";
        /*StorageVolume[] storageVolume = storageManager.getVolumeList();
        for (int i=0; i<storageVolume.length;i++) {
          StorageVolume sv =   storageVolume[i];
          System.out.println("storagevolume"+sv.getPath());
          if(sv == null || SD_DIR.equals(sv.getPath())) {
              state = sv != null ? storageManager.getVolumeState(sv
                  .getPath()) : Environment.MEDIA_MOUNTED;
          }
        }*/
        state=storageManager.getVolumeState(SD_DIR);
        MMILog.d(TAG, "path: " + SD_DIR + ", state: " +state);
        if (state.equals(
                Environment.MEDIA_MOUNTED)) {
            return true;
        } else {
            return false;
        }
    }

    private String getExternalFilePath() {
        String externalPath = SD_DIR;//Environment.getExternalStorageDirectory()
                //.getAbsolutePath();
        return externalPath + STRING_TESTFILE_NAME;
    }

    /* *****************************************************************************
     * METHOD process external file's life cycle
     * ********************************
     * *******************************************
     */
    private File setExternalFileCreate(String path) throws IOException {
        File externalFile = new File(path);

        if (!externalFile.exists()) {
            MMILog.d(TAG, "before create!");
            externalFile.createNewFile();
            MMILog.d(TAG, "end create!");
        }
        if (externalFile.exists()) {
            MMILog.d(TAG, "create sicess!");
        }else
            MMILog.d(TAG, "create fail!");
        return externalFile;
    }

    private File setExternalFileDelete(File externalFile) throws IOException {
        if (externalFile.exists()) {
            externalFile.delete();
        }
        return externalFile;
    }

    private FileOutputStream setExternalFileWrite(File externalFile)
            throws FileNotFoundException, IOException {
        FileOutputStream externalFileOutputStream;
        externalFileOutputStream = new FileOutputStream(externalFile);
        externalFileOutputStream.write(VALUE_TEST_BYTE);
        externalFileOutputStream.close();
        return externalFileOutputStream;
    }

    private FileInputStream setExternalFileRead(File externalFile)
            throws FileNotFoundException, IOException {
        FileInputStream externalFileInputStream;
        externalFileInputStream = new FileInputStream(externalFile);
        isReadDataSame = isReadDataSame(externalFileInputStream.read());
        externalFileInputStream.close();
        return externalFileInputStream;
    }

    /* *****************************************************************************
     * METHOD check external memory read test
     * ***********************************
     * ****************************************
     */
    private boolean startFileReadTest() {
        String filePath;
        File externalFile = null;
        FileInputStream externalFileInputStream = null;
        FileOutputStream externalFileOutputStream = null;

        if (!isSDcardMounted()) {
            return false;
        }

        filePath = getExternalFilePath();
        try {
            externalFile = setExternalFileCreate(filePath);
            externalFileOutputStream = setExternalFileWrite(externalFile);
            MMILog.d(TAG, "write");
            externalFileInputStream = setExternalFileRead(externalFile);
            MMILog.d(TAG, "read");
            externalFile = setExternalFileDelete(externalFile);
        } catch (FileNotFoundException e) {
            MMILog.e(TAG, e + ", externalFile: " + externalFile);
            return false;
        } catch (IOException e) {
            MMILog.e(TAG, e + ", isSDcardMounted: " + isSDcardMounted());
            return false;
        } catch (Exception e) {
            MMILog.e(TAG, e.toString());
            return false;
        } finally {
            IoUtils.closeQuietly(externalFileInputStream);
            IoUtils.closeQuietly(externalFileOutputStream);
        }

        return isReadDataSame;
    }

    private boolean isReadDataSame(int readData) {
        MMILog.d(TAG, "isReadDataSame: " + readData);
        return readData == VALUE_TEST_BYTE;
    }

    private Runnable mSdFinish = new Runnable() {

        @Override
        public void run() {
             // TODO Auto-generated method stub
            onPassClick();
        }
     };

     private class MemberStorageEventListener extends StorageEventListener {
         @Override
         public void onStorageStateChanged(String path, String oldState,
                 String newState) {
             super.onStorageStateChanged(path, oldState, newState);
             if (Environment.MEDIA_MOUNTED.equals(newState)) {
                 SD_DIR = path;
                 runTest();
             }
         }
     }

}
