/******************************************************************************/
/*                                                               Date:05/2014 */
/*                             PRESENTATION                                   */
/*                                                                            */
/*      Copyright 2014 TCL Communication Technology Holdings Limited.         */
/*                                                                            */
/* This material is company confidential, cannot be reproduced in any form    */
/* without the written permission of TCL Communication Technology Holdings    */
/* Limited.                                                                   */
/*                                                                            */
/* -------------------------------------------------------------------------- */
/* Author:  Shishun Liu                                                       */
/* E-Mail:  shishun.liu@tcl.com                                               */
/* Role  :  MMITest                                                           */
/* Reference documents :  TCL NB NPI MMI TEST SPEC_V1.5.pdf                   */
/* -------------------------------------------------------------------------- */
/* Comments:                                                                  */
/* File    : /development/apps/MMITest/src/com/android/mmi/SoftwarePartion.java*/
/* Labels  :                                                                  */
/* -------------------------------------------------------------------------- */
/* ========================================================================== */
/* Modifications on Features list / Changes Request / Problems Report         */
/* -------------------------------------------------------------------------- */
/* date    | author         | key                | comment (what, where, why) */
/* --------|----------------|--------------------|--------------------------- */
/* 05/08/14| Shishun.Liu    |                    | Porting for MMITest        */
/*---------|----------------|--------------------|--------------------------- */
/******************************************************************************/
package com.android.mmi;

import com.android.mmi.util.VersionAPI;

public class SoftwarePartion extends TestBase {
    private String swinfo="";

    @Override
    public void run() {
        // TODO Auto-generated method stub
        try {
            swinfo=showSwVersionPanel();
        } catch (Exception e) {
            e.printStackTrace();
        }
        this.setDefActionMessage("Image Mapping\n"+swinfo);
        this.setPassButtonEnable(true);
    }

    private String showSwVersionPanel() {
        VersionAPI v= new VersionAPI();
        String boot_ver = v.getBootVer();/*boot version*/
        String sys_ver = v.getSystemVer();/*system version*/
        String custpack_ver=v.getCustpackVer();
        String recovery_ver =v.getRecoveryVer();
        String modem_ver = v.getModemVer();

        String sw_version = "";
        sw_version +=boot_ver.contains("??")?"":(boot_ver+'\n');
        sw_version +=sys_ver.contains("??")?"":(sys_ver+'\n');
        sw_version +=custpack_ver.contains("??")?"":(custpack_ver+'\n');
        sw_version +=recovery_ver.contains("??")?"":(recovery_ver+'\n');
        sw_version +=modem_ver.contains("??")?"":(modem_ver+'\n');

        return sw_version;
    }
}
