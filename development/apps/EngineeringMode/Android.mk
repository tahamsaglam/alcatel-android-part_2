LOCAL_PATH := $(call my-dir)
include $(CLEAR_VARS)

LOCAL_MODULE_TAGS := optional

LOCAL_SRC_FILES := $(call all-subdir-java-files)


LOCAL_PACKAGE_NAME := TestMode
LOCAL_CERTIFICATE := platform

LOCAL_OVERRIDES_PACKAGES := Home

#LOCAL_JAVA_LIBRARIES := tct.feature_query

include $(BUILD_PACKAGE)

# Also build all of the sub-targets under this one: the shared library.
include $(call all-makefiles-under,$(LOCAL_PATH))

#endif
