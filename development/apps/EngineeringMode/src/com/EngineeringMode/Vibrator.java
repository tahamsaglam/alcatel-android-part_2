/******************************************************************************/
/*                                                               Date:09/2012 */
/*                             PRESENTATION                                   */
/*                                                                            */
/*      Copyright 2012 TCL Communication Technology Holdings Limited.         */
/*                                                                            */
/* This material is company confidential, cannot be reproduced in any form    */
/* without the written permission of TCL Communication Technology Holdings    */
/* Limited.                                                                   */
/*                                                                            */
/* -------------------------------------------------------------------------- */
/* Author:  Qianbo Pan                                                        */
/* E-Mail:  Qianbo.Pan@tcl-mobile.com                                         */
/* Role  :  MMITest                                                           */
/* Reference documents :  Scribe 5_MMI_AUTO_Test_specfication_V3.0.Doc        */
/* -------------------------------------------------------------------------- */
/* Comments:                                                                  */
/* File    : /development/apps/EngineeringMode/src/com/EngineeringMode/       */
/*                                                         VibratorTest.java  */
/* Labels  :                                                                  */
/* -------------------------------------------------------------------------- */
/* ========================================================================== */
/* Modifications on Features list / Changes Request / Problems Report         */
/* -------------------------------------------------------------------------- */
/* date    | author         | key                | comment (what, where, why) */
/* --------|----------------|--------------------|--------------------------- */
/* 12/09/12| Qianbo.Pan     |                    | Porting for EngineeringMode*/
/*         |                |                    |                            */
/*---------|----------------|--------------------|--------------------------- */
/******************************************************************************/
package com.EngineeringMode;

import android.content.Context;
import android.os.Vibrator;
import android.util.TctLog;

class VibratorTest extends Test {
    String TAG = "VibratorTest";

    private Vibrator vibrator = null;
    private TestLayout1 tl;

    VibratorTest(ID pid, String s) {
        super(pid, s);
    }

    VibratorTest(ID pid, String s, int timein) {
        super(pid, s, timein, 0);
    }

    @Override
    protected void Run() {
        if (null == mContext) {
            TctLog.e(TAG, "mContext is null");
        } else {
            long[] pattern = new long[] { 100, 1000 };
            if (null == vibrator) {
                vibrator = (Vibrator) mContext
                        .getSystemService(Context.VIBRATOR_SERVICE);
            }

            // this function executes the test
            switch (mState) {
            case INIT:
                mTimeIn.start();

                // Run every seconds
                vibrator.vibrate(pattern, 0);

                tl = new TestLayout1(mContext, mName, "is vibrator on?");
                // if(!mTimeIn.isFinished())
                // tl.setAutoTestButtons(false);
                // tl.setAutoTestButtons(true);

                mContext.setContentView(tl.ll);

                SetTimer(5000, new CallBack() {
                    public void c() {
                        Stop();
                    }
                });
                mState++;
                break;

            case END:
                vibrator.cancel();
                tl = new TestLayout1(mContext, mName, "test finished");
                mContext.setContentView(tl.ll);
                break;

            default:
                break;
            }
        }

    }

    @Override
    protected void onTimeInFinished() {
        TctLog.d(TAG, "update buttons");
        // if(tl != null)
        // tl.setAutoTestButtons(true);
    }

}
