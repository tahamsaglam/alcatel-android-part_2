/******************************************************************************/
/*                                                               Date:09/2012 */
/*                             PRESENTATION                                   */
/*                                                                            */
/*      Copyright 2012 TCL Communication Technology Holdings Limited.         */
/*                                                                            */
/* This material is company confidential, cannot be reproduced in any form    */
/* without the written permission of TCL Communication Technology Holdings    */
/* Limited.                                                                   */
/*                                                                            */
/* -------------------------------------------------------------------------- */
/* Author:  Qianbo Pan                                                        */
/* E-Mail:  Qianbo.Pan@tcl-mobile.com                                         */
/* Role  :  MMITest                                                           */
/* Reference documents :  Scribe 5_MMI_AUTO_Test_specfication_V3.0.Doc        */
/* -------------------------------------------------------------------------- */
/* Comments:                                                                  */
/* File    : /development/apps/EngineeringMode/src/com/EngineeringMode/       */
/*                                                       EmVibratorTest.java  */
/* Labels  :                                                                  */
/* -------------------------------------------------------------------------- */
/* ========================================================================== */
/* Modifications on Features list / Changes Request / Problems Report         */
/* -------------------------------------------------------------------------- */
/* date    | author         | key                | comment (what, where, why) */
/* --------|----------------|--------------------|--------------------------- */
/* 12/09/12| Qianbo.Pan     |                    | Porting for EngineeringMode*/
/* 28/05/13| Xinjian.Fang   |                    |  Add wakelock to this test */
/*---------|----------------|--------------------|--------------------------- */
/******************************************************************************/
package com.EngineeringMode;

import android.app.Activity;
import android.app.Service;
import android.os.PowerManager;
import android.os.PowerManager.WakeLock;
import android.os.Vibrator;
import android.text.InputType;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.TctLog;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.ToggleButton;

import java.util.Timer;
import java.util.TimerTask;
import android.view.Window;
import android.view.WindowManager;
import android.view.KeyEvent;

public class EmVibratorTest extends Activity {
    private Vibrator mVibrator;

    Timer timerVib;
    TimerTask taskVib;

    private ToggleButton mVibratorSwitchBtn;
    private Button mSetVibTimesBtn;
    private Button mSetVibOnIntervalBtn;
    private Button mSetVibOffIntervalBtn;
    private TextView mShowTimes;
    private TextView mShowVibOnInterval;
    private TextView mShowVibOffInterval;
    private EditText mSetVibOnInterval;
    private EditText mSetVibOffInterval;
    private EditText mSetVibTimes;

    private Long iTimes;
    private Long iVibOnInterval;
    private Long iVibOffInterval;

    private long iTimesUsed = 50;
    private long iVibOnIntervalUsed = 4000;
    private long iVibOffIntervalUsed = 2000;

    private long iCountVib = 0;

    private String strTimes;
    private PowerManager  pm=null;
    private WakeLock wk=null;
    private static final String TAG = "EmVibratorTest";

    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        int mask = WindowManager.LayoutParams.FLAG_FULLSCREEN;
        mask |= WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON;
        getWindow().setFlags(mask, mask);
        setContentView(R.layout.vibratetest);

        pm=(PowerManager)getSystemService(Context.POWER_SERVICE);
        wk=pm.newWakeLock(PowerManager.SCREEN_BRIGHT_WAKE_LOCK, this.getClass().getCanonicalName());

        mVibratorSwitchBtn = (ToggleButton) findViewById(R.id.VibTogglebutton);
        mSetVibTimesBtn = (Button) findViewById(R.id.SetVibTimesbutton);
        mSetVibOnIntervalBtn = (Button) findViewById(R.id.SetVibOnIntervalbutton);
        mSetVibOffIntervalBtn = (Button) findViewById(R.id.SetVibOffIntervalbutton);

        mShowTimes = (TextView) findViewById(R.id.VibTimesDisplay);
        mShowVibOnInterval = (TextView) findViewById(R.id.VibOnIntervalDisplay);
        mShowVibOffInterval = (TextView) findViewById(R.id.VibOffIntervalDisplay);

        mSetVibTimes = (EditText) findViewById(R.id.SetVibTimes);
        mSetVibOnInterval = (EditText) findViewById(R.id.OnVibIntervalEdit);
        mSetVibOffInterval = (EditText) findViewById(R.id.OffVibIntervalEdit);

        mSetVibTimes.setRawInputType(InputType.TYPE_CLASS_NUMBER);
        mSetVibOnInterval.setRawInputType(InputType.TYPE_CLASS_NUMBER);
        mSetVibOffInterval.setRawInputType(InputType.TYPE_CLASS_NUMBER);

        mVibratorSwitchBtn.setOnClickListener(VibratorSwitchlistener);
        mSetVibTimesBtn.setOnClickListener(SetVibTimeslistener);
        mSetVibOnIntervalBtn.setOnClickListener(SetVibOnIntervallistener);
        mSetVibOffIntervalBtn.setOnClickListener(SetVibOffIntervallistener);

        mShowTimes.setText("Times :" + iTimesUsed);
        mShowVibOnInterval.setText("Vib On Interval : " + iVibOnIntervalUsed
                + "ms");
        mShowVibOffInterval.setText("Vib off Interval : " + iVibOffIntervalUsed
                + "ms");

        iTimes = new Long(iTimesUsed);
        iVibOnInterval = new Long(iVibOnIntervalUsed);
        iVibOffInterval = new Long(iVibOffIntervalUsed);

        mVibrator = (Vibrator) getApplication().getSystemService(
                Service.VIBRATOR_SERVICE);
        TctLog.i(TAG,"Vibratortest onStart");
    }

    private OnClickListener VibratorSwitchlistener = new OnClickListener() {
        public void onClick(View v) {

            if (mVibratorSwitchBtn.isChecked()) {
                TctLog.e(TAG, "startVibrator");
                iCountVib = 0;
                VibrateTestOn();

            } else {
                TctLog.e(TAG, "stopVibrator");
                mVibrator.cancel();
                 if (timerVib != null) {
                  timerVib.cancel();
             }
                iCountVib = 0;
            }
        }
    };

    private OnClickListener SetVibTimeslistener = new OnClickListener() {
        public void onClick(View v) {
            if (mSetVibTimes.getText().toString().length() == 0) {
                TctLog.e(TAG, "the text is null");
                return;
            }
            iTimes = Long.parseLong(mSetVibTimes.getText().toString());
            if (iTimes.longValue() >= 1 && iTimes.longValue() <= 100000 && (!mVibratorSwitchBtn.isChecked())) {
                iTimesUsed = iTimes.longValue();
                mShowTimes.setText("Times :" + iTimesUsed);
            }

        }
    };

    private OnClickListener SetVibOnIntervallistener = new OnClickListener() {
        public void onClick(View v) {
            if (mSetVibOnInterval.getText().toString().length() == 0) {
                TctLog.e(TAG, "the text is null");
                return;
            }

            iVibOnInterval = Long.parseLong(mSetVibOnInterval.getText()
                    .toString());

            if (iVibOnInterval.longValue() > 10
                    && iVibOnInterval.longValue() < 10000 && (!mVibratorSwitchBtn.isChecked())) {
                iVibOnIntervalUsed = iVibOnInterval.longValue();
                mShowVibOnInterval.setText("Vib On Interval : "
                        + iVibOnIntervalUsed + "ms");
            }

        }
    };

    private OnClickListener SetVibOffIntervallistener = new OnClickListener() {
        public void onClick(View v) {
            if (mSetVibOffInterval.getText().toString().length() == 0) {
                TctLog.e(TAG, "the text is null");
                return;
            }
            iVibOffInterval = Long.parseLong(mSetVibOffInterval.getText()
                    .toString());

            if (iVibOffInterval.longValue() > 10
                    && iVibOffInterval.longValue() < 10000 && (!mVibratorSwitchBtn.isChecked())) {
                iVibOffIntervalUsed = iVibOffInterval.longValue();
                mShowVibOffInterval.setText("Vib Off Interval : "
                        + iVibOffIntervalUsed + "ms");
            }

        }
    };

    private void VibrateTestOn() {
        if (iCountVib >= iTimes) {
            iCountVib = 0;
            return;
        }
        timerVib = new Timer();
        taskVib = new TimerTask() {
            public void run() {
                VibrateTestOn();
            }
        };
        mVibrator.vibrate(iVibOnIntervalUsed);
        timerVib.schedule(taskVib, iVibOnIntervalUsed + iVibOffIntervalUsed);
        iCountVib++;
        if(iCountVib % 10 == 0){
            TctLog.i(TAG,"iCountVib="+iCountVib);
        }
    }

    protected void onResume() {
        super.onResume();
        if(wk != null){
            wk.acquire();
        }
        TctLog.i(TAG,"on Resume");
    }

    protected void onPause() {
        super.onPause();
        if(wk !=null){
            wk.release();
        }
         TctLog.i(TAG,"on Pause");
    }

     protected void onStop() {
        super.onStop();
        /*mVibrator.cancel();
        if (timerVib != null) {
            timerVib.cancel();
        }
        iCountVib = 0; */
         TctLog.i(TAG,"onStop");
    }

    protected void onDestroy() {
        super.onDestroy();
        mVibrator.cancel();
        if (timerVib != null) {
            timerVib.cancel();
        }
        wk = null;
        pm = null;
        iCountVib = 0;
        TctLog.i(TAG,"onDestroy");
    }

}
