/******************************************************************************/
/*                                                               Date:09/2012 */
/*                             PRESENTATION                                   */
/*                                                                            */
/*      Copyright 2012 TCL Communication Technology Holdings Limited.         */
/*                                                                            */
/* This material is company confidential, cannot be reproduced in any form    */
/* without the written permission of TCL Communication Technology Holdings    */
/* Limited.                                                                   */
/*                                                                            */
/* -------------------------------------------------------------------------- */
/* Author:  Qianbo Pan                                                        */
/* E-Mail:  Qianbo.Pan@tcl-mobile.com                                         */
/* Role  :  MMITest                                                           */
/* Reference documents :  Scribe 5_MMI_AUTO_Test_specfication_V3.0.Doc        */
/* -------------------------------------------------------------------------- */
/* Comments:                                                                  */
/* File    : /development/apps/EngineeringMode/src/com/EngineeringMode/       */
/*                                                             LEDTorch.java  */
/* Labels  :                                                                  */
/* -------------------------------------------------------------------------- */
/* ========================================================================== */
/* Modifications on Features list / Changes Request / Problems Report         */
/* -------------------------------------------------------------------------- */
/* date    | author         | key                | comment (what, where, why) */
/* --------|----------------|--------------------|--------------------------- */
/* 12/09/12| Qianbo.Pan     |                    | Porting for EngineeringMode*/
/*         |                |                    |                            */
/*---------|----------------|--------------------|--------------------------- */
/* 12/22/15| Zhanghong      | alm defect 1096794 | Add front flash led control*/
/*         |                |                    |                            */
/******************************************************************************/
package com.EngineeringMode;

import java.io.IOException;
import java.lang.reflect.Method;
import java.util.Timer;
import java.util.TimerTask;

import android.app.Activity;
import android.content.Context;
import android.graphics.SurfaceTexture;//[DEFECT]-Add by TSNJ Zhenhua.Fan,16/12/2015,DEFECT- 1019103
import android.hardware.camera2.CameraAccessException;
import android.hardware.camera2.CameraCharacteristics;
//import android.hardware.Camera;
//import android.hardware.Camera.Parameters;
import android.hardware.camera2.CameraManager;
import android.hardware.camera2.CameraMetadata;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.InputType;
import android.util.Log;
import android.util.TctLog;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.ToggleButton;

public class LEDTorch extends Activity {
    private ToggleButton mLEDTorchSwitchBtn;
    private Button mSetFlashTimesBtn;
    private Button mSetFlashOnIntervalBtn;
    private Button mSetFlashOffIntervalBtn;
    private TextView mShowTimes;
    private TextView mShowFlashOnInterval;
    private TextView mShowFlashOffInterval;
    private EditText mSetFlashLEDOnInterval;
    private EditText mSetFlashLEDOffInterval;
    private EditText mSetFlashTimes;
    private RadioButton mMainLED;
    private RadioButton mFrontLED;
    private RadioGroup mLEDGroup;
    //private int mCameraId;


    private Long iTimes;
    private Long iFlashOnInterval;
    private Long iFlashOffInterval;

    private long iTimesUsed = 3000;
    private long iFlashOnIntervalUsed = 180000;
    private long iFlashOffIntervalUsed = 60000;
    private long iFlashCount = 0;


    Timer timerLEDON;

    Timer timerLEDOFF;

    private static final String TAG = "Torch";

    //    public static final String FLASH_LED_FILE = "/sys/class/leds/led-flash/brightness";
    public static final int MIN = 0;
    public static final int NORMAL = 180;
    public static final int MAX = 255;
    public static final int FLASH = 127;

    String strOpen = "torch";
    String strOff = "off";

    private static final int END_LED_TORCH = 0;

    //private Camera mCamera = null;
    //private Camera.Parameters mParameters;
    private CameraManager mCameraManager;
    private String mCameraID_Front;
    private String mCameraID_Back;
    private String mCameraId;

    /**
     * Called when the activity is first created.
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        int mask = WindowManager.LayoutParams.FLAG_FULLSCREEN;
        mask |= WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON;
        getWindow().setFlags(mask, mask);
        setContentView(R.layout.torch);
        mLEDTorchSwitchBtn = (ToggleButton) findViewById(R.id.TorchTogglebutton);
        mSetFlashTimesBtn = (Button) findViewById(R.id.SetTimesbutton);
        mSetFlashOnIntervalBtn = (Button) findViewById(R.id.SetOnIntervalbutton);
        mSetFlashOffIntervalBtn = (Button) findViewById(R.id.SetOffIntervalbutton);

        mShowTimes = (TextView) findViewById(R.id.TimesDisplay);
        mShowFlashOnInterval = (TextView) findViewById(R.id.FlashOnIntervalDisplay);
        mShowFlashOffInterval = (TextView) findViewById(R.id.FlashOffIntervalDisplay);

        mSetFlashTimes = (EditText) findViewById(R.id.SetFlashTimes);
        mSetFlashLEDOnInterval = (EditText) findViewById(R.id.OnIntervalEdit);
        mSetFlashLEDOffInterval = (EditText) findViewById(R.id.OffIntervalEdit);
        mMainLED = (RadioButton) findViewById(R.id.main_led);
        mFrontLED = (RadioButton) findViewById(R.id.front_led);
        mLEDGroup = (RadioGroup) findViewById(R.id.led_group);
        mLEDGroup.setOnCheckedChangeListener(LEDChangeListener);
        //mCameraId = Camera.CameraInfo.CAMERA_FACING_BACK;

        mSetFlashTimes.setRawInputType(InputType.TYPE_CLASS_NUMBER);
        mSetFlashLEDOnInterval.setRawInputType(InputType.TYPE_CLASS_NUMBER);
        mSetFlashLEDOffInterval.setRawInputType(InputType.TYPE_CLASS_NUMBER);

        mLEDTorchSwitchBtn.setOnClickListener(LEDTorchSwitchlistener);
        mLEDTorchSwitchBtn.setOnCheckedChangeListener(LEDTorchSwitchChangeListener);
        mSetFlashTimesBtn.setOnClickListener(SetFlashTimeslistener);
        mSetFlashOnIntervalBtn.setOnClickListener(SetFlashOnIntervallistener);
        mSetFlashOffIntervalBtn.setOnClickListener(SetFlashOffIntervallistener);
        mCameraManager = (CameraManager) getSystemService(Context.CAMERA_SERVICE);
        if(mCameraManager!=null) {
            String[] cameraList;
            try {
                cameraList = mCameraManager.getCameraIdList();
                for(String cameraString:cameraList) {
                    int camFacing = mCameraManager.getCameraCharacteristics(cameraString)
                            .get(CameraCharacteristics.LENS_FACING).intValue();
                    if( camFacing == CameraMetadata.LENS_FACING_FRONT) {
                        mCameraID_Front = cameraString;
                    }
                    else {
                        mCameraID_Back = cameraString;
                    }
                }
            } catch (CameraAccessException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
        mCameraId = mCameraID_Back;
    }


    @Override
    protected void onResume() {
        super.onResume();
        TctLog.e(TAG, "onResume");
        iTimesUsed = Long.parseLong(mSetFlashTimes.getText().toString());
        iFlashOnIntervalUsed = Long.parseLong(mSetFlashLEDOnInterval.getText().toString());
        iFlashOffIntervalUsed = Long.parseLong(mSetFlashLEDOffInterval.getText().toString());
        mShowTimes.setText("Times :" + iTimesUsed);
        mShowFlashOnInterval.setText("LED On Interval : "
                + iFlashOnIntervalUsed + "ms");
        mShowFlashOffInterval.setText("LED off Interval : "
                + iFlashOffIntervalUsed + "ms");

        iTimes = new Long(iTimesUsed);
        iFlashOnInterval = new Long(iFlashOnIntervalUsed);
        iFlashOffInterval = new Long(iFlashOffIntervalUsed);
        mSetFlashTimes.clearFocus();
        mSetFlashLEDOnInterval.clearFocus();
        mSetFlashLEDOffInterval.clearFocus();
        mLEDTorchSwitchBtn.setChecked(iFlashCount == 0 ? false : true);
    }


    @Override
    protected void onPause() {
        super.onPause();
        TctLog.e(TAG, "onPause");
        StopFlashLEDTorch();
    }

    private OnClickListener LEDTorchSwitchlistener = new OnClickListener() {
        public void onClick(View v) {

            if (mLEDTorchSwitchBtn.isChecked()) {
                TctLog.e(TAG, "startTorch");
                StartFlashLEDTorch();
            } else {
                TctLog.e(TAG, "stopTorch");
                StopFlashLEDTorch();
            }

        }
    };

    private CompoundButton.OnCheckedChangeListener LEDTorchSwitchChangeListener = new CompoundButton.OnCheckedChangeListener() {
        @Override
        public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
            setRadioButtonEnable(!isChecked);
        }
    };

    private RadioGroup.OnCheckedChangeListener LEDChangeListener = new RadioGroup.OnCheckedChangeListener() {
        @Override
        public void onCheckedChanged(RadioGroup group, int checkedId) {
            switch (checkedId) {
                case R.id.main_led:
                    //mCameraId = CameraMetadata.LENS_FACING_BACK; //Camera.CameraInfo.CAMERA_FACING_BACK;
                    mCameraId = mCameraID_Back;
                    break;
                case R.id.front_led:
                    //mCameraId = CameraMetadata.LENS_FACING_FRONT; //Camera.CameraInfo.CAMERA_FACING_FRONT;
                    mCameraId = mCameraID_Front;
                    break;
            }
        }
    };

    private void setRadioButtonEnable(boolean enable) {
        TctLog.e(TAG, "RadioButton enable:" + enable);
        mMainLED.setEnabled(enable);
        mFrontLED.setEnabled(enable);
    }

    private OnClickListener SetFlashTimeslistener = new OnClickListener() {
        public void onClick(View v) {

            String s = mSetFlashTimes.getText().toString().trim();
            if (s != null && s.length() > 0) {
                TctLog.e(TAG, "the text is " + s);
                iTimes = Long.parseLong(mSetFlashTimes.getText().toString());

                if (iTimes.longValue() >= 1 && iTimes.longValue() <= 100000) {
                    iTimesUsed = iTimes.longValue();
                    mShowTimes.setText("Times :" + iTimesUsed);
                } else {
                    TctLog.e(TAG, "please input valid value");
                    // Toast.makeText(this,
                    // "Please input value between 1 and 100000",
                    // Toast.LENGTH_SHORT).show();

                }
            } else
                TctLog.e(TAG, "the text is null");
        }
    };

    private OnClickListener SetFlashOnIntervallistener = new OnClickListener() {
        public void onClick(View v) {

            String s = mSetFlashLEDOnInterval.getText().toString().trim();
            if (s != null && s.length() > 0) {
                TctLog.e(TAG, "the text is " + s);
                iFlashOnInterval = Long.parseLong(mSetFlashLEDOnInterval.getText()
                        .toString());

                if (iFlashOnInterval.longValue() >= 10
                        && iFlashOnInterval.longValue() <= 180000) {
                    iFlashOnIntervalUsed = iFlashOnInterval.longValue();
                    mShowFlashOnInterval.setText("LED On Interval : "
                            + iFlashOnIntervalUsed + "ms");
                } else {
                    TctLog.e(TAG, "please input valid value");
                    // Toast.makeText(this,
                    // "Please input value between 10 and 10000",
                    // Toast.LENGTH_SHORT).show();

                }
            } else
                TctLog.e(TAG, "the text is null");

        }
    };
    private OnClickListener SetFlashOffIntervallistener = new OnClickListener() {
        public void onClick(View v) {

            String s = mSetFlashLEDOffInterval.getText().toString().trim();
            if (s != null && s.length() > 0) {
                TctLog.e(TAG, "the text is " + s);
                iFlashOffInterval = Long.parseLong(mSetFlashLEDOffInterval
                        .getText().toString());

                if (iFlashOnInterval.intValue() >= 10
                        && iFlashOnInterval.intValue() <= 10000) {
                    iFlashOffIntervalUsed = iFlashOffInterval.longValue();
                    mShowFlashOffInterval.setText("LED Off Interval : "
                            + iFlashOffIntervalUsed + "ms");
                } else {
                    TctLog.e(TAG, "please input valid value");
                    // Toast.makeText(this,
                    // "Please input value between 10 and 10000",
                    // Toast.LENGTH_SHORT).show();
                }
            } else
                TctLog.e(TAG, "the text is null");
        }
    };


    private void StartFlashLEDTorch() {
        openCamera();
        iFlashCount = iTimesUsed;
        FlashLEDON();
    }

    private void StopFlashLEDTorch() {
//        brighnesstSet(MIN);
        openCameraLed(false);
        if (timerLEDON != null)
            timerLEDON.cancel();
        if (timerLEDOFF != null)
            timerLEDOFF.cancel();
        iFlashCount = 0;
        stopCamera();
    }


    private void FlashLEDON() {
//        brighnesstSet(MAX);
        openCameraLed(true);
        timerLEDOFF = new Timer();
        TimerTask taskLEDOFF = new TimerTask() {

            @Override
            public void run() {
                // TODO Auto-generated method stub
                FlashLEDOFF();
            }

        };
        timerLEDOFF.schedule(taskLEDOFF, iFlashOnIntervalUsed);
    }

    private void FlashLEDOFF() {
//        brighnesstSet(MIN);
        openCameraLed(false);
        if (iFlashCount > 1) {
            iFlashCount--;
            timerLEDON = new Timer();
            TimerTask taskLEDON = new TimerTask() {

                @Override
                public void run() {
                    // TODO Auto-generated method stub
                    FlashLEDON();
                }

            };
            timerLEDON.schedule(taskLEDON, iFlashOffIntervalUsed);
        } else {
            Message msg = new Message();//function 1,send message to UI thread.
            msg.what = END_LED_TORCH;
            mHandler.sendMessage(msg);

        }
    }

    Handler mHandler = new Handler() {
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case END_LED_TORCH:
                    mLEDTorchSwitchBtn.setChecked(false);
                    stopCamera();
                    break;
                default:
                    break;
            }
            super.handleMessage(msg);
        }
    };

//    private void brighnesstSet(int brightness)
//    {
//        try{
//             File file=new File(FLASH_LED_FILE);
//             if(!file.exists())
//             {
//                 TctLog.e(TAG,"File not found");
//             }
//             FileOutputStream out=new FileOutputStream(file,true);
//             out.write(Integer.toString(brightness).getBytes());
//             out.close();
//        }catch(Exception e)
//        {
//            e.printStackTrace();
//            TctLog.e("TAG","Write brightness fail");
//        }
//
//    }

    private void openCamera() {
        /*TctLog.e(TAG, "openCamera");
        try {
            Method openMethod = Class.forName("android.hardware.Camera").getMethod("openLegacy", int.class, int.class);
            mCamera = (Camera) openMethod.invoke(null, mCameraId, 0x100);
            mParameters = mCamera.getParameters();
            mParameters.set("no-display-mode", 1);
//            mCamera = Camera.open(mCameraId);
//            mCamera.setPreviewTexture(new SurfaceTexture(0));//[DEFECT]-Add by TSNJ Zhenhua.Fan,16/12/2015,DEFECT- 1019103
            mCamera.startPreview();
        } catch (Exception e) {
            e.printStackTrace();
        }*/
    }

    private void stopCamera() {
        /*TctLog.e(TAG, "stopCamera");
        if (mCamera != null) {
            try {
                //TODO Without this it will be slow in stopcamera. DONT KNOW WHY!
                mCamera.setPreviewTexture(new SurfaceTexture(0));
            } catch (IOException e) {
                e.printStackTrace();
            }
            mCamera.stopPreview();
            mCamera.release();
            mCamera = null;
        }*/
    }

    private void openCameraLed(boolean on) {
        TctLog.e(TAG, "openCameraLed: " + on);
        /*if (mCamera != null) {
            mParameters = mCamera.getParameters();
            mParameters.setFlashMode(on ? Parameters.FLASH_MODE_TORCH : Camera.Parameters.FLASH_MODE_OFF);
            mCamera.setParameters(mParameters);
        }*/
        if(mCameraManager!=null) {
            try {
                mCameraManager.setTorchMode(mCameraId, on);
            } catch (CameraAccessException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
    }

    protected void onDestroy() {
        TctLog.e(TAG, "onDestroy");
        super.onDestroy();
    }

}
